#!/usr/bin/python

import sys, getopt, os, re, glob, shutil
from xml.etree.ElementTree import parse, ElementTree

def main(argv):
    inputfile = ''
    filesource = ''
    split_node = 'record'
    has_extension = False
    max_files = 1000
    outdir = os.getcwd()
    indir = ''

    try:
        opts, args = getopt.getopt(argv, "i:f:s:em:o:d:")
    except getopt.GetoptError:
        print("splitter.py -i <inputfile> -f <filesource>")
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-i':
            inputfile = arg
        elif opt == '-f':
            filesource = arg
        elif opt == '-e':
            has_extension = True
        elif opt == '-m':
            max_files = int(arg)
        elif opt == '-o':
            outdir = arg
        elif opt == '-s':
            split_node = arg
        elif opt == '-d':
            indir = arg

    print("Parsing....")
    f2 = open(inputfile, 'r')
    tree = parse(f2)
    f2.close()

    records = tree.findall(split_node)
    if len(records) < 1:
        print("Could not find split node '{}'".format(split_node))
        sys.exit(2)

    print("{} records found".format(len(records)))
        
    filenodes = records[0].findall(filesource)
    if len(filenodes) < 1:
        print("Could not find file source '{}'".format(filesource))
        sys.exit(2)

    nodes = []
    duplicates = []

    for r in records:
        if len(nodes) % max_files == 0:
            cdir = "{}/{}/".format(outdir, (len(nodes) / max_files))
            if not os.path.isdir(cdir):
                os.mkdir(cdir)
                print("Created directory " + cdir)
        sources = r.findall(filesource)
        if len(sources) < 1:
            print("Could not find file source for:")
            print(r.text)
        if sources[0].text:
            filename = sources[0].text.strip()
            if not filename in nodes: 
                if has_extension:
                    img_file = indir + filename
                    if os.path.isfile(img_file):
                        copy_files(r, cdir, filename[:-4], img_file, nodes)
                        print("Successfully copied: " + img_file)
                    else:
                        print("Couldn't find file: " + img_file)
                else:
                    files = glob.glob("{}/{}.*".format(indir, filename))
                    if len(files) == 1:
                        copy_files(r, cdir, filename, files[0], nodes)
                        print("Copied: " + files[0])
                    elif len(files) == 0:
                        print("Couldn't find file for " + filename)
                    else:
                        print("Found multiple files that match " + filename)
                        print(files)
            else:
                duplicates.append(filename)
                print("Found duplicate: " + filename)
    
    print("Created {} xml files".format(len(nodes)))
    print("Found {} duplicates".format(len(duplicates)))
                
def copy_files(r, cdir, filename, img_file, nodes):
    shutil.copy(img_file, cdir)
    dest_file = open(cdir + filename + ".xml", 'wb')
    ElementTree(r).write(dest_file)
    dest_file.close()
    nodes.append(filename)

if __name__ == "__main__":
    main(sys.argv[1:])
