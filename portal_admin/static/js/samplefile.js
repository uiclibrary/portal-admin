function get_values(selector, prefix)
{
    var data = {};
    $.each($(selector), function(){
        data[$(this).attr('name').replace(prefix, "")] = $(this).val();
    });
    return data;
}

function validate_field(bname, file, split_node)
{
    var prefix = bname.replace("test", "");
    var data = {"filename": file, "split_node": split_node};
    $.extend(data, get_values("input[name^=" + prefix + "][type!='radio']", prefix));
    $.extend(data, get_values("input[name^=" + prefix + "][type='radio']:checked", prefix));
    $.extend(data, get_values("select[name^=" + prefix + "]", prefix));
    $.ajax({ url: "/validatexpath/", 
             type: "POST",
             dataType: "json", 
             data: JSON.stringify(data), 
             success: function(data){
                 var alert = $("#id_" + prefix + "alert");
                 alert.removeClass();
                 alert.addClass("alert alert-" + data["result"]);
		 alert.empty();
		 if(data["result"] == "danger"){
		     alert.append("<span class='glyphicon glyphicon-remove'></span>");
		 }
		 else{
		     alert.append("<span class='glyphicon glyphicon-ok'></span>");
		 }
		 if(data["value"].length > 1 && data["result"] != "danger"){
		     var span = $("<span/>").text(" " + data["value"].length + " values found").appendTo(alert);
		     var ul = $("<ul/>").appendTo(alert);
		     $.each(data["value"], function(i){
			 var li = $("<li/>").text(data["value"][i]).appendTo(ul);
		     });
		 }
		 else{
		     $("<span/>").text(" " + data["value"]).appendTo(alert);
		 }
             }});
}

function validate_filesource(bname, file)
{
    var prefix = bname.replace("2-test", "");
    var data = {};
    data["filename"] = file;
    if ($("#id_" + prefix + "record_separator").attr('type') != 'hidden'){
	data["split_node"] = $("#id_" + prefix + "record_separator").val();
    }
    data["xpath"] = $("#id_" + prefix + "image_filename").val();
    data["instance"] = $("#id_" + prefix + "filename_instance").val();
    data["includes_fileextension"] = $("#id_" + prefix + "has_extension").val();
    data["use_filename"] = $("input[name=" + prefix + "use_filename]:checked").val();
    $.ajax({ url: "/validatefilesource/", 
             type: "POST",
             dataType: "json", 
             data: JSON.stringify(data), 
             success: function(data){
                 var alert = $("#id_" + prefix + "2-alert");
                 alert.removeClass();
                 alert.addClass("alert alert-" + data["result"]);
		 alert.empty();
		 if(data["result"] == "danger"){
		     alert.append("<span class='glyphicon glyphicon-remove'></span>");
		 }
		 else{
		     alert.append("<span class='glyphicon glyphicon-ok'></span>");
		 }
                 alert.append("<span> " + data["value"] + "</span>")
             }});
}

function validate_separator(bname, file)
{
    var prefix = bname.replace("1-test", "");
    var split_node = $("#id_" + prefix + "record_separator").val();
    var data = {"filename": file, "split_node": split_node};
    $.ajax({ url: "/validateseparator/",
	     type: "POST",
	     dataType: "json",
	     data: JSON.stringify(data),
	     success: function(data){
                 var alert = $("#id_" + prefix + "1-alert");
                 alert.removeClass();
                 alert.addClass("alert alert-" + data["result"]);
		 alert.empty();
		 if(data["result"] == "danger"){
		     alert.append("<span class='glyphicon glyphicon-remove'></span>");
		 }
		 else{
		     alert.append("<span class='glyphicon glyphicon-ok'></span>");
		 }
                 alert.append("<span> " + data["value"] + "</span>")
	     }
	   });
}

function add_another(add_btn)
{
    var prefix = add_btn.name.replace("add-another", "");
    var total = $('#id_' + prefix + 'TOTAL_FORMS').val();
    var fprefix = prefix + (total - 1) + '-';
    var newprefix = prefix + total + '-';

    var selector = "div[id='id_" + fprefix + "form-fields']";
    var newElement = $(selector).clone(true, true);

    newElement.attr({'id': "id_" + newprefix + "form-fields"});
    newElement.find(':input').each(function() {
        var name = $(this).attr('name').replace(fprefix, newprefix);
        var id = 'id_' + name;
        $(this).attr({'name': name, 'id': id});
	if(name.indexOf("-test") == -1 && name.indexOf("-instance") == -1){
	    $(this).val('');
	}
    });
    newElement.find('label').each(function() {
        var newFor = $(this).attr('for').replace(fprefix, newprefix);
        $(this).attr('for', newFor);
    });
    newElement.find("div[id='id_" + fprefix + "alert']").each(function(){
        var id = $(this).attr('id').replace(fprefix, newprefix);
        $(this).attr({'id': id});
	$(this).removeClass();
	$(this).addClass("hidden");
	$(this).text('');
    });
    total++;
    $('#id_' + prefix + 'TOTAL_FORMS').val(total);
    $(selector).after(newElement);
}
