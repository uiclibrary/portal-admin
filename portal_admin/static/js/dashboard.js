// GENERAL

// mark placeholder anchors
$('a[href="#"]').css('color', 'Silver')
$('a[href="#"] .badge').css('background-color', '#bbb').load(function(e) {
    this.parent('a').removeAttr("href")
});
$('a[href="#"] .label').css('background-color', '#bbb').load(function(e) {
    this.parent('a').removeAttr("href")
});

// fill navbar navigation, only shown when xs
var sidebar_links = $('#sidebar_nav ul').children().clone()
$('#navbar_nav_list').append(sidebar_links)


// DEPOSIT

// radio buttons cause dependent fields to become cleared and deactivated
$("input[name='4-0-0-use_xpath']").change( function() {
    if ( $("#id_4-0-0-use_xpath_0").prop("checked" ) === true ) {
        $("#filename_fields").prop('disabled', false)
    } else {
        $("#filename_fields").prop('disabled', true)
        $("#filename_fields").closest('form').find("input[type=text], textarea").val("")
    }
})

// SEARCH

// popovers
$('.hover_popover').popover({"trigger": "hover"})



// TAGGING
// checkbox select all checkbox for tabs
$('.checkbox_select_all_tab').change(function(event) {
    var checkboxes = $(this).parent().parent().parent().find('input')
    if ($(this).is(":checked")) {
        checkboxes.each(function() {
            if ($(this).is(":visible")) {
                this.checked = true
            }
        })
    } else {
        checkboxes.each(function() {
            if ($(this).is(":visible")) {
                this.checked = false
            }
        })
    }
})

// tag filtering
$( ".tag_filter_field" ).keyup(function() {
    filter_tags(this)
})

// clear tag filter input button
$( ".filter_tag_remove" ).click(function() {
  var input_field = $(this).parent().parent().find('input')
  input_field.val('')
  filter_tags(input_field)
})

function filter_tags(input_field) {
    var value = $(input_field).val()
    var current_pane
    var labels = $(".tab-pane.active #tag_checkboxes label")

    labels.each(function() {
        var label_text = $(this).text()

        if (label_text.toLowerCase().search(value.toLowerCase()) > -1) {
            $(this).parent().show()
        }
        else {
            $(this).parent().hide()
        }
    })
}

// ADMINISTRATION
// Error logs: date picker
$(function() {
    $( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd" })
})


// Sidebar movement
$('.sidebar').change(function(event) {

})

// Inline Editing
//$(document).ready(function() {
//    $.fn.editable.defaults.mode = 'inline';
//    $('#object_title').editable({
//        url: '#',
//        title: 'Enter username',
//        inputclass: 'editable_title',
//    });
//});


$("#tag_pulldown_button").click(function() {
    togglePulldown() })
var pulldownHeight = "6rem";

$(function(){
    $('#tag_pulldown').each(function () {
                var current = $(this)
                current.attr("box_h", current.height())
            }
     )
    $("#tag_pulldown").css("height", pulldownHeight)
})

var toggle = 'closed'
function togglePulldown() {
    var open_height = parseInt($("#tag_pulldown").attr("box_h")) + 25 + "px"

    if (toggle === 'closed') {
        $("#tag_pulldown").animate({"height": open_height}, {duration: "medium" })
        toggle = 'open'
        $("#pulldown_chevron").toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    } else {
        $("#tag_pulldown").animate({"height": pulldownHeight}, {duration: "medium" })
        toggle = 'closed'
        $("#pulldown_chevron").toggleClass('glyphicon-chevron-up glyphicon-chevron-down');
    }
}
