import os

from django import forms
from django.conf import settings

from portal_admin.inspection import verify_xml_structure
from portal_admin.custom_logging import XMLStructureInvalid
from portal_admin.context_managers import strip_ns_file

class StripTextField(forms.CharField):
    def clean(self, value):
        return super(StripTextField, self).clean(value).strip()

class XPathTextField(StripTextField):
    def clean(self, value):
        return super(XPathTextField, self).clean(value).rstrip("/")

class XMLFileField(forms.FileField):
    def clean(self, *args, **kwargs):
        super(XMLFileField, self).clean(*args, **kwargs)
        tmp = args[0]

        if not tmp.name.endswith(".xml"):
            raise forms.ValidationError("File is not XML")

        path = os.path.join(getattr(settings,'SAMPLE_FILE_ROOT'), tmp.name)
        d = open(path, 'wb+')
        for c in tmp.chunks():
            d.write(c)
        d.close()

        strip_ns_file(path)

        try:
            verify_xml_structure(path)
        except XMLStructureInvalid:
            raise forms.ValidationError("File is not valid XML")

        return args

class ObjectChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj

class FieldChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.field_label

