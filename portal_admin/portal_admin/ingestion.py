from django.conf import settings

from portal_admin import models as pa
from portal_admin import utils

from django.utils.module_loading import import_by_path

import datetime

from xml.etree.ElementTree import tostring

def update_digobj(digobj_id, record, context_id):
    digobj = pa.DigObject.objects.get(pk=digobj_id)
    tclass = import_by_path(settings.IMPORT_TRANSFER_BACKEND)
    transfer = tclass()
    transfer.delete_files(digobj, pa.DigObjectStatus.objects.all())
    digobj.file_set.all().delete()
    digobj.importedvalue_set.all().delete()
    digobj.title = utils.assemble_value(record, digobj.profile.pk, 'title')
    digobj.save()
    import_values(digobj.pk, digobj.profile.pk, record)
    create_files(digobj.pk, digobj.profile.pk, context_id, record)

def create_digobj(record, profile_id, context_id, location_id=None):
    """
    Given an XML record, a profile, and context; creates a digital
    object.  This top-level object does not have any ElementSource(s).
    """

    profile = pa.Profile.objects.get(id=profile_id)
    context = pa.Context.objects.get(id=context_id)
    location = pa.Location.objects.get(id=location_id) if location_id else None

    digobject = pa.DigObject.objects.create(ark=utils.generate_ark(),
                                            title=utils.assemble_value(record, profile_id, 'title'),
                                            profile=profile,
                                            digobject_form=profile.digobject_form,
                                            location=location)

    pa.DigObjectContext.objects.create(digobject=digobject,
                                       context=context,
                                       digobject_status=pa.DigObjectStatus.objects.get(status='Deposited'))

    return digobject.id

def import_values(digobj_id, profile_id, record):
    """
    Given an XML record, a profile, and a digital object;
    creates the set of ElementSource(s) and stores them
    as ImportedValue(s), thus describing the digital object.
    """

    profile = pa.Profile.objects.get(id=profile_id)
    digobj = pa.DigObject.objects.get(id=digobj_id)

    sources = pa.ElementSource.objects.filter(profile=profile)
    for s in sources:
        if s.default_text:
            val = s.default_text
            element, created = pa.ValueElement.objects.get_or_create(element=val)
            pa.ImportedValue.objects.create(value=element,
                                            digobject=digobj,
                                            dcxml_format=s.dcxml_format)
        else:
            try:
                if s.use_filename:
                    vals = [record.filename.replace(".xml", "")]
                else:
                    vals = utils.get_clean_values(record.record, s.xpath, s.instance_number, s.delimiter)

                for val in vals:
                    element, created = pa.ValueElement.objects.get_or_create(element=val)
                    pa.ImportedValue.objects.create(value=element,
                                                    digobject=digobj,
                                                    dcxml_format=s.dcxml_format)
            except utils.FieldNotFound:
                continue
            except utils.ValNotFound:
                continue

def create_files(digobj_id, profile_id, context_id, record):
    """
    Creates File objects, representing first the XML file, or
    metadata record, used to define the digital object;
    and then creating a set of File objects to represent
    any media attached to the record.
    """

    profile = pa.Profile.objects.get(id=profile_id)
    context = pa.Context.objects.get(id=context_id)
    digobj = pa.DigObject.objects.get(id=digobj_id)

    checksum = utils.get_checksum(tostring(record.record))
    pa.File.objects.create(filename_original=record.filename,
                           filename_new="{}.xml".format(digobj.ark),
                           digobject=digobj,
                           checksum=checksum)
    fsources = pa.FileSource.objects.filter(profile=profile)
    content = [utils.get_content_pointer(record, f) for f in fsources]

    if profile.is_remote:
        files = [(utils.get_checksum(c.encode('utf-8')), c.split(".")[-1], c.split("/")[-1]) for c in content]
    else:
        fpaths = [utils.find_fpath(c, digobj.digobject_form._type, record.path) for c in content]
        files = []
        for fpath in fpaths:
            with open(fpath,'rb') as f:
                files.append((utils.get_checksum(f.read()),
                              fpath.split(".")[-1],
                              fpath.split("/")[-1]))

    for f in files:
        pa.File.objects.create(
            filename_original=f[2],
            filename_new="{}.{}".format(digobj.ark, f[1]),
            digobject=digobj,
            checksum=f[0])

    tclass = import_by_path(settings.IMPORT_TRANSFER_BACKEND)
    transfer = tclass()
    transfer.transfer(digobj, context.id, record, files)

def add_dcxml_element(root, element_name, text, purpose=None):
        x = SubElement(root, element_name)
        if purpose:
            x.attrib['purpose'] = purpose
        x.text = str(text)

def create_dc_xml(digobj_id, context_id):
    """
    Creates a dc.xml file, describing the digital object;
    based on the various information pulled via ElementSource(s)
    """

    digobj = pa.DigObject.objects.get(id=digobj_id)
    return digobj.create_dcxml(context_id)

def create_location_dcxml(location_id):
    location = pa.Location.objects.get(pk=location_id)
    return location.create_dcxml()

def create_institution_dcxml(institution_id):
    inst = pa.Institution.objects.get(pk=institution_id)
    return inst.create_dcxml()

def validate_xpath(record, xpath, instance, delimiter):
    values = []
    for v in utils.get_clean_values(record, xpath, instance, delimiter):
        values.append(v)
    return values
