from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from portal_admin.models import Location
from portal_admin import processor

class Command(BaseCommand):
    can_import_settings = True

    def handle(self, *args, **options):
        for l in Location.objects.all():
            path = processor.generate_location_dcxml(l.pk)
            print(path)
            
