NULLS = [None, "", False]

from portal_admin import ingestion
from portal_admin import utils
from xml.etree.ElementTree import parse

class ValidationError(Exception):
    pass

def generate_xpath(xpath, attribute, value):
    if not attribute in NULLS:
        if not value in NULLS:
            return "{}[@{}=\"{}\"]".format(xpath, attribute, value)
        else:
            return "{}/@{}".format(xpath, attribute)
    else:
        return xpath

def validate_separator(filename, split_node):
    with open(filename, 'r', encoding="utf-8") as f:
        records = parse(f).findall(split_node)
        if len(records) < 1:
            raise ValidationError("Separator not found")
    return "File contains {} records".format(len(records))

def validate_filesource(filename, data):
    if data.get("use_filename", "False") == "True":
        return "Using filename"
    xpath = generate_xpath(data["xpath"], None, None)
    return validate_xpath(filename, data.get('split_node', None), xpath, data.get('instance', 0), None)

def validate_with_sample_file(filename, data):
    data = validate_formdata(data, True)
    if data.get("use_xpath", "True") == "False":
        if "default" in data:
            return [data["default"]]
        else:
            return "Using filename"
    xpath = generate_xpath(data["xpath"], data.get('attribute', None), data.get('attribute_value', None))
    return validate_xpath(filename, data.get('split_node', None), xpath, data.get('instance', -1), data.get('delimiter', None))

def validate_formdata(data, is_required):
    # if there's a value there should be an attribute
    if len(data.get('attribute', "")) == 0 and len(data.get('attribute_value', "")) > 0: 
        raise ValidationError("You must enter an attribute to enter an attribute value.")
    # if this form is empty and not required ignore these errors
    if is_required or len(data.get('xpath', '')) > 0:
        # if it says there's a delimiter there must be one
        if data.get('has_delimiter', "False") == "True" and len(data.get("delimiter")) == 0:
            raise ValidationError("You did not enter a delimiter")
        # if "use_xpath" is in the data we're using a default or filename widget
        if "use_xpath" in data:
            # if it says to use xpath make sure there is one
            if data["use_xpath"] == "True":
                if len(data['xpath']) == 0:
                    raise ValidationError("You did not enter an xpath.")
            # if it's a default make sure there is one
            elif "default" in data:
                if len(data['default']) == 0:
                    raise ValidationError("You did not enter a default value.")
    return data

def validate_xpath(filename, split_node, xpath, instance, delimiter):
    try:
        with open(filename, 'r', encoding="utf-8") as f:
            if split_node in NULLS:
                record = parse(f)
            else:
                records = parse(f).findall(split_node)
                if len(records) < 1:
                    raise ValidationError("File contains no records")
                record = records[0]
        return ingestion.validate_xpath(record, xpath, int(instance), delimiter)
    except utils.FieldNotFound:
        raise ValidationError("Field not found")
    except utils.ValNotFound:
        raise ValidationError("Value not found")
