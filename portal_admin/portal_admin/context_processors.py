from django.contrib.auth.models import User
from .models import Context, Institution

page_titles = {
    'home': 'Home',
    'overview': 'Overview',
    'help': 'Help',
    'help_form': 'Help',
    'help_sent': 'Help',
    'error_logs': 'Error Logs',
    'error_log': 'Error Log',
    'delete_error_log': 'Delete Error Log',
    'update_institution': 'Update Institution',
    'update_users': 'Update Users',
    'update_locations': 'Update Locations',
    'reports': 'Reports',
    'password_change': 'Change Your Password',
    'password_change_done': 'Password Change Complete',
    'password_reset_confirm': 'Confirm Password Change',
    'password_reset_complete': 'Password Reset Complete',
    'login': 'Login',
    'select_context': 'Select Context',
    'logout': 'Logout',
    'deposit': 'Deposit',
    'create_profile': 'Create Profile',
    'profile_saved': 'Profile Saved',
    'upload': 'Upload',
    'uploaded': 'Uploaded',
    'upload_rules': 'Upload Rules',
    'validate_xpath': 'Validate XPath',
    'validate_separator': 'Validate Separator',
    'validate_filesource': 'Validate File Source',
    'browse': 'Browse',
    'tag': 'Tag',
    'add_tags': 'Add Tags',
    'batch_add_tags': 'Batch Add Tags',
    'facet_values': 'Browse Facet Values',
    'review_tags': 'Review Tags',
    'retag': 'Retag',
    'remove_tag': 'Remove Tag',
    'browse_values': 'Browse Values',
    'browse_by_value': 'Browse Values',
    'browse_records': 'Browse Records',
    'browse_records_by_format': 'Browse by Format',
    'browse_records_by_facet_value': 'Browse by Facet Value',
    'browse_records_by_facet_group': 'Browse by Facet Group',
    'browse_records_by_value': 'Browse by Value',
    'review_records': 'Review Records',
    'record': 'Record',
    'delete_record': 'Delete Record',
    'update_record': 'Update Record',
    'record_original_metadata': 'Original Metadata',
    'record_dcxml': 'Record DCXML',
    'delete_records': 'Delete Records',
    'records_deleted': 'Records Deleted',
    'connect': 'Connect',
    'connect_select_children': 'Select Children',
    'connect_confirm': 'Connection Confirmation',
    'connect_delete': 'Delete Connections',
    'preview': 'Preview',
    'preview_record': 'Preview Record',
    'preview_confirm': 'Preview Confirmation',
    'publish': 'Publish',
    'publish_record': 'Publish Record',
    'publish_confirm': 'Publish Confirmation',
    'reindex': 'Reindex',
    'jfu_upload': 'Upload',
    'jfu_delete': 'Delete'
}

def general_template_vars(request):
    path = request.get_full_path()
    page_title = 'Metadata Hopper'

    try:
        url_base = request.resolver_match.url_name
        if url_base in page_titles:
            page_title = 'Metadata Hopper | {}'.format(page_titles[url_base])
    except:
        url_base = ''

    user = request.user

    if 'context' in request.session:
        context = Context.objects.get(id=request.session['context'])
    else:
        context = ''

    if 'institution' in request.session:
        institution = Institution.objects.get(id=request.session['institution'])
    else:
        institution = ''

    return {
        'user': user,
        'path_no_params': request.path,
        'context': context,
        'institution': institution,
        'page_title': page_title,
        'url_base': url_base,
    }
