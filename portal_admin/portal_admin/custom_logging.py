from portal_admin.models import Context, Profile
import datetime

class CustomLogfileError(Exception):
    """
    An abstract error class.
    """
    pass

class RequiredFieldNotFound(CustomLogfileError):
    """
    Exception thrown when a required field does not exist in a record.
    """
    pass

class RequiredValueNotFound(CustomLogfileError):
    """
    Exception thrown when a field exists, but the value is not found.
    """
    pass

class XMLStructureInvalid(CustomLogfileError):
    """
    Exception thrown when the XML file contains invalid or malicious XML.
    """
    pass

class LocalContentNotFound(CustomLogfileError):
    """
    Exception thrown when local media content is not found, but should be.
    """
    pass

class InvalidRemoteContent(CustomLogfileError):
    """
    Exception thrown when remote media content is not found, but should be.
    """
    pass

class NoXMLFound(CustomLogfileError):
    """
    Exception thrown when no XML file is found in the archivist upload.
    """
    pass

class DuplicateFileFound(CustomLogfileError):
    """
    Exception thrown when a duplicate portal_admin.File is attempted.
    """
    pass

class InvalidSplitNode(CustomLogfileError):
    """
    Exception thrown when the split node is not found in the input file
    """
    pass

# ~~~ Top-Level Log & Error Functions

def _get_logfile_header(context_id, profile_id):
    """
    Returns a string, to be used as the header for logging.
    Is descriptive of the project, context, and provides contact information to the user.
    """

    context = Context.objects.get(id=context_id)
    profile = Profile.objects.get(id=profile_id)
    data = """
The following files were submitted to the [{}] by [{}] under the deposit profile [{}] on [{}].  They were not added to the portal for the reasons listed below.

You can use this file to correct and redeposit these records to the portal, using the same deposit profile.

IMPORTANT: when you make corrections, only edit the metadata records within this file; please do not edit other parts of this file.

Please contact [{}] if you need help redepositing these records.

========================
""".format(context.name, profile.institution.full_name, profile.name,
           datetime.datetime.now().strftime("%Y-%m-%d %H:%M"), context.help_email)
    return data


