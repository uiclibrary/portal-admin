from django import forms

from portal_admin.forms.base import BootstrapForm, BootstrapModelForm

from django.forms.models import modelformset_factory
from django.contrib.auth.models import User
from portal_admin.models import Location

class DateForm(BootstrapForm):
    start_date = forms.DateField()
    end_date = forms.DateField()

class ContactForm(BootstrapForm):
    message = forms.CharField(required=True, widget=forms.Textarea,
                              label="Message")

class UserUpdateForm(BootstrapModelForm):
    class Meta:
        model = User

class LocationUpdateForm(BootstrapModelForm):
    class Meta:
        model = Location

UserFormSet = modelformset_factory(User, form=UserUpdateForm, fields=('is_active',), extra=0)

LocationFormSet = modelformset_factory(Location, form=LocationUpdateForm, exclude=('institution',), extra=0)

