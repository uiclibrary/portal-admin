from django import forms

from django.contrib.auth.models import User
from portal_admin.models import Context, Institution, UserRole

from django.contrib.auth import authenticate

from portal_admin.forms.base import BootstrapForm

class LoginForm(BootstrapForm):
    username = forms.CharField(required=True)
    password = forms.CharField(widget=forms.PasswordInput, required=True)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        if username and password:
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    roles = UserRole.objects.filter(user=user)
                    if roles.count() > 0:
                        cleaned_data['user'] = user
                        cleaned_data['roles'] = roles
                        return cleaned_data
                    else:
                        raise forms.ValidationError("User is not associated with any institutions")
                else:
                    raise forms.ValidationError("User's account has been deactivated")
            else:
                raise forms.ValidationError("Invalid login")
        return cleaned_data

class SelectContextForm(BootstrapForm):
    user = forms.ModelChoiceField(queryset=User.objects.all(), required=True, widget=forms.HiddenInput())
    context = forms.ModelChoiceField(queryset=Context.objects.all(), 
                                     empty_label="Select a context", required=True)
    institution = forms.ModelChoiceField(queryset=Institution.objects.all().order_by('full_name'), 
                                         empty_label="Select an institution", required=True)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', False)
        super(SelectContextForm, self).__init__(*args, **kwargs)
        if user:
            self.initial['user'] = user
            roles = UserRole.objects.filter(user=user)
            self.fields['context'].queryset = Context.objects.filter(pk__in=roles.values_list('context', flat=True))
            if not UserRole.objects.filter(user=user, role__rolename="manager").exists():
                self.fields['institution'].queryset = Institution.objects\
                                                                 .filter(pk__in=roles.values_list('institution', flat=True))

    def clean(self):
        cleaned_data = super(SelectContextForm, self).clean()
        user = cleaned_data.get('user')
        institution = cleaned_data.get('institution')
        context = cleaned_data.get('context')
        if user and institution and context:
            if not UserRole.objects.filter(user=user, context=context, role__rolename="manager").exists() and \
               not UserRole.objects.filter(user=user, institution=institution, context=context).exists():
                raise forms.ValidationError("User does not have a role in that institution and context")
        return cleaned_data
