from django import forms

from portal_admin.fields import ObjectChoiceField, FieldChoiceField
from portal_admin.models import (
    DCXMLFormat,
    DigObject,
    DigObjectStatus,
    DigObjectForm,
    ValueElement,
    FacetValue,
    Context)

from portal_admin.forms.base import BootstrapForm
from django.core.urlresolvers import reverse, reverse_lazy

from django.forms.formsets import BaseFormSet
from django.forms.formsets import formset_factory


class FileForm(BootstrapForm):
    upload = forms.FileField()
    def __init__(self, *args, **kwargs):
        self.file_obj = kwargs.pop('file_obj')
        super(FileForm, self).__init__(*args, **kwargs)


class BaseFileFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.digobject = kwargs.pop('instance')
        self.extra = self.digobject.file_set.count()
        super(BaseFileFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, index, **kwargs):
        kwargs['file_obj'] = self.digobject.file_set.all()[index]
        return super(BaseFileFormSet, self)._construct_form(index, **kwargs)


FileFormSet = formset_factory(FileForm, formset=BaseFileFormSet)


class HiddenDigObjectsForm(forms.Form):
    objs = forms.ModelMultipleChoiceField(
        queryset=DigObject.objects.all(),
        widget=forms.MultipleHiddenInput)


class DigObjectGenericForm(BootstrapForm):

    status = forms.ModelChoiceField(
        queryset=DigObjectStatus.objects.all(),
        required=False,
        empty_label="Select status")
    type = forms.ChoiceField(
        choices=([('', "Select type")] + \
            list(DigObjectForm.objects.values_list('_type', 'dcxml_value').distinct())),
        required=False)

    field = FieldChoiceField(DCXMLFormat.objects.exclude(field_label=""),
        required=False,
        empty_label="Search all fields")
    predicate = forms.ChoiceField(
        choices=[
            ('element__istartswith', 'starts with'), \
            ('element__icontains', 'contains'),],
        widget=forms.RadioSelect,
        required=False,
        initial='element__icontains')
    q = forms.CharField(
        required=False,
        label="Find keywords in original metadata")

    def default_action(self):
        return reverse_lazy('browse_records')

    def submit_button_value(self):
        return 'Filter'


class DigObjectSearchForm(DigObjectGenericForm):
    '''
    Search records.
    '''

    def submit_button_value(self):
        return 'Search'


class DigObjectFilterForm(DigObjectGenericForm):
    '''
    Filter records.
    '''

    def submit_button_value(self):
        return 'Filter'


class ValueElementFilterForm(BootstrapForm):
    '''
    Filter Values (ImportedValue/ValueElements).
    '''

    # filters by object
    status = forms.ModelChoiceField(
        queryset=DigObjectStatus.objects.all(),
        required=False,
        empty_label="Select status")
    type = forms.ChoiceField(
        choices=([('', "Select type")] + \
            list(DigObjectForm.objects.values_list('_type', 'dcxml_value').distinct())),
        required=False)
    field = FieldChoiceField(DCXMLFormat.objects.exclude(field_label=""),
        required=False,
        empty_label="Search all fields")

    # filters by value
    predicate = forms.ChoiceField(
        choices=[
            ('element__istartswith', 'starts with'), \
            ('element__icontains', 'contains'),],
        widget=forms.RadioSelect,
        required=False,
        initial='element__icontains')
    q = forms.CharField(
        required=False,
        label="Find keywords in terms")

    def default_action(self):
        return reverse_lazy('browse_values')

    def submit_button_value(self):
        return 'Filter'


class FacetValueFilterForm(ValueElementFilterForm):
    '''
    Filter ValueElement (ImportedValue/ValueElements).
    '''

    # override predicate
    predicate = forms.ChoiceField(
        choices=[
            ('value__istartswith', 'starts with'), \
            ('value__icontains', 'contains'),],
        widget=forms.RadioSelect,
        required=False,
        initial='value__icontains')


class RecordForm(BootstrapForm):
    '''
    A full-page form for for filtering and sorting digital objects (records).
    '''
    items = forms.MultipleChoiceField(
        choices=[],
        widget=forms.CheckboxSelectMultiple,
        error_messages={
            'required': "Please select records",
            'invalid': "Please select valid records"},
        required=False)
    def __init__(self, request, *args, **kwargs):
        self.pages = kwargs.pop('pages')
        self.page = self.pages.page(kwargs.pop('page'))
        super(RecordForm, self).__init__(*args, **kwargs)

        self.request = request

        objs = self.page.object_list
        self.fields['items'].choices = []

        choices = self.fields['items'].choices
        context = Context.objects.get(pk = request.session['context'])
        for obj in objs:
            item = (obj.pk, {'name': obj.title,
                             'identifier': obj.local_identifier,
                             'description': obj.description,
                             'thumbnail': obj.thumb(),
                             'status': obj.get_status(context),
                             'url': reverse('record', args=[obj.pk]) })
            choices.append(item)

    def clean(self, *args, **kwargs):
        cleaned_data = super(RecordForm, self).clean(*args, **kwargs)

        if not self.request.POST.get('items') and not self.request.POST.get('pager_selections'):
            from django.core.exceptions import ValidationError
            raise ValidationError('Please select records.')

        return cleaned_data


class SelectValueElementsForm(BootstrapForm):
    '''
    A full-page for for filtering and sorting imported metadata.
    '''
    items = forms.MultipleChoiceField(
        choices=[],
        widget=forms.CheckboxSelectMultiple,
        error_messages={
            'required': "Please select records",
            'invalid': "Please select valid records"},
        required=True)
    action = forms.ChoiceField(
        choices=(
            ("", "Select an action"),
            ("delete", "Delete selected records"),
            ("review", "Review selected records"),
            ("add", "Add tags to selected records"),),
        error_messages={
            'required': "Please select an action",
            'invalid': "Please select a valid action"},
        required=True)

    def __init__(self, *args, **kwargs):
        self.pages = kwargs.pop('pages')
        self.page = self.pages.page(kwargs.pop('page'))

        # objects filtered for current session
        objs = kwargs.pop('objs')

        super(SelectValueElementsForm, self).__init__(*args, **kwargs)

        value_elements = self.page.object_list

        self.fields['items'].choices = []
        choices = self.fields['items'].choices

        for ve in value_elements:
            count = objs.filter(importedvalue__value=ve).count()

            choice = (ve.pk, {
                    'name': ve,
                    'count': count,
                    'url': ve.get_absolute_url()
                    })

            choices.append(choice)


class SelectFacetValuesForm(BootstrapForm):
    '''
    A full-page form for for filtering and sorting assigned facet value metadata (tags).
    '''
    items = forms.MultipleChoiceField(
        choices=[],
        widget=forms.CheckboxSelectMultiple,
        error_messages={
            'required': "Please select records",
            'invalid': "Please select valid records"},
        required=True)
    action = forms.ChoiceField(
        choices=(
            ("", "Select an action"),
            ("delete", "Delete selected records"),
            ("review", "Review selected records"),
            ("remove", "Remove current tag from selected records"),
            ("add", "Add tags to selected records"),),
        error_messages={
            'required': "Please select an action",
            'invalid': "Please select a valid action"},
        required=True)

    def __init__(self, *args, **kwargs):
        self.pages = kwargs.pop('pages')
        self.page = self.pages.page(kwargs.pop('page'))

        # objects filtered for current session
        objs = kwargs.pop('objs')

        super(SelectFacetValuesForm, self).__init__(*args, **kwargs)

        facet_values = self.page.object_list

        self.fields['items'].choices = []
        choices = self.fields['items'].choices

        for fv in facet_values:
            count = objs.filter(digobjectfacetvalue__facet_value=fv).count()

            choice = (fv.pk, {
                    'name': fv,
                    'count': count,
                    'url': fv.get_absolute_url()
                    })

            choices.append(choice)
