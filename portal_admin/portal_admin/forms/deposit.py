from django import forms

from django.forms.formsets import BaseFormSet
from django.forms.formsets import formset_factory

from portal_admin.fields import StripTextField, XPathTextField, XMLFileField

from portal_admin.forms.base import BootstrapForm

from portal_admin.models import (Profile,
                                 ElementSource,
                                 FileSource,
                                 Location,)

from portal_admin import validation

class BlankForm(forms.Form):
    pass

class ImageSubmissionForm(BootstrapForm):
    _help = {'image_submission': ""}
    image_submission = forms.ChoiceField(choices=((False, "Yes"), (True, "No"),))

class ImageMetadataForm(BootstrapForm):
    _help = {'record_separator': "Name the element that denotes the beginning and end of each record. You do not need to include brackets, just the text of the element name (e.g., record).",
             'image_filename': "The metadata file for each image must contain the file name of the corresponding image.",
             'image_suffix': 'If you use a suffix at the end of your filename, before the file extension, please enter it here. For example, if you use "-der" to indicate derivatives (filename-der.jpg), enter “-der” here.'}
    record_separator = XPathTextField(max_length=255, help_text=_help['record_separator'])
    image_filename = XPathTextField(max_length=255, help_text=_help['image_filename'], label="Source element")
    filename_instance = forms.ChoiceField(choices=[((i - 1), i,) for i in range(1, 9)], label="Instance")
    use_filename = forms.ChoiceField(choices=((False, "Value appears in metadata record"), 
                                              (True, "Use filename")), 
                                     widget=forms.RadioSelect)
    image_suffix = forms.CharField(max_length=15, help_text=_help['image_suffix'], required=False)
    has_extension = forms.ChoiceField(choices=(("", "Unspecified"), (False, "No"), (True, "Yes"),),
                                      label="Includes file extension", required=False)
    def __init__(self, *args, **kwargs):
        use_splitnode = kwargs.pop('use_splitnode', False)
        self.is_remote = kwargs.pop('is_remote', False)
        super(ImageMetadataForm, self).__init__(*args, **kwargs)
        if use_splitnode:
            self.fields['use_filename'].widget = forms.HiddenInput()
            self.fields['use_filename'].required = False
        else:
            self.fields['record_separator'].widget = forms.HiddenInput()
            self.fields['record_separator'].required = False
            self.fields['image_filename'].required = False

        if self.is_remote:
            self.fields['use_filename'].widget = forms.HiddenInput()
            self.fields['use_filename'].required = False
            self.fields['image_suffix'].widget = forms.HiddenInput()
            self.fields['image_suffix'].required = False
            self.fields['has_extension'].widget = forms.HiddenInput()
            self.fields['has_extension'].required = False
            
    def clean(self):
        cleaned_data = super(ImageMetadataForm, self).clean()
        # if we're not using the filename and image_filename is not filled out
        if cleaned_data.get('use_filename', 'True') == 'False':
            if not cleaned_data.get('image_filename', None):
                # get the errors associated with image_filename
                error_list = self.errors.get('image_filename')
                # if there aren't any create a blank list
                if error_list is None:
                    error_list = forms.util.ErrorList()
                    self.errors['image_filename'] = error_list
                # add an error message that the image filename is required
                error_list.append("You must enter a path to the image filename")
            if cleaned_data.get('has_extension', '') == '':
                error_list = self.errors.get('has_extension')
                if error_list is None:
                    error_list = forms.util.ErrorList()
                    self.errors['has_extension'] = error_list
                error_list.append("You must specify if the filenames in the metadata include file extensions.")
        return cleaned_data

    def save(self, profile):
        profile.split_node = self.cleaned_data['record_separator']
        profile.is_remote = self.is_remote
        profile.save()
        fs = FileSource(profile=profile,
                        xpath=self.cleaned_data['image_filename'],
                        use_filename=(self.cleaned_data['use_filename'] == 'True'),
                        order=0,
                        suffix=self.cleaned_data['image_suffix'],
                        instance_number=int(self.cleaned_data['filename_instance']),
                        has_extension=(self.cleaned_data['has_extension'] == 'True'))
        fs.save()
        return fs, profile

class LocationForm(BootstrapForm):
    location = forms.ModelChoiceField(queryset=Location.objects.all())

    def __init__(self, *args, **kwargs):
        institution = kwargs.pop('institution', False)
        super(LocationForm, self).__init__(*args, **kwargs)
        if institution:
            self.fields['location'].queryset = Location.objects.filter(institution=institution)

class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        for f in self.fields:
            self.fields[f].widget.attrs['class'] = 'form-control'
    class Meta:
        model = Profile
        fields = ['name', 'description', 'institution']
        widgets = {'institution': forms.HiddenInput()}

class SampleFileForm(BootstrapForm):
    sample_file = XMLFileField()

class UploadForm(BootstrapForm):
    upload_dir = forms.CharField(max_length=255, widget=forms.HiddenInput)

class ElementSourceForm(forms.Form):
    def __init__(self, *args, **kwargs):
        pf = kwargs.pop('profile_field')
        super(ElementSourceForm, self).__init__(*args, **kwargs)

        self.dcxml_format = pf.dcxml_format
        self.empty_permitted = not pf.dcxml_format.is_required

        if pf.use_complex_widget or pf.use_filename_widget:
            self.fields['use_xpath'] = forms.ChoiceField(choices=((True, "Value appears in metadata record"), 
                                                                  (False, "Use text below for value")), 
                                                         widget=forms.RadioSelect,
                                                         required=pf.dcxml_format.is_required)

        self.fields['xpath'] = XPathTextField(max_length=255,
                                              label=pf.source_label,
                                              required=pf.dcxml_format.is_required if pf.use_source_widget else False)
        if pf.use_complex_widget:
            self.fields['default'] = StripTextField(label="Default text",
                                                    widget=forms.Textarea,
                                                    required=False)

        if pf.use_attribute_widget:
            self.fields['attribute'] = StripTextField(max_length=255, 
                                                      help_text=pf.attribute_widget_hint,
                                                      required=False)
            if pf.use_attribute_value_widget:
                self.fields['attribute_value'] = StripTextField(max_length=255, 
                                                                help_text=pf.attribute_value_hint,
                                                                required=False)

        if pf.use_instance_widget:
            choices = [((i - 1), i,) for i in range(1, 9)]
            if pf.use_instance_all:
                choices.insert(0, (-1, "All"))
            self.fields['instance'] = forms.ChoiceField(choices=choices)

        if pf.use_delimiter_widget:
            self.fields['has_delimiter'] = forms.ChoiceField(choices=((False, "No"), (True, "Yes"),))
            self.fields['delimiter'] = StripTextField(max_length=45, required=False)

        for f in self.fields:
            self.fields[f].widget.attrs['class'] = 'form-control'

    def clean(self):
        try:
            return validation.validate_formdata(super(ElementSourceForm, self).clean(), not self.empty_permitted)
        except validation.ValidationError as e:
            raise forms.ValidationError(str(e))
            

    def save(self, profile):
        xpath = None
        default = None
        use_filename = False

        if self.cleaned_data.get('use_xpath', "True") == "True":
            xpath = validation.generate_xpath(self.cleaned_data.get('xpath'), 
                                              self.cleaned_data.get('attribute', None), 
                                              self.cleaned_data.get('attribute_value', None))
        elif "default" in self.cleaned_data:
            default = self.cleaned_data["default"]
        else:
            use_filename = True

        if xpath or default or use_filename:
            instance = int(self.cleaned_data.get('instance', -1))
            delimiter = self.cleaned_data.get('delimiter', None) if self.cleaned_data.get('has_delimiter', "False") == "True" else None
            element = ElementSource(xpath=xpath,
                                    dcxml_format=self.dcxml_format,
                                    profile=profile,
                                    instance_number=instance,
                                    delimiter=delimiter,
                                    default_text=default,
                                    use_filename=use_filename)

            element.save()
            return element
        else:
            return None

class BaseElementSourceFormSet(BaseFormSet):
    base_fields = {}

    def __init__(self, *args, **kwargs):
        self.profile_field = kwargs.pop('profile_field')
        self.extra = 1
        self.max_num = 10 if self.profile_field.can_add_widget else 1
        kwargs.pop('empty_permitted', False)
        super(BaseElementSourceFormSet, self).__init__(*args, **kwargs)
        self.base_fields = self.forms[0].base_fields

    def _construct_form(self, index, **kwargs):
        kwargs['profile_field'] = self.profile_field
        return super(BaseElementSourceFormSet, self)._construct_form(index, **kwargs)

    def save(self, profile):
        return [x for x in [form.save(profile) for form in self.forms] if not x == None]

ElementSourceFormSet = formset_factory(ElementSourceForm, formset=BaseElementSourceFormSet)

class BaseProfileFieldFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.profile_fields = kwargs.pop('profile_fields', [])
        self.extra = len(self.profile_fields)
        super(BaseProfileFieldFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, index, **kwargs):
        kwargs['profile_field'] = self.profile_fields[index]
        return super(BaseProfileFieldFormSet, self)._construct_form(index, **kwargs)

    def save(self, profile):
        return [x for x in [form.save(profile) for form in self.forms] if len(x) > 0]

ProfileFieldFormSet = formset_factory(ElementSourceFormSet, formset=BaseProfileFieldFormSet)
