from django import template

register = template.Library()

from django.db.models import Q

@register.assignment_tag(takes_context=True)
def is_admin(context):
    return context['user'].userrole_set.filter(context=context['context'])\
                                       .filter(Q(role__rolename="manager") |
                                               Q(role__rolename="localadmin", institution=context["institution"])).exists()
