from django import template
from django.template import Context

register = template.Library()

@register.tag(name="render_tips")
def render_tips(parser, token):
    try:
        tag_name, url = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires a single argument" % token.contents.split()[0])
    return RenderTipsNode(url)

class RenderTipsNode(template.Node):
    def __init__(self, url):
        self.url = template.Variable(url)

    def render(self, context):
        url_chunks = self.url.resolve(context)[1:-1].split("/")
        template_name = "_".join([x for x in url_chunks if not x.isdigit()])
        t = template.loader.select_template(["help/{}.html".format(template_name),'help/default.html'])
        return t.render(Context({}, autoescape=context.autoescape))
