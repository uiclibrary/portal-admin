# A sub-settings file, meant to be imported into the standard Django settings file.

# While the normal settings.py is intended for system-level settings, this file should
# be used to represent specific data and values to the application itself.

# Example: The state a server is hosted in, would belong in the standard settings file, represented perhaps by timezone information.
# However, the default state that an institution belongs to, would be expressed in this file.
import os

DEFAULT_STATE = 'IL'
DEFAULT_COUNTRY = 'United States'

# List of image-extensions, used to verify remote content is, indeed, an image.
# source : http://www.library.cornell.edu/preservation/tutorial/presentation/table7-1.html
ALLOWED_EXTS = {
    'images': ['gif', 'jpeg', 'jpg', 'jpx', 'fpx', 'pcd', 'png', 'jpf', 'jpe', 'bmp',
               'GIF', 'JPEG', 'JPG', 'JPX', 'FPX', 'PCD', 'PNG', 'JPF', 'JPE', 'BMP'],
    'archivalcollections': ['pdf'],
    }

# Define where additional files are stored based on the digobject_form
FILE_DEST = {
    'images': 'media/',
    'archivalcollections': 'data/'
}

# Define the size of created thumbnails and the maximum size for main images
THUMBNAIL_SIZE = 300, 300
IMAGE_SIZE_MAIN = 1600, 1200

# Location, on local server, where files are stored for XTF
# Override in local_settings.py
FILE_ROOT = "/opt/portal-data/"

# Location to upload sample files for rules creation
SAMPLE_FILE_ROOT = "/tmp/"

MEDIA_URL = '/media/'

# Trailing Cleanup - Remove these punctuation symbols, commonly found trailing in fields,
# example: <myTitle>What an awesome story, </myTitle>
TRAILING_CLEANUP = [',', ';']

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S",
        },
        'simple': { 'format': '%(levelname)s %(message)s' },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(os.path.dirname(os.path.dirname(__file__)), 'portal_admin.log'),
            'formatter': 'verbose',
        },
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'portal_admin': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
        'django.db.backends': {
            'handlers': ['null'],
            'propagate': False,
            'level': 'DEBUG',
        },
    },
}

LOGIN_REDIRECT_URL = '/overview/'
LOGIN_URL = '/accounts/login/'

PRODUCTION_SERVER = False

# Defines which manifest_formats include multiple records and should be split before processing
SPLITNODE_FORMATS = ["singleXML", "using a single XML file"]

# Define the dcxml_format that is the used to make Connections
DEFAULT_DCXMLFORMAT = {
    'dcxml_purpose': 'isPartOf',
    'element_name': 'relation'
}

# Defines the import backend for images
# LocalFileBackend will upload images to the media directory in the FILE_ROOT
# AmazonS3Backend will upload images to AmazonS3 using credentials defined in local_settings.py
IMPORT_TRANSFER_BACKEND = "portal_admin.import_backends.LocalFileBackend"

# Based on the element name of the dcxml_format associated with an ImportedValue
# the autotagger can be overriden.  By default override "Names"
AUTOTAGGERS = {"creator": "portal_admin.autotaggers.NamesAutotagger"}

# Associate the names of the indexes with the directories that are used to create
# the index in the XTF installation
INDEX_NAMES = {'Previewed': 'preview',
               'Published': 'publish'}

EMAIL_NOTIFICATIONS = False

USE_EZID = False
