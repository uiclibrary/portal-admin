import datetime

from portal_admin import utils

from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse

from django.template.defaultfilters import truncatechars  # or truncatewords

from xml.etree.ElementTree import tostring, Element, SubElement
from xml.dom import minidom
from django.db.models import Min

def add_dcxml_element(root, element_name, text, purpose=None):
        x = SubElement(root, element_name)
        if purpose:
            x.attrib['purpose'] = purpose
        x.text = str(text)

class Role(models.Model):
    _help = {
        'rolename': '',
        'digobjectforms': 'Defines which DigObjectForms users with this role may make Profiles with',
    }

    rolename = models.CharField(max_length=45, null=False, default='', help_text=_help['rolename'])
    digobjectforms = models.ManyToManyField('DigObjectForm', null=True, blank=True, help_text=_help['digobjectforms'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.rolename

class UserRole(models.Model):
    _help = {
        'role': '',
        'user': '',
        'institution': '',
        'context': '',
    }

    role = models.ForeignKey('Role', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['role'])
    user = models.ForeignKey('auth.User', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['user'])
    institution = models.ForeignKey('Institution', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['institution'])
    context = models.ForeignKey('Context', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['context'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "{}, {} with {} in {} Context.".format(str(self.user),
            str(self.role), str(self.institution), str(self.context))

class Institution(models.Model):
    _help = {
        'full_name': 'Public display: brief record',
        'xtf_name': 'Shorthand used for XTF directory structure',
        'facet_name': 'Public display: facet value for limiting by institution',
        'url': 'Public display: used to link end-user to institution website',
    }

    full_name = models.CharField(max_length=255, null=False, default='', help_text=_help['full_name'])
    xtf_name = models.CharField(max_length=45, null=False, default='', help_text=_help['xtf_name'])
    facet_name = models.CharField(max_length=45, null=False, default='', help_text=_help['facet_name'])
    url = models.CharField(max_length=255, null=False, default='', help_text=_help['url'])

    def create_dcxml(self):
        root = Element('dc')

        add_dcxml_element(root, 'full_name', self.full_name)
        add_dcxml_element(root, 'facet_name', self.facet_name)
        add_dcxml_element(root, 'xtf_name', self.xtf_name)
        for l in self.location_set.all():
            add_dcxml_element(root, 'location', l.pk)

        filename = utils.brand_path(self.xtf_name) + self.xtf_name + ".dc.xml"
        with open(filename, 'w', encoding="utf-8") as f:
            data = minidom.parseString(tostring(root, 'utf-8')).toprettyxml(indent="\t")
            f.write(data)
        return filename

    def save(self, *args, **kwargs):
        super(Institution, self).save(*args, **kwargs)
        self.create_dcxml()

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.full_name

class Location(models.Model):
    _help = {
        'name': '',
        'url': 'Public display: used to link end-user to location website',
        'phone': '',
        'email': '',
        'web_contact_form': '',
        'institution': '',
        'street_address': 'The complete street address of the location, without city, state, country, or zipcode',
        'city': '',
        'state': 'Default value should be IL.  Should not display to public unless value is non-default',
        'country': 'Default value should be United States.  Should not display to public unless value is non-default',
        'zipcode': 'The use case is not for mailing, it is for visiting in person',
        }

    name = models.CharField(max_length=255, null=False, default='', help_text=_help['name'])
    url = models.CharField(max_length=255, null=False, default='', help_text=_help['url'])
    phone = models.CharField(max_length=45, null=True, blank=True, help_text=_help['phone'])
    email = models.EmailField(max_length=45, null=True, blank=True, help_text=_help['email'])
    web_contact_form = models.CharField(max_length=255, null=True, blank=True, help_text=_help['web_contact_form'])
    institution = models.ForeignKey('Institution', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['institution'])
    street_address = models.TextField(null=False, default='', help_text=_help['street_address'])
    city = models.CharField(max_length=255, null=False, default='', help_text=_help['city'])
    state = models.CharField(max_length=255, null=False, default=settings.DEFAULT_STATE, help_text=_help['state'])
    country = models.CharField(max_length=255, null=False, default=settings.DEFAULT_COUNTRY, help_text=_help['country'])
    zipcode = models.CharField(max_length=255, null=True, blank=True, help_text=_help['zipcode'])

    def create_dcxml(self):
        root = Element('dc')

        add_dcxml_element(root, 'publisher', self.name, 'location_name')
        add_dcxml_element(root, 'publisher', self.url, 'location_url')
        add_dcxml_element(root, 'publisher', self.phone, 'location_phone')
        add_dcxml_element(root, 'publisher', self.email, 'location_email')
        add_dcxml_element(root, 'publisher', self.web_contact_form, 'location_web_contact_form')
        add_dcxml_element(root, 'publisher', self.institution.full_name, 'location_institution_name')
        add_dcxml_element(root, 'publisher', self.street_address, 'location_street_address')
        add_dcxml_element(root, 'publisher', self.city, 'location_city')
        add_dcxml_element(root, 'publisher', self.state, 'location_state')
        add_dcxml_element(root, 'publisher', self.country, 'location_country')
        add_dcxml_element(root, 'publisher', self.zipcode, 'location_zipcode')

        filename = utils.location_path(self.institution.xtf_name) + str(self.pk) + ".dc.xml"
        with open(filename, 'w', encoding="utf-8") as f:
            data = minidom.parseString(tostring(root, 'utf-8')).toprettyxml(indent="\t")
            f.write(data)
        return filename

    def save(self, *args, **kwargs):
        is_new = self.pk is None
        super(Location, self).save(*args, **kwargs)
        self.create_dcxml()
        if is_new:
            self.institution.create_dcxml()

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.name

class Context(models.Model):
    _help = {
        'name': '',
        'description': 'This displays to the admin user to explain which context are available to work with',
        'help_email': '',
        'preview_url': '',
        'publish_url': '',
        'naan': 'note: we possibly will not use this in constructing arks'
        }

    name = models.CharField(max_length=255, null=False, default='', help_text=_help['name'])
    description = models.TextField(null=True, blank=True, help_text=_help['description'])
    help_email = models.EmailField(max_length=45, null=True, blank=True, help_text=_help['help_email'])
    preview_url = models.CharField(max_length=255, null=True, blank=True, help_text=_help['preview_url'])
    publish_url = models.CharField(max_length=255, null=True, blank=True, help_text=_help['publish_url'])
    naan = models.CharField(max_length=45, null=True, blank=True, help_text=_help['naan'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.name

class InstitutionContext(models.Model):
    _help = {
        'institution': '',
        'context': '',
        'member_type': '',
        'google_snippet': '',
        'main_contact_name': 'An administrative contact person; likely the person listed on an MOU, and not necessarily someone who logs in',
        'main_contact_email': '',
        'main_contact_phone': '',
        'use_auto_classify': 'Shows whether or not the institution has decided to  opt-in to auto-classifying for facets.  Default is true'
        }

    institution = models.ForeignKey('Institution', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['institution'])
    context = models.ForeignKey('Context', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['context'])
    member_type = models.CharField(max_length=255, null=False, blank=True, default='', help_text=_help['member_type'])
    google_snippet = models.TextField(null=True, blank=True, help_text=_help['google_snippet'])
    main_contact_name = models.CharField(max_length=255, null=True, blank=True, help_text=_help['main_contact_name'])
    main_contact_email = models.EmailField(max_length=45, null=True, blank=True, help_text=_help['main_contact_email'])
    main_contact_phone = models.CharField(max_length=45, null=True, blank=True, help_text=_help['main_contact_phone'])
    use_auto_classify = models.BooleanField(default=True, help_text=_help['use_auto_classify'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "{} in {} Context.".format(str(self.institution), str(self.context))

class ProfileField(models.Model):
    _help = {
        'digobject_form': '',
        'dcxml_format': '',
        'use_source_widget': 'Indicates a text entry box for the field name of this element.  Default is true',
        'source_label': 'The label that displays for the source_widget text box',
        'use_complex_widget': 'A complex widget, used in place of source_widget.\nThe archivist has the option of drawing the value from a metadata field or entering default text.  Default is false',
        'use_instance_widget': 'If true, places a number drop-down select to indicate instance on form.  Default is false',
        'use_instance_all': 'If true, use all occurances of this element from the original source.',
        'use_filename_widget': 'Checkbox, use the filename as the source of data rather than the input form.  Default is false',
        'can_add_widget': 'If true, gives the user the option to add another source field for extracting data.  Default is false',
        'use_delimiter_widget': 'If true, enables the user to provide a delimiter for multiple values within a field.  Default is false',
        'field_order': 'Determines the order of field display on form',
        'help_text': 'Source of onscreen hints within form',
        'use_attribute_widget': 'Determines if an attribute will be requested.  Default is false',
        'attribute_hint': 'Help text below attribute widget',
        'use_attribute_value_widget': 'Determines if an attribute value will be requested.  Default is false',
        'attribute_value_hint': 'Help text below attribute value widget',
        }

    digobject_form = models.ForeignKey('DigObjectForm', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['digobject_form'])
    dcxml_format = models.ForeignKey('DCXMLFormat', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['dcxml_format'])
    use_source_widget = models.BooleanField(default=True, help_text=_help['use_source_widget'])
    source_label = models.CharField(max_length=45, null=False, default='', help_text=_help['source_label'])
    use_complex_widget = models.BooleanField(default=False, help_text=_help['use_complex_widget'])
    use_instance_widget = models.BooleanField(default=False, help_text=_help['use_instance_widget'])
    use_instance_all = models.BooleanField(default=False, help_text=_help['use_instance_all'])
    use_filename_widget = models.BooleanField(default=False, help_text=_help['use_filename_widget'])
    can_add_widget = models.BooleanField(default=False, help_text=_help['can_add_widget'])
    use_delimiter_widget = models.BooleanField(default=False, help_text=_help['use_delimiter_widget'])
    field_order = models.IntegerField(null=False, default=-1, help_text=_help['field_order'])
    help_text = models.TextField(null=True, blank=True, help_text=_help['help_text'])
    use_attribute_widget = models.BooleanField(default=False, help_text=_help['use_attribute_widget'])
    attribute_widget_hint = models.CharField(max_length=45, null=True, blank=True, help_text=_help['attribute_hint'])
    use_attribute_value_widget = models.BooleanField(default=False, help_text=_help['use_attribute_value_widget'])
    attribute_value_hint = models.CharField(max_length=45, null=True, blank=True, help_text=_help['attribute_value_hint'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "{} for {}.".format(self.dcxml_format.field_label, str(self.digobject_form))

class FileSource(models.Model):
    _help = {
        'profile': '',
        'xpath': '',
        'use_filename': '',
        'order': '',
        'suffix': '',
        'instance_number': '',
        'has_extension': '',
        }

    profile = models.ForeignKey('Profile', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['profile'])
    xpath = models.CharField(max_length=255, default='', null=False, help_text=_help['xpath'])
    use_filename = models.BooleanField(default=False, help_text=_help['use_filename'])
    order = models.IntegerField(null=True, blank=True, help_text=_help['order'])
    suffix = models.CharField(max_length=15, null=True, blank=True, help_text=_help['suffix'])
    instance_number = models.IntegerField(null=True, blank=True, help_text=_help['instance_number'])
    has_extension = models.BooleanField(default=False, help_text=_help['has_extension'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "source xpath: {}, for Profile {}".format(self.xpath, str(self.profile))

class Profile(models.Model):
    _help = {
        'name': 'Displays to the user to help select the correct rules for deposit',
        'description': 'Describe your import mapping rules.',
        'institution': '',
        'is_remote': 'Indicates whether or not a digital object is on a remote server.  Default is false',
        'split_node': 'Where to split the metadata file into separate records',
        'digobject_form': '',
        'use_metadata_filename': 'Tells the system to use the metadata filename in order to find the media filename.  Default is false',
        }

    name = models.CharField(max_length=255, null=False, default='', help_text=_help['name'])
    description = models.TextField(null=False, default='', help_text=_help['description'])
    institution = models.ForeignKey('Institution', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['institution'])
    is_remote = models.BooleanField(default=False, help_text=_help['is_remote'])
    split_node = models.CharField(max_length=45, null=True, blank=True, help_text=_help['split_node'])
    digobject_form = models.ForeignKey('DigObjectForm', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['digobject_form'])
    use_metadata_filename = models.BooleanField(default=False, help_text=_help['use_metadata_filename'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.name

class DCXMLFormat(models.Model):
    _help = {
        'dcxml_purpose': '',
        'element_name': '',
        'field_label': 'Example: Names',
        'is_required': '',
        'use_dcxml_element': '',
        'concat_order': 'The order in which to assemble elements of this type, null if only one element',
        'separator': '',
        }

    dcxml_purpose = models.CharField(max_length=45, null=True, blank=True, help_text=_help['dcxml_purpose'])
    element_name = models.CharField(max_length=45, null=False, default='', help_text=_help['element_name'])
    field_label = models.CharField(max_length=45, null=True, blank=True, help_text=_help['field_label'])
    is_required = models.BooleanField(default=False, help_text=_help['is_required'])
    use_dcxml_element = models.BooleanField(default=False, help_text=_help['use_dcxml_element'])
    concat_order = models.IntegerField(null=True, blank=True, help_text=_help['concat_order'])
    separator = models.CharField(max_length=1, null=True, blank=True, help_text=_help['separator'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        if self.concat_order != None:
            s = "DCXML {}[{}]".format(self.element_name, self.concat_order)
        else:
            s = "DCXML {}".format(self.element_name)
        if self.dcxml_purpose:
            s += ": {}".format(self.dcxml_purpose)
        return s

    def get_records_url(self):
        #  http://localhost:8000/browse/values/13
        return reverse('browse_records_by_format', args=[str(self.id)])
    def get_absolute_url(self):
        # http://localhost:8000/records/formats/13/
        return reverse('browse_by_value', args=[str(self.id)])

class ElementSource(models.Model):
    _help = {
        'profile': '',
        'dcxml_format': '',
        'xpath': 'This is the path in the metadata file.  This is what the archivist types in the form when creating import rules',
        'instance_number': '',
        'delimiter': 'The delimiter that this field should be split on',
        'default_text': 'In case the user chose to have default text instead of extracting values for an element',
        'use_filename': 'Indicates if the imported value should be created using the filename'
        }

    profile = models.ForeignKey('Profile', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['profile'])
    dcxml_format = models.ForeignKey('DCXMLFormat', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['dcxml_format'])
    xpath = models.CharField(max_length=255, null=True, blank=True, help_text=_help['xpath'])
    instance_number = models.IntegerField(null=True, blank=True, help_text=_help['instance_number'])
    delimiter = models.CharField(max_length=45, null=True, blank=True, help_text=_help['delimiter'])
    default_text = models.TextField(null=True, blank=True, help_text=_help['default_text'])
    use_filename = models.BooleanField(default=False, help_text=_help['use_filename'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "Element {}, of format {}".format(self.xpath, str(self.dcxml_format))

class ImportedValue(models.Model):
    _help = {
        'value': '',
        'digobject': '',
        'dcxml_format': '',
        }

    value = models.ForeignKey('ValueElement', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['value'])
    digobject = models.ForeignKey('DigObject', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['digobject'])
    dcxml_format = models.ForeignKey('DCXMLFormat', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['dcxml_format'])

    @property
    def short_value(self):
        return truncatechars(self.value, 100)

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "{}, for DigObject {}.".format(self.value, str(self.digobject))

class ValueElement(models.Model):
    '''
    Normalization of ImportedValue values.
    '''
    _help = {
        'value': 'Example: "Architects - Chicago (Ill) - Homes and Haunts"',
    }
    element = models.TextField(null=False, default='', help_text=_help['value'])

    def __str__(self):
        return self.element
    def get_absolute_url(self):
        return reverse('browse_records_by_value', args=[str(self.id)])


class FacetGroup(models.Model):
    '''
    Groups of FacetValues.  Equates to tabs on batch add tag page.
    '''
    _help = {
        'label': '',
        'dcxml_format': '',
        'context': '',
        'is_openedit': '',
        }

    label = models.CharField(max_length=45, null=False, default='', help_text=_help['label'])
    dcxml_format = models.ForeignKey('DCXMLFormat', null=True, blank=True, on_delete=models.PROTECT, help_text=['dcxml_format'])
    context = models.ForeignKey('Context', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['context'])
    is_openedit = models.BooleanField(default=False, help_text=_help['is_openedit'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.label

    def get_records_url(self):
        return reverse('browse_records_by_facet_group', args=[str(self.id)])
    def get_absolute_url(self):
        return reverse('browse_by_tag', args=[str(self.id)])

class FacetValue(models.Model):
    _help = {
        'value': 'Example: Architecture',
        'facet_group': '',
        }
    class Meta:
        ordering = ['value', 'id']
        unique_together = ('facet_group', 'value')

    value = models.CharField(
        max_length=255,
        null=False,
        default='',
        help_text=_help['value'])
    facet_group = models.ForeignKey('FacetGroup',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        help_text=_help['facet_group'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.value
    def get_absolute_url(self):
        return reverse('browse_records_by_facet_value', args=[str(self.id)])

class DigObjectFacetValue(models.Model):
    _help = {
        'digobject': '',
        'facet_value': '',
        'is_autoassigned': 'Indicates whether the system or the archivist assigned the facetvalue.  Default is false',
        'trigger_term': "Show rule that triggered facet being added. This is optional as users can add facets manually",
        }

    digobject = models.ForeignKey('DigObject',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        help_text=_help['digobject'])
    facet_value = models.ForeignKey('FacetValue',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        help_text=_help['facet_value'])
    is_autoassigned = models.BooleanField(
        default=False,
        help_text=_help['is_autoassigned'])
    trigger_term = models.ForeignKey('TriggerTerm',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        help_text=_help['trigger_term'])
    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "{} for DigObject {}.".format(str(self.facet_value), str(self.digobject))

class TriggerTerm(models.Model):
    _help = {
        'term': 'This identifies the term to be found in ImportedValue.imported_value.  Wildcards would be permitted.  Example: architects',
        'dcxml_format': '',
        'facet_value': 'Example: may point to 272, for Architecture',
        'match_type': 'Determines how query will match search terms to imported values',
        'context': 'Each context may have its own automatic classification rules',
        'is_active': "",
        }

    term = models.CharField(max_length=255, null=False, default='', help_text=_help['term'])
    dcxml_format = models.ForeignKey('DCXMLFormat', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['dcxml_format'])
    facet_value = models.ForeignKey('FacetValue', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['facet_value'])
    match_type = models.ForeignKey('MatchType', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['match_type'])
    context = models.ForeignKey('Context', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['context'])
    is_active = models.BooleanField(default=True, help_text=_help['is_active'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "{} {} \"{}\"".format(self.facet_value.facet_group.label, self.match_type.query, self.term)

class MatchType(models.Model):
    _help = {
        'query': "",
    }
    query = models.CharField(max_length=45, null=False, default='', help_text=_help['query'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.query

class DigObject(models.Model):
    _help = {
        'ark': '',
        'title': 'Concatenation of Profile.main_source, Profile.title_date, and Profile.subtitle',
        'profile': '',
        'digobject_form': '',
        'location': '',
        }

    ark = models.CharField(
        max_length=45,
        null=False,
        default='',
        help_text=_help['ark'])
    title = models.CharField(
        max_length=255,
        null=False,
        default='',
        help_text=_help['title'])
    profile = models.ForeignKey('Profile',
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        help_text=_help['profile'])
    digobject_form = models.ForeignKey('DigObjectForm',
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        help_text=_help['digobject_form'])
    location = models.ForeignKey('Location',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        help_text=_help['location'])

    def local_identifier(self):
        ids = self.importedvalue_set.filter(dcxml_format__element_name='identifier')
        return ids[0] if ids.count() > 0 else None

    def description(self):
        descs = self.importedvalue_set.filter(dcxml_format__element_name='description')
        return descs[0] if descs.count() > 0 else None

    def remove_from_context(self, context):
        DigObjectContext.objects.get(digobject=self, context__pk=context).delete()

    # This delete is called in task.py where associated files are also deleted and a reindex is triggered.
    # It should NEVER be called elsewhere as the files won't be deleted and the index will not match the
    # state in hopper.
    def delete(self):
        # If this DigObject lives in only one context go ahead and delete it
        # otherwise just delete it from this context. ev 2014-09-10
        if self.digobjectcontext_set.count() == 0:
            # create the vestige of this DigObject that was.
            deleted_obj = DeletedDigObject.objects.create(ark=self.ark, title=self.title)

            # If there is a local id keep a record of it
            for localid in self.importedvalue_set.filter(dcxml_format__element_name="identifier"):
                DeletedImportedValue.objects.create(deleted_digobject=deleted_obj,
                                                    local_id=localid.value )

            # Before deleting the digobject itself
            # delete all the related materials
            self.file_set.all().delete()
            self.event_set.all().delete()
            self.importedvalue_set.all().delete()
            self.digobjectfacetvalue_set.all().delete()
            super(DigObject, self).delete()
            return deleted_obj
        else:
            return self

    def add_child(self, child_dig_object):
        c = Connection(parent=self, child=child_dig_object)
        # make sure this connection goes through it's full validation
        # process to detect illegal connections
        c.full_clean()
        c.save(force_insert=True)
        return c

    def add_parent(self, parent_dig_object):
        c = Connection(parent=parent_dig_object, child=self)
        # make sure this connection goes through it's full validation
        # process to detect illegal connections
        c.full_clean()
        c.save(force_insert=True)
        return c

    def get_children(self):
        connections = Connection.objects.filter(parent=self)
        return DigObject.objects.filter(parent_set__in=connections).distinct()

    def get_parents(self):
        connections = Connection.objects.filter(child=self)
        return DigObject.objects.filter(child_set__in=connections).distinct()

    def get_connection(self, other_object):
        child_connection = Connection.objects.get(child=self, parent=other_object)
        parent_connection = Connection.objects.get(child=other_object, parent=self)

        if child_connection and parent_connection:
            raise self.MultipleObjectsReturned('DigObject {} has multiple connections to the same object.'.format(self.pk))
        else:
            return parent_connection
            return child_connection

    def abandon_parent(self, parent_dig_object):
        Connection.objects.get(child=self, parent=parent_dig_object).delete()

    def abandon_child(self, child_dig_object):
        Connection.objects.get(parent=self, child=child_dig_object).delete()

    def get_status(self, context):
        return DigObjectContext.objects.get(digobject=self, context=context).digobject_status

    def set_status(self, context_id, status, meta_event):
        '''
        Changing the status of a digobject creates a new directory tree in settings.FILE_ROOT,
        with matching symlinks pointing to the original files held in the original root (FILE_ROOT/Deposited/).
        '''
        my_context = DigObjectContext.objects.get(digobject=self, context__pk=context_id)
        old_status = my_context.digobject_status

        # alter status
        my_context.digobject_status = status
        my_context.save(update_fields=['digobject_status'])

        # create relevant directory structure
        path = utils.digobj_path(self.id, context_id)
        utils.ensure_dirs(path)

        # get or create related event and attach to meta_event
        log_text = 'The status of this digital object was successfully changed to "{}".  Previously its status was "{}".'\
            .format(status.status, old_status.status)
        event_type, created = EventType.objects.get_or_create(
            name='Status Change: {}'.format(status.status),
            log_name=None,
            log_help_text=log_text)
        event = Event.objects.create(
            meta_event=meta_event,
            event_type=event_type,
            digobject=self)

    def remove_tag(self, facet_value):
        '''Remove a tag from this record.'''
        dofv = DigObjectFacetValue.objects.filter(
            digobject=self,
            facet_value=facet_value)
        dofv.delete()

    def thumb(self):
        if self.digobject_form._type == "images":
            return "{}/{}/{}/thumbs/{}.jpg".format(
                self.digobject_form._type,
                self.profile.institution.xtf_name,
                self.profile.id,
                self.ark)
        else:
            return False

    def image(self):
        if self.digobject_form._type == "images":
            return "{}/{}/{}/{}.jpg".format(
                self.digobject_form._type,
                self.profile.institution.xtf_name,
                self.profile.id,
                self.ark)
        else:
            return False

    def create_dcxml(self, context_id):
        deposited = DigObjectStatus.objects.get(status="Deposited")
        deposited_path = utils.digobj_path(self.pk, context_id, deposited.id)

        root = Element('dc')

        add_dcxml_element(root, 'title', self.title, 'title')
        add_dcxml_element(root, 'identifier', self.ark, 'primary_identifier')

        for imported_value in ImportedValue.objects.filter(digobject=self, dcxml_format__use_dcxml_element=True):
            add_dcxml_element(root,
                              imported_value.dcxml_format.element_name,
                              imported_value.value.element,
                              imported_value.dcxml_format.dcxml_purpose)

        add_dcxml_element(root, 'publisher', self.profile.institution.full_name, 'institution_name')
        add_dcxml_element(root, 'publisher', self.profile.institution.url, 'institution_url')
        add_dcxml_element(root, 'publisher', self.location.pk, 'location')
        add_dcxml_element(root, 'type', self.digobject_form.dcxml_value, 'types')
        add_dcxml_element(root, 'format', self.digobject_form._format, 'original_metadata_format')

        for dfv in DigObjectFacetValue.objects.filter(digobject=self,
                                                         facet_value__facet_group__context__id=context_id):
            dformat = dfv.facet_value.facet_group.dcxml_format
            value = dfv.facet_value.value
            add_dcxml_element(root, dformat.element_name, value, dformat.dcxml_purpose)
            add_dcxml_element(root, dformat.element_name,
                              "{}::{}".format(value[:1].upper(), value),
                              dformat.dcxml_purpose + "_alpha")

        for c in self.parent_set.all():
            add_dcxml_element(root, c.dcxml_format.element_name, c.parent.ark, c.dcxml_format.dcxml_purpose)

        for d in DigObjectForm.objects.filter(pk__in=self.child_set.values_list('child__digobject_form', flat=True).distinct()):
            add_dcxml_element(root, 'relation', "{}:{}".format(d._type, d._format), 'hasPart')

        date_published = self.event_set.filter(event_type__name="Status Change: Published")\
                                       .aggregate(m=Min('meta_event__datestamp'))
        # if there is a published object use that date else use now because we won't add the status
        # change event until after indexing. EV 8/26/2015
        d = date_published['m'].date() if date_published['m'] else datetime.date.today()
        add_dcxml_element(root, 'date', d, 'date_published')

        utils.ensure_dirs(deposited_path)

        filename = deposited_path.format('data') + "{}.dc.xml".format(self.ark)
        dc_xml_file = open(filename, 'w', encoding="utf-8")
        data = minidom.parseString(tostring(root, 'utf-8')).toprettyxml(indent="\t")
        dc_xml_file.write(data)
        dc_xml_file.close()
        return filename

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.title
    def get_absolute_url(self):
        return reverse('record', args=[str(self.id)])


class DeletedDigObject(models.Model):
    _help = {
        'ark' : '',
        'title': '',
        'event': '',
        }

    ark = models.CharField(max_length=45, null=False, default='', help_text=_help['ark'])
    title = models.CharField(max_length=255, null=False, default='', help_text=_help['title'])
    event = models.ForeignKey('Event', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['event'])
    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.title

class DeletedImportedValue(models.Model):
    _help = {
        'deleted_digobject': '',
        'local_id' : 'The value for this field comes from ImportedValue.imported_value where dcxml_purpose == "local_identifier"',
        }

    deleted_digobject = models.ForeignKey('DeletedDigObject', null=True, blank=True, on_delete=models.PROTECT,
                                          help_text=_help['deleted_digobject'])
    local_id = models.TextField(null=False, default='', help_text=_help['local_id'])

class File(models.Model):
    _help = {
        'filename_original': '',
        'filename_new': 'This is the DigObject.ark, along with any needed file extension',
        'digobject': '',
        'checksum': '',
        'file_order': 'If multiple files (for example, a book), this field indicates the order the files should be displayed.',
        }

    filename_original = models.CharField(max_length=255, null=True, blank=True, help_text=_help['filename_original'])
    filename_new = models.CharField(max_length=45, null=True, blank=True, help_text=_help['filename_new'])
    digobject = models.ForeignKey('DigObject', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['digobject'])
    checksum = models.CharField(max_length=255, null=False, default='', help_text=_help['checksum'])
    file_order = models.IntegerField(null=True, blank=True, help_text=_help['file_order'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.filename_original

class Path(models.Model):
    _help = {
        'paths': '',
        'context': '',
        'digobject_status': '',
        }

    paths = models.CharField(max_length=255, null=False, default='', help_text=_help['paths'])
    context = models.ForeignKey('Context', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['context'])
    digobject_status = models.ForeignKey('DigObjectStatus', null=False, blank=False, on_delete=models.PROTECT,
                                         help_text=_help['digobject_status'])
    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.paths

class DigObjectContext(models.Model):
    _help = {
        'digobject': '',
        'context': '',
        'digobject_status': '',
        'dirty': '',
        }

    digobject = models.ForeignKey('DigObject', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['digobject'])
    context = models.ForeignKey('Context', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['context'])
    digobject_status = models.ForeignKey('DigObjectStatus', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['digobject_status'])
    dirty = models.BooleanField(default=False, help_text=_help['dirty'])

    def set_dirty(self, value):
        self.dirty = value
        self.save(update_fields=['dirty'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "{} in {} Context.".format(str(self.digobject), str(self.context))

class DigObjectStatus(models.Model):
    class Meta:
        verbose_name_plural = "dig object statuses"

    _help = {
        'status': '',
        }

    status = models.CharField(max_length=45, null=False, default='', help_text=_help['status'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.status

class DigObjectForm(models.Model):
    _help = {
        '_format': 'XTF indexing type.  For release one, archival collections or images',
        '_type': 'This is the specific metadata standard used.  For example, EAD',
        'use_manifest': 'Specify wheter or not a manifest needs to be uploaded',
        'manifest_format': 'If use_manifest is true, then this must have a value',
        'use_media_folder': '',
        'dcxml_value': '',
        'can_be_parent': 'Whether a record connected to another record will be allowed to have children.',
        'can_be_child': 'Whether a record connected to another record will be allowed to have parents.',
        }

    _format = models.CharField(max_length=45, null=False, default='', help_text=_help['_format'])
    _type = models.CharField(max_length=45, null=False, default='', help_text=_help['_type'])
    use_manifest = models.BooleanField(default=False, help_text=_help['use_manifest'])
    manifest_format = models.CharField(max_length=45, null=True, blank=True, help_text=_help['manifest_format'])
    use_media_folder = models.BooleanField(default=False, help_text=_help['use_media_folder'])
    dcxml_value = models.CharField(max_length=45, null=False, default='', help_text=_help['dcxml_value'])
    can_be_parent = models.BooleanField(default=False, help_text=_help['can_be_parent'])
    can_be_child = models.BooleanField(default=True, help_text=_help['can_be_child'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        if self.use_manifest:
            return "{}: {}/{}".format(self.dcxml_value, self._format, self.manifest_format)
        return "{}: {}".format(self.dcxml_value, self._format)

class Connection(models.Model):

    _help = {
        'parent': 'A digital object considered a parent to the digital object specified in the "child" field',
        'child': 'A digital object considered a child to the digital object specified in the "parent" field',
        'element_name': 'The dc.xml name',
        'dcxml_format': 'The purpose for this relation.' }
    class Meta:
        unique_together = (("parent", "child"),)

    def validate_parent(pk):
        parent_obj = DigObject.objects.get(pk=pk)
        # Don't allow connections with objects of formats
        # which can't be parents
        if parent_obj.digobject_form.can_be_parent != True:
            raise ValidationError(
                'DigObject (id {}) cannot be a parent.'.format(pk) )

    def validate_child(pk):
        child_obj = DigObject.objects.get(pk=pk)
        # Don't allow connections with objects of formats
        # which can't be children
        if child_obj.digobject_form.can_be_child != True:
            raise ValidationError(
                'DigObject (id {}) cannot be a child.'.format(pk) )

    def clean(self):
        # Don't allow objects to connect to themselves
        if self.parent == self.child:
            raise ValidationError('Objects cannot be connected with themselves.')
        # Enforce uniqueness in reverse
        if Connection.objects.filter(child=self.parent, parent=self.child):
            raise ValidationError('A reverse connection for these objects already exists.')

    def get_default_dcxml_format_pk():
        '''Utility function to pull in default from settings.'''
        dcxml_format, created = DCXMLFormat.objects\
                                           .get_or_create(dcxml_purpose=settings.DEFAULT_DCXMLFORMAT['dcxml_purpose'],
                                                          element_name=settings.DEFAULT_DCXMLFORMAT['element_name'])
        return dcxml_format.pk

    parent = models.ForeignKey('DigObject', related_name='child_set', null=False, blank=False,
                               validators=[validate_parent], help_text=_help['parent'])
    child = models.ForeignKey('DigObject', related_name='parent_set', null=False, blank=False,
                              validators=[validate_child], help_text=_help['child'])
    dcxml_format = models.ForeignKey('DCXMLFormat', max_length=45, null=False, blank=False, on_delete=models.PROTECT,
                                     default = get_default_dcxml_format_pk, help_text=_help['dcxml_format'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "'DigObject {}' > 'DigObject {}'".format(str(self.parent.pk), str(self.child.pk))

class MetaEvent(models.Model):
    _help = {
        'profile': '',
        'context': '',
        'user': '',
        'error_log_url': 'This should lead directly to the log file download',
        'datestamp': 'The date/time when the event occurred',
    }

    profile = models.ForeignKey('Profile', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['profile'])
    context = models.ForeignKey('Context', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['context'])
    user = models.ForeignKey('auth.User', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['user'])
    error_log_url = models.CharField(max_length=255, null=True, blank=True, help_text=_help['error_log_url'])
    datestamp = models.DateTimeField(null=False, default=datetime.datetime.now(), auto_now_add=True)

    def deposit_events(self):
        return self.event_set.filter(event_type__name="Deposited")

    def deposit_error_events(self):
        return self.event_set.filter(event_type__name__startswith="Deposit Failed")

    def update_error_events(self):
        return self.event_set.filter(event_type__name__startswith="Update Failed")

    def error_count(self):
        return self.event_set.filter(event_type__name__contains="Failed").count()

    def success_count(self):
        return self.event_set.exclude(event_type__name__contains="Failed").count()

    def apply_tag_event(self, digobject):
        Event.objects.create(meta_event=self,
                             event_type=EventType.objects.get(name="Tag applied"),
                             digobject=digobject)

    def delete_tag_event(self, digobject):
        Event.objects.create(meta_event=self,
                             event_type=EventType.objects.get(name="Tag deleted"),
                             digobject=digobject)

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return "MetaEvent: {}/{}".format(self.profile.name if self.profile else "No Profile",
                                         self.context.name)

class Event(models.Model):
    _help = {
        'meta_event': '',
        'event_type': '',
        'digobject': '', }

    meta_event = models.ForeignKey('MetaEvent', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['meta_event'])
    event_type = models.ForeignKey('EventType', null=False, blank=False, on_delete=models.PROTECT, help_text=_help['event_type'])
    digobject = models.ForeignKey('DigObject', null=True, blank=True, on_delete=models.PROTECT, help_text=_help['digobject'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.event_type.name

class EventType(models.Model):
    _help = {
        'name': '',
        'log_name': '',
        'log_help_text': '',
        }

    name = models.CharField(max_length=255, null=False, default='', help_text=_help['name'])
    log_name = models.CharField(max_length=45, null=True, blank=True, help_text=_help['log_name'])
    log_help_text = models.TextField(null=False, blank=True, default='', help_text=_help['log_help_text'])

    def __str__(self):
        return self.__unicode__()
    def __unicode__(self):
        return self.name

