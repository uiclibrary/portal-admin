# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Role'
        db.create_table('portal_admin_role', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rolename', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
        ))
        db.send_create_signal('portal_admin', ['Role'])

        # Adding M2M table for field digobjectforms on 'Role'
        m2m_table_name = db.shorten_name('portal_admin_role_digobjectforms')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('role', models.ForeignKey(orm['portal_admin.role'], null=False)),
            ('digobjectform', models.ForeignKey(orm['portal_admin.digobjectform'], null=False))
        ))
        db.create_unique(m2m_table_name, ['role_id', 'digobjectform_id'])

        # Adding model 'UserRole'
        db.create_table('portal_admin_userrole', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('role', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Role'], blank=True, null=True, on_delete=models.PROTECT)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], blank=True, null=True, on_delete=models.PROTECT)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Institution'], blank=True, null=True, on_delete=models.PROTECT)),
            ('context', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Context'], blank=True, null=True, on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['UserRole'])

        # Adding model 'Institution'
        db.create_table('portal_admin_institution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('full_name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('xtf_name', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('facet_name', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('url', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
        ))
        db.send_create_signal('portal_admin', ['Institution'])

        # Adding model 'Location'
        db.create_table('portal_admin_location', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('url', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=45, blank=True, null=True)),
            ('web_contact_form', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True, null=True)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Institution'], blank=True, null=True, on_delete=models.PROTECT)),
            ('street_address', self.gf('django.db.models.fields.TextField')(default='')),
            ('city', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('state', self.gf('django.db.models.fields.CharField')(default='IL', max_length=255)),
            ('country', self.gf('django.db.models.fields.CharField')(default='United States', max_length=255)),
            ('zipcode', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True, null=True)),
        ))
        db.send_create_signal('portal_admin', ['Location'])

        # Adding model 'Context'
        db.create_table('portal_admin_context', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('help_email', self.gf('django.db.models.fields.EmailField')(max_length=45, blank=True, null=True)),
            ('naan', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
        ))
        db.send_create_signal('portal_admin', ['Context'])

        # Adding model 'InstitutionContext'
        db.create_table('portal_admin_institutioncontext', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Institution'], blank=True, null=True, on_delete=models.PROTECT)),
            ('context', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Context'], blank=True, null=True, on_delete=models.PROTECT)),
            ('member_type', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
            ('google_snippet', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('main_contact_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True, null=True)),
            ('main_contact_email', self.gf('django.db.models.fields.EmailField')(max_length=45, blank=True, null=True)),
            ('main_contact_phone', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('use_auto_classify', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('portal_admin', ['InstitutionContext'])

        # Adding model 'ProfileField'
        db.create_table('portal_admin_profilefield', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('digobject_form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObjectForm'], on_delete=models.PROTECT)),
            ('dcxml_format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DCXMLFormat'], on_delete=models.PROTECT)),
            ('use_source_widget', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('source_label', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('use_complex_widget', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('use_instance_widget', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('use_instance_all', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('use_filename_widget', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('can_add_widget', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('use_delimiter_widget', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('field_order', self.gf('django.db.models.fields.IntegerField')(default=-1)),
            ('help_text', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('use_attribute_widget', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('attribute_widget_hint', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('use_attribute_value_widget', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('attribute_value_hint', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
        ))
        db.send_create_signal('portal_admin', ['ProfileField'])

        # Adding model 'FileSource'
        db.create_table('portal_admin_filesource', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Profile'], on_delete=models.PROTECT)),
            ('xpath', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('use_filename', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('suffix', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True, null=True)),
            ('instance_number', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('has_extension', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('portal_admin', ['FileSource'])

        # Adding model 'Profile'
        db.create_table('portal_admin_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(default='')),
            ('institution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Institution'], on_delete=models.PROTECT)),
            ('is_remote', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('split_node', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('digobject_form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObjectForm'], on_delete=models.PROTECT)),
            ('use_metadata_filename', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('portal_admin', ['Profile'])

        # Adding model 'DCXMLFormat'
        db.create_table('portal_admin_dcxmlformat', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('dcxml_purpose', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('element_name', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('field_label', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('is_required', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('use_dcxml_element', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('concat_order', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('separator', self.gf('django.db.models.fields.CharField')(max_length=1, blank=True, null=True)),
        ))
        db.send_create_signal('portal_admin', ['DCXMLFormat'])

        # Adding model 'ElementSource'
        db.create_table('portal_admin_elementsource', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Profile'], on_delete=models.PROTECT)),
            ('dcxml_format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DCXMLFormat'], on_delete=models.PROTECT)),
            ('xpath', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True, null=True)),
            ('instance_number', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('delimiter', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('default_text', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('use_filename', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('portal_admin', ['ElementSource'])

        # Adding model 'ImportedValue'
        db.create_table('portal_admin_importedvalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.ValueElement'], on_delete=models.PROTECT)),
            ('digobject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObject'], on_delete=models.PROTECT)),
            ('dcxml_format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DCXMLFormat'], on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['ImportedValue'])

        # Adding model 'ValueElement'
        db.create_table('portal_admin_valueelement', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('element', self.gf('django.db.models.fields.TextField')(default='')),
        ))
        db.send_create_signal('portal_admin', ['ValueElement'])

        # Adding model 'FacetGroup'
        db.create_table('portal_admin_facetgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('dcxml_format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DCXMLFormat'], blank=True, null=True, on_delete=models.PROTECT)),
            ('context', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Context'], blank=True, null=True, on_delete=models.PROTECT)),
            ('is_openedit', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('portal_admin', ['FacetGroup'])

        # Adding model 'FacetValue'
        db.create_table('portal_admin_facetvalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('facet_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.FacetGroup'], blank=True, null=True, on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['FacetValue'])

        # Adding unique constraint on 'FacetValue', fields ['facet_group', 'value']
        db.create_unique('portal_admin_facetvalue', ['facet_group_id', 'value'])

        # Adding model 'DigObjectFacetValue'
        db.create_table('portal_admin_digobjectfacetvalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('digobject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObject'], blank=True, null=True, on_delete=models.PROTECT)),
            ('facet_value', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.FacetValue'], blank=True, null=True, on_delete=models.PROTECT)),
            ('is_autoassigned', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('trigger_term', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.TriggerTerm'], blank=True, null=True, on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['DigObjectFacetValue'])

        # Adding model 'TriggerTerm'
        db.create_table('portal_admin_triggerterm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('term', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('dcxml_format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DCXMLFormat'], blank=True, null=True, on_delete=models.PROTECT)),
            ('facet_value', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.FacetValue'], blank=True, null=True, on_delete=models.PROTECT)),
            ('match_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.MatchType'], blank=True, null=True, on_delete=models.PROTECT)),
            ('context', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Context'], blank=True, null=True, on_delete=models.PROTECT)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('portal_admin', ['TriggerTerm'])

        # Adding model 'MatchType'
        db.create_table('portal_admin_matchtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('query', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
        ))
        db.send_create_signal('portal_admin', ['MatchType'])

        # Adding model 'DigObject'
        db.create_table('portal_admin_digobject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ark', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Profile'], on_delete=models.PROTECT)),
            ('digobject_form', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObjectForm'], on_delete=models.PROTECT)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Location'], blank=True, null=True, on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['DigObject'])

        # Adding model 'DeletedDigObject'
        db.create_table('portal_admin_deleteddigobject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ark', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Event'], blank=True, null=True, on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['DeletedDigObject'])

        # Adding model 'DeletedImportedValue'
        db.create_table('portal_admin_deletedimportedvalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deleted_digobject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DeletedDigObject'], blank=True, null=True, on_delete=models.PROTECT)),
            ('local_id', self.gf('django.db.models.fields.TextField')(default='')),
        ))
        db.send_create_signal('portal_admin', ['DeletedImportedValue'])

        # Adding model 'File'
        db.create_table('portal_admin_file', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('filename_original', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True, null=True)),
            ('filename_new', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('digobject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObject'], on_delete=models.PROTECT)),
            ('checksum', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('file_order', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
        ))
        db.send_create_signal('portal_admin', ['File'])

        # Adding model 'Path'
        db.create_table('portal_admin_path', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('paths', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('context', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Context'], on_delete=models.PROTECT)),
            ('digobject_status', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObjectStatus'], on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['Path'])

        # Adding model 'DigObjectContext'
        db.create_table('portal_admin_digobjectcontext', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('digobject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObject'], on_delete=models.PROTECT)),
            ('context', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Context'], on_delete=models.PROTECT)),
            ('digobject_status', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObjectStatus'], on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['DigObjectContext'])

        # Adding model 'DigObjectStatus'
        db.create_table('portal_admin_digobjectstatus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
        ))
        db.send_create_signal('portal_admin', ['DigObjectStatus'])

        # Adding model 'DigObjectForm'
        db.create_table('portal_admin_digobjectform', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('_format', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('_type', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('use_manifest', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('manifest_format', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('use_media_folder', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('dcxml_value', self.gf('django.db.models.fields.CharField')(default='', max_length=45)),
            ('can_be_parent', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('can_be_child', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('portal_admin', ['DigObjectForm'])

        # Adding model 'Connection'
        db.create_table('portal_admin_connection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(related_name='child_set', to=orm['portal_admin.DigObject'])),
            ('child', self.gf('django.db.models.fields.related.ForeignKey')(related_name='parent_set', to=orm['portal_admin.DigObject'])),
            ('dcxml_format', self.gf('django.db.models.fields.related.ForeignKey')(default=19, max_length=45, to=orm['portal_admin.DCXMLFormat'], on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['Connection'])

        # Adding unique constraint on 'Connection', fields ['parent', 'child']
        db.create_unique('portal_admin_connection', ['parent_id', 'child_id'])

        # Adding model 'MetaEvent'
        db.create_table('portal_admin_metaevent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Profile'], blank=True, null=True, on_delete=models.PROTECT)),
            ('context', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.Context'], on_delete=models.PROTECT)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], on_delete=models.PROTECT)),
            ('error_log_url', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True, null=True)),
            ('datestamp', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2015, 7, 24, 0, 0), auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('portal_admin', ['MetaEvent'])

        # Adding model 'Event'
        db.create_table('portal_admin_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('meta_event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.MetaEvent'], on_delete=models.PROTECT)),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.EventType'], on_delete=models.PROTECT)),
            ('digobject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portal_admin.DigObject'], blank=True, null=True, on_delete=models.PROTECT)),
        ))
        db.send_create_signal('portal_admin', ['Event'])

        # Adding model 'EventType'
        db.create_table('portal_admin_eventtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('log_name', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True, null=True)),
            ('log_help_text', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
        ))
        db.send_create_signal('portal_admin', ['EventType'])


    def backwards(self, orm):
        # Removing unique constraint on 'Connection', fields ['parent', 'child']
        db.delete_unique('portal_admin_connection', ['parent_id', 'child_id'])

        # Removing unique constraint on 'FacetValue', fields ['facet_group', 'value']
        db.delete_unique('portal_admin_facetvalue', ['facet_group_id', 'value'])

        # Deleting model 'Role'
        db.delete_table('portal_admin_role')

        # Removing M2M table for field digobjectforms on 'Role'
        db.delete_table(db.shorten_name('portal_admin_role_digobjectforms'))

        # Deleting model 'UserRole'
        db.delete_table('portal_admin_userrole')

        # Deleting model 'Institution'
        db.delete_table('portal_admin_institution')

        # Deleting model 'Location'
        db.delete_table('portal_admin_location')

        # Deleting model 'Context'
        db.delete_table('portal_admin_context')

        # Deleting model 'InstitutionContext'
        db.delete_table('portal_admin_institutioncontext')

        # Deleting model 'ProfileField'
        db.delete_table('portal_admin_profilefield')

        # Deleting model 'FileSource'
        db.delete_table('portal_admin_filesource')

        # Deleting model 'Profile'
        db.delete_table('portal_admin_profile')

        # Deleting model 'DCXMLFormat'
        db.delete_table('portal_admin_dcxmlformat')

        # Deleting model 'ElementSource'
        db.delete_table('portal_admin_elementsource')

        # Deleting model 'ImportedValue'
        db.delete_table('portal_admin_importedvalue')

        # Deleting model 'ValueElement'
        db.delete_table('portal_admin_valueelement')

        # Deleting model 'FacetGroup'
        db.delete_table('portal_admin_facetgroup')

        # Deleting model 'FacetValue'
        db.delete_table('portal_admin_facetvalue')

        # Deleting model 'DigObjectFacetValue'
        db.delete_table('portal_admin_digobjectfacetvalue')

        # Deleting model 'TriggerTerm'
        db.delete_table('portal_admin_triggerterm')

        # Deleting model 'MatchType'
        db.delete_table('portal_admin_matchtype')

        # Deleting model 'DigObject'
        db.delete_table('portal_admin_digobject')

        # Deleting model 'DeletedDigObject'
        db.delete_table('portal_admin_deleteddigobject')

        # Deleting model 'DeletedImportedValue'
        db.delete_table('portal_admin_deletedimportedvalue')

        # Deleting model 'File'
        db.delete_table('portal_admin_file')

        # Deleting model 'Path'
        db.delete_table('portal_admin_path')

        # Deleting model 'DigObjectContext'
        db.delete_table('portal_admin_digobjectcontext')

        # Deleting model 'DigObjectStatus'
        db.delete_table('portal_admin_digobjectstatus')

        # Deleting model 'DigObjectForm'
        db.delete_table('portal_admin_digobjectform')

        # Deleting model 'Connection'
        db.delete_table('portal_admin_connection')

        # Deleting model 'MetaEvent'
        db.delete_table('portal_admin_metaevent')

        # Deleting model 'Event'
        db.delete_table('portal_admin_event')

        # Deleting model 'EventType'
        db.delete_table('portal_admin_eventtype')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'symmetrical': 'False', 'to': "orm['auth.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'portal_admin.connection': {
            'Meta': {'unique_together': "(('parent', 'child'),)", 'object_name': 'Connection'},
            'child': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'parent_set'", 'to': "orm['portal_admin.DigObject']"}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'default': '19', 'max_length': '45', 'to': "orm['portal_admin.DCXMLFormat']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'child_set'", 'to': "orm['portal_admin.DigObject']"})
        },
        'portal_admin.context': {
            'Meta': {'object_name': 'Context'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'help_email': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'naan': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.dcxmlformat': {
            'Meta': {'object_name': 'DCXMLFormat'},
            'concat_order': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'dcxml_purpose': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'element_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            'field_label': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'separator': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True', 'null': 'True'}),
            'use_dcxml_element': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portal_admin.deleteddigobject': {
            'Meta': {'object_name': 'DeletedDigObject'},
            'ark': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Event']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.deletedimportedvalue': {
            'Meta': {'object_name': 'DeletedImportedValue'},
            'deleted_digobject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DeletedDigObject']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_id': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        'portal_admin.digobject': {
            'Meta': {'object_name': 'DigObject'},
            'ark': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            'digobject_form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObjectForm']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Location']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Profile']", 'on_delete': 'models.PROTECT'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.digobjectcontext': {
            'Meta': {'object_name': 'DigObjectContext'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Context']", 'on_delete': 'models.PROTECT'}),
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObject']", 'on_delete': 'models.PROTECT'}),
            'digobject_status': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObjectStatus']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portal_admin.digobjectfacetvalue': {
            'Meta': {'object_name': 'DigObjectFacetValue'},
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObject']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'facet_value': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.FacetValue']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_autoassigned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'trigger_term': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.TriggerTerm']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'})
        },
        'portal_admin.digobjectform': {
            'Meta': {'object_name': 'DigObjectForm'},
            '_format': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            '_type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            'can_be_child': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'can_be_parent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dcxml_value': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manifest_format': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'use_manifest': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_media_folder': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portal_admin.digobjectstatus': {
            'Meta': {'object_name': 'DigObjectStatus'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'})
        },
        'portal_admin.elementsource': {
            'Meta': {'object_name': 'ElementSource'},
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DCXMLFormat']", 'on_delete': 'models.PROTECT'}),
            'default_text': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'delimiter': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instance_number': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Profile']", 'on_delete': 'models.PROTECT'}),
            'use_filename': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'xpath': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True', 'null': 'True'})
        },
        'portal_admin.event': {
            'Meta': {'object_name': 'Event'},
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObject']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.EventType']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.MetaEvent']", 'on_delete': 'models.PROTECT'})
        },
        'portal_admin.eventtype': {
            'Meta': {'object_name': 'EventType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_help_text': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'log_name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.facetgroup': {
            'Meta': {'object_name': 'FacetGroup'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Context']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DCXMLFormat']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_openedit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'label': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'})
        },
        'portal_admin.facetvalue': {
            'Meta': {'unique_together': "(('facet_group', 'value'),)", 'ordering': "['value', 'id']", 'object_name': 'FacetValue'},
            'facet_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.FacetGroup']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.file': {
            'Meta': {'object_name': 'File'},
            'checksum': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObject']", 'on_delete': 'models.PROTECT'}),
            'file_order': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'filename_new': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'filename_original': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portal_admin.filesource': {
            'Meta': {'object_name': 'FileSource'},
            'has_extension': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instance_number': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Profile']", 'on_delete': 'models.PROTECT'}),
            'suffix': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True', 'null': 'True'}),
            'use_filename': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'xpath': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.importedvalue': {
            'Meta': {'object_name': 'ImportedValue'},
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DCXMLFormat']", 'on_delete': 'models.PROTECT'}),
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObject']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.ValueElement']", 'on_delete': 'models.PROTECT'})
        },
        'portal_admin.institution': {
            'Meta': {'object_name': 'Institution'},
            'facet_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            'full_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'xtf_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'})
        },
        'portal_admin.institutioncontext': {
            'Meta': {'object_name': 'InstitutionContext'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Context']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'google_snippet': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Institution']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'main_contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'main_contact_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True', 'null': 'True'}),
            'main_contact_phone': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'member_type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'use_auto_classify': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'portal_admin.location': {
            'Meta': {'object_name': 'Location'},
            'city': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'country': ('django.db.models.fields.CharField', [], {'default': "'United States'", 'max_length': '255'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Institution']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'IL'", 'max_length': '255'}),
            'street_address': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'web_contact_form': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True', 'null': 'True'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True', 'null': 'True'})
        },
        'portal_admin.matchtype': {
            'Meta': {'object_name': 'MatchType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'query': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'})
        },
        'portal_admin.metaevent': {
            'Meta': {'object_name': 'MetaEvent'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Context']", 'on_delete': 'models.PROTECT'}),
            'datestamp': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 7, 24, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'error_log_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Profile']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'on_delete': 'models.PROTECT'})
        },
        'portal_admin.path': {
            'Meta': {'object_name': 'Path'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Context']", 'on_delete': 'models.PROTECT'}),
            'digobject_status': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObjectStatus']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paths': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.profile': {
            'Meta': {'object_name': 'Profile'},
            'description': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'digobject_form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObjectForm']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Institution']", 'on_delete': 'models.PROTECT'}),
            'is_remote': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'split_node': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'use_metadata_filename': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portal_admin.profilefield': {
            'Meta': {'object_name': 'ProfileField'},
            'attribute_value_hint': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'attribute_widget_hint': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True', 'null': 'True'}),
            'can_add_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DCXMLFormat']", 'on_delete': 'models.PROTECT'}),
            'digobject_form': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObjectForm']", 'on_delete': 'models.PROTECT'}),
            'field_order': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'help_text': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source_label': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'}),
            'use_attribute_value_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_attribute_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_complex_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_delimiter_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_filename_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_instance_all': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_instance_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_source_widget': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'portal_admin.role': {
            'Meta': {'object_name': 'Role'},
            'digobjectforms': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['portal_admin.DigObjectForm']", 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rolename': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '45'})
        },
        'portal_admin.triggerterm': {
            'Meta': {'object_name': 'TriggerTerm'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Context']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DCXMLFormat']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'facet_value': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.FacetValue']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'match_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.MatchType']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'term': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        'portal_admin.userrole': {
            'Meta': {'object_name': 'UserRole'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Context']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Institution']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.Role']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'blank': 'True', 'null': 'True', 'on_delete': 'models.PROTECT'})
        },
        'portal_admin.valueelement': {
            'Meta': {'object_name': 'ValueElement'},
            'element': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['portal_admin']