# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Context.preview_url'
        db.add_column('portal_admin_context', 'preview_url',
                      self.gf('django.db.models.fields.CharField')(null=True, max_length=255, blank=True),
                      keep_default=False)

        # Adding field 'Context.publish_url'
        db.add_column('portal_admin_context', 'publish_url',
                      self.gf('django.db.models.fields.CharField')(null=True, max_length=255, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Context.preview_url'
        db.delete_column('portal_admin_context', 'preview_url')

        # Deleting field 'Context.publish_url'
        db.delete_column('portal_admin_context', 'publish_url')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['auth.Group']", 'related_name': "'user_set'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['auth.Permission']", 'related_name': "'user_set'"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'object_name': 'ContentType', 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'portal_admin.connection': {
            'Meta': {'object_name': 'Connection', 'unique_together': "(('parent', 'child'),)"},
            'child': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObject']", 'related_name': "'parent_set'"}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DCXMLFormat']", 'max_length': '45', 'default': '19'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portal_admin.DigObject']", 'related_name': "'child_set'"})
        },
        'portal_admin.context': {
            'Meta': {'object_name': 'Context'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'help_email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'naan': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'preview_url': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'}),
            'publish_url': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'})
        },
        'portal_admin.dcxmlformat': {
            'Meta': {'object_name': 'DCXMLFormat'},
            'concat_order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'dcxml_purpose': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'element_name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            'field_label': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'separator': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '1', 'blank': 'True'}),
            'use_dcxml_element': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portal_admin.deleteddigobject': {
            'Meta': {'object_name': 'DeletedDigObject'},
            'ark': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Event']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"})
        },
        'portal_admin.deletedimportedvalue': {
            'Meta': {'object_name': 'DeletedImportedValue'},
            'deleted_digobject': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.DeletedDigObject']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_id': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        'portal_admin.digobject': {
            'Meta': {'object_name': 'DigObject'},
            'ark': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            'digobject_form': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObjectForm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Location']", 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.Profile']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"})
        },
        'portal_admin.digobjectcontext': {
            'Meta': {'object_name': 'DigObjectContext'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.Context']"}),
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObject']"}),
            'digobject_status': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObjectStatus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portal_admin.digobjectfacetvalue': {
            'Meta': {'object_name': 'DigObjectFacetValue'},
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.DigObject']", 'blank': 'True'}),
            'facet_value': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.FacetValue']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_autoassigned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'trigger_term': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.TriggerTerm']", 'blank': 'True'})
        },
        'portal_admin.digobjectform': {
            'Meta': {'object_name': 'DigObjectForm'},
            '_format': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            '_type': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            'can_be_child': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'can_be_parent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dcxml_value': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manifest_format': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'use_manifest': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_media_folder': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portal_admin.digobjectstatus': {
            'Meta': {'object_name': 'DigObjectStatus'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"})
        },
        'portal_admin.elementsource': {
            'Meta': {'object_name': 'ElementSource'},
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DCXMLFormat']"}),
            'default_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'delimiter': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instance_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.Profile']"}),
            'use_filename': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'xpath': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'})
        },
        'portal_admin.event': {
            'Meta': {'object_name': 'Event'},
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.DigObject']", 'blank': 'True'}),
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.EventType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_event': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.MetaEvent']"})
        },
        'portal_admin.eventtype': {
            'Meta': {'object_name': 'EventType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_help_text': ('django.db.models.fields.TextField', [], {'blank': 'True', 'default': "''"}),
            'log_name': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"})
        },
        'portal_admin.facetgroup': {
            'Meta': {'object_name': 'FacetGroup'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Context']", 'blank': 'True'}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.DCXMLFormat']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_openedit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"})
        },
        'portal_admin.facetvalue': {
            'Meta': {'object_name': 'FacetValue', 'ordering': "['value', 'id']", 'unique_together': "(('facet_group', 'value'),)"},
            'facet_group': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.FacetGroup']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"})
        },
        'portal_admin.file': {
            'Meta': {'object_name': 'File'},
            'checksum': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObject']"}),
            'file_order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'filename_new': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'filename_original': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portal_admin.filesource': {
            'Meta': {'object_name': 'FileSource'},
            'has_extension': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instance_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.Profile']"}),
            'suffix': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '15', 'blank': 'True'}),
            'use_filename': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'xpath': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"})
        },
        'portal_admin.importedvalue': {
            'Meta': {'object_name': 'ImportedValue'},
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DCXMLFormat']"}),
            'digobject': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObject']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.ValueElement']"})
        },
        'portal_admin.institution': {
            'Meta': {'object_name': 'Institution'},
            'facet_name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            'full_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'xtf_name': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"})
        },
        'portal_admin.institutioncontext': {
            'Meta': {'object_name': 'InstitutionContext'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Context']", 'blank': 'True'}),
            'google_snippet': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Institution']", 'blank': 'True'}),
            'main_contact_email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'main_contact_name': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'}),
            'main_contact_phone': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'member_type': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255', 'default': "''"}),
            'use_auto_classify': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'portal_admin.location': {
            'Meta': {'object_name': 'Location'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "'United States'"}),
            'email': ('django.db.models.fields.EmailField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Institution']", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'phone': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "'IL'"}),
            'street_address': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'web_contact_form': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'})
        },
        'portal_admin.matchtype': {
            'Meta': {'object_name': 'MatchType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'query': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"})
        },
        'portal_admin.metaevent': {
            'Meta': {'object_name': 'MetaEvent'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.Context']"}),
            'datestamp': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True', 'default': 'datetime.datetime(2015, 7, 24, 0, 0)'}),
            'error_log_url': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Profile']", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['auth.User']"})
        },
        'portal_admin.path': {
            'Meta': {'object_name': 'Path'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.Context']"}),
            'digobject_status': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObjectStatus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paths': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"})
        },
        'portal_admin.profile': {
            'Meta': {'object_name': 'Profile'},
            'description': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'digobject_form': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObjectForm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.Institution']"}),
            'is_remote': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"}),
            'split_node': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'use_metadata_filename': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portal_admin.profilefield': {
            'Meta': {'object_name': 'ProfileField'},
            'attribute_value_hint': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'attribute_widget_hint': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '45', 'blank': 'True'}),
            'can_add_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DCXMLFormat']"}),
            'digobject_form': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'to': "orm['portal_admin.DigObjectForm']"}),
            'field_order': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'help_text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source_label': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"}),
            'use_attribute_value_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_attribute_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_complex_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_delimiter_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_filename_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_instance_all': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_instance_widget': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'use_source_widget': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'portal_admin.role': {
            'Meta': {'object_name': 'Role'},
            'digobjectforms': ('django.db.models.fields.related.ManyToManyField', [], {'null': 'True', 'symmetrical': 'False', 'to': "orm['portal_admin.DigObjectForm']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rolename': ('django.db.models.fields.CharField', [], {'max_length': '45', 'default': "''"})
        },
        'portal_admin.triggerterm': {
            'Meta': {'object_name': 'TriggerTerm'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Context']", 'blank': 'True'}),
            'dcxml_format': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.DCXMLFormat']", 'blank': 'True'}),
            'facet_value': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.FacetValue']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'match_type': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.MatchType']", 'blank': 'True'}),
            'term': ('django.db.models.fields.CharField', [], {'max_length': '255', 'default': "''"})
        },
        'portal_admin.userrole': {
            'Meta': {'object_name': 'UserRole'},
            'context': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Context']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'institution': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Institution']", 'blank': 'True'}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['portal_admin.Role']", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.PROTECT', 'null': 'True', 'to': "orm['auth.User']", 'blank': 'True'})
        },
        'portal_admin.valueelement': {
            'Meta': {'object_name': 'ValueElement'},
            'element': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['portal_admin']