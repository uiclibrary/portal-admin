from django.conf import settings

from django.shortcuts import redirect

from django.contrib.auth import login
from django.core.urlresolvers import reverse

from django.views.generic import FormView

from portal_admin.models import UserRole

from portal_admin.forms.auth import LoginForm, SelectContextForm

class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = settings.LOGIN_REDIRECT_URL

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context.update({'next': self.request.REQUEST.get('next', '')})
        return context

    def form_valid(self, form):
        login(self.request, form.cleaned_data['user'])
        if form.cleaned_data['roles'].count() == 1:
            role = UserRole.objects.get(user=form.cleaned_data['user'])
            if not role.role.rolename == 'manager':
                self.request.session['institution'] = role.institution.id
                self.request.session['context'] = role.context.id
                return redirect(self.success_url)

        n = self.request.REQUEST.get('next', '')
        if len(n) > 0:
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(self.request.REQUEST.get('next'),
                                     reverse('select_context'))
        else:
            return redirect(reverse('select_context'))

class SelectContextView(FormView):
    template_name = "select_context.html"
    form_class = SelectContextForm
    success_url = settings.LOGIN_REDIRECT_URL

    def get_context_data(self, **kwargs):
        context = super(SelectContextView, self).get_context_data(**kwargs)
        context.update({'next': self.request.REQUEST.get('next', '')})
        return context

    def get_form_kwargs(self):
        kwargs = super(SelectContextView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        self.request.session['institution'] = form.cleaned_data['institution'].id
        self.request.session['context'] = form.cleaned_data['context'].id
        n = self.request.REQUEST.get('next', '')
        return redirect(n if len(n) > 0 else self.success_url)
