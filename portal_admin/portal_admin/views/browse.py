import os, tempfile

from urllib.parse import urlencode, urlparse

from django.conf import settings
from django.shortcuts import redirect
from django.http import HttpResponse, Http404

from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.views.generic import (
    DeleteView,
    DetailView,
    FormView,
    TemplateView
)
from django.contrib import messages


from django.views.generic.edit import FormMixin

from portal_admin import processor
from portal_admin import utils

from portal_admin.models import (
    Connection,
    Context,
    DCXMLFormat,
    DigObject,
    DigObjectContext,
    DigObjectFacetValue,
    FacetGroup,
    FacetValue,
    ImportedValue,
    FacetValue,
    ValueElement,)

from portal_admin.forms.record import (
    DigObjectSearchForm,
    DigObjectFilterForm,
    ValueElementFilterForm,
    FacetValueFilterForm,

    FileForm,
    HiddenDigObjectsForm,
    RecordForm,
    SelectValueElementsForm,
    SelectFacetValuesForm
    )

from portal_admin.forms.tag import TagFilterForm
from portal_admin.forms.deposit import UploadForm

from portal_admin.tasks import delete_objects, update_digobject

class FilterDigObjectsMixin(object):

    def get_params(self, request):
        status = request.GET.get('status', False)
        t = request.GET.get('type', False)
        profile = request.GET.get('profile', False)
        dcxml_format = request.GET.get('field', False)
        q = request.GET.get('q', False)
        sort = request.GET.get('sort', False)

        return {'status': status,
                't': t,
                'profile': profile,
                'dcxml_format': dcxml_format,
                'q': q,
                'sort': sort,
                }

    def filter_digobjects(self, request):
        objs = DigObject.objects.filter(profile__institution=request.session['institution'],
                                        digobjectcontext__context=request.session['context'])
        p = self.get_params(request)

        # filters
        if p['status']:
            # filter: status based on selection from user
            objs = objs.filter(digobjectcontext__digobject_status=p['status'])
        if p['t']:
            # filter: images/archival based on selection from user
            objs = objs.filter(digobject_form___type=p['t'])
        if p['profile']:
            # filter: to given profile if the user has chosen one
            objs = objs.filter(profile=p['profile'])
        if p['dcxml_format']:
            # filter: to given field if the user has chosen one
            objs = objs.filter(importedvalue__dcxml_format=p['dcxml_format'])
        if p['q']:
            # filter: if query
            predicate = self.request.GET.get('predicate', "element__icontains")
            elements = ValueElement.objects.filter(**{predicate: p['q'].strip()})
            values = ImportedValue.objects.filter(value__in=elements)
            objs = objs.filter(pk__in=values.values_list("digobject", flat=True))

        # sorting
        objs = objs.order_by('title') # default sort

        sort_list = [
            'title', '-title',
            'profile', '-profile',
        ]
        sort_dict = {
            'status': 'digobjectcontext__digobject_status',
            '-status': '-digobjectcontext__digobject_status',
            'type': 'digobject_form___type',
            '-type': '-digobject_form___type',
        }

        # sort from user
        if p.get('sort', False) and p['sort'] in sort_list:
            objs = objs.order_by(p['sort'])
        elif p.get('sort', False) and p['sort'] in sort_dict.keys():
            objs = objs.order_by(sort_dict[p['sort']])

        # only unique

        objs = objs.distinct()
        return objs



    def filter_value_elements(self, request, dcxml_pk):
        p = self.get_params(request)

        # filters by object
        # filter: since most parameter values filter through objs,
        # base most value filters on objs.
        objs = DigObject.objects.filter(profile__institution=request.session['institution'],
                                        digobjectcontext__context=request.session['context'])

        if p['status']:
            # filter: status based on selection from user
            objs = objs.filter(digobjectcontext__digobject_status=p['status'])
        if p['t']:
            # filter: images/archival based on selection from user
            objs = objs.filter(digobject_form___type=p['t'])
        if p['profile']:
            # filter: to given profile if the user has chosen one
            objs = objs.filter(profile=p['profile'])
        if p['dcxml_format']:
            # filter: to given field if the user has chosen one
            objs = objs.filter(importedvalue__dcxml_format=p['dcxml_format'])

        # filter by value_element
        if dcxml_pk:
            value_elements = ValueElement.objects.filter(importedvalue__digobject__in=objs, importedvalue__dcxml_format=dcxml_pk)
        else:
            value_elements = ValueElement.objects.filter(importedvalue__digobject__in=objs)

        if p['q']:
            # filter: if query
            predicate = self.request.GET.get('predicate', "element__icontains")
            value_elements = value_elements.filter(**{predicate: p['q'].strip()})

        # sorting
        sort_dict = {
            'value': 'element',
            '-value': '-element',
        }

        # sort from user
        if p.get('sort', False) and p['sort'] in sort_dict.keys():
            value_elements = value_elements.order_by(sort_dict[p['sort']])
        else:
            value_elements = value_elements.order_by('element') # default sort

        return value_elements.distinct()


    def filter_facet_values(self, request):
        p = self.get_params(request)

        # filters by object
        # filter: since most parameter values filter through objs,
        # base most value filters on objs.
        objs = DigObject.objects.filter(profile__institution=request.session['institution'],
                                        digobjectcontext__context=request.session['context'])
        if p['status']:
            # filter: status based on selection from user
            objs = objs.filter(digobjectcontext__digobject_status=p['status'])
        if p['t']:
            # filter: images/archival based on selection from user
            objs = objs.filter(digobject_form___type=p['t'])
        if p['profile']:
            # filter: to given profile if the user has chosen one
            objs = objs.filter(profile=p['profile'])
        if p['dcxml_format']:
            # filter: to given field if the user has chosen one
            objs = objs.filter(importedvalue__dcxml_format=p['dcxml_format'])

        # filter by facet_value
        facet_values = FacetValue.objects.filter(digobjectfacetvalue__digobject__in=objs)
        if p['q']:
            # filter: if query
            predicate = self.request.GET.get('predicate', "value__icontains")
            facet_values = facet_values.filter(**{predicate: p['q'].strip()})

        # sorting
        facet_values = facet_values.order_by('value') # default sort

        sort_dict = {
            'value': 'value',
            '-value': '-value',
        }

        # sort from user
        if p.get('sort', False) and p['sort'] in sort_dict.keys():
            facet_values = facet_values.order_by(sort_dict[p['sort']])


        return facet_values.distinct()


class SearchBrowseView(FilterDigObjectsMixin, TemplateView):
    '''
    Provides some basic searching and browsing facility.
    '''
    template_name = "browse/search_browse.html"

    def get_context_data(self, **kwargs):
        context = super(SearchBrowseView, self).get_context_data(**kwargs)
        objs = self.filter_digobjects(self.request)
        tab = self.request.GET.get('tab', 'search')

        # filter: DCXMLFormats from imported values associated with digobjects in this context/institution
        fields = DCXMLFormat.objects.filter(importedvalue__digobject__in=objs)\
                                    .exclude(field_label=None)\
                                    .annotate(value_count=Count('importedvalue__value', distinct=True))\
                                    .annotate(record_count=Count('importedvalue__digobject', distinct=True))\
                                    .distinct()

        # filter: facet groups associated with facet values associated with digobjects in this context/institution
        # aggregates: number of digobjectfacetvalues in this institution/context
        # sort: Alphabetical by facet_group.label
        facet_count = Count('facetvalue__digobjectfacetvalue', distinct=True)
        tf = TagFilterForm()
        if tf.is_valid():
            groups = FacetGroup.objects.filter(context=self.request.session['context'],
                                               facetvalue__digobjectfacetvalue__digobject__in=objs,
                                               facetvalue__digobjectfacetvalue__is_autoassigned=\
                                               (tf.cleaned_data['assigned']=='True'))
        else:
            groups = FacetGroup.objects.filter(context=self.request.session['context'],
                                               facetvalue__digobjectfacetvalue__digobject__in=objs)

        groups = groups.annotate(value_count=Count('facetvalue', distinct=True))\
                       .annotate(record_count=Count('facetvalue__digobjectfacetvalue__digobject', distinct=True))\
                       .order_by('label')

        context.update({'tab': tab,
                        'fields': fields,
                        'groups': groups,
                        'search_form': DigObjectSearchForm(),
                        'filter_form': DigObjectFilterForm(),})
        return context



class BrowseView(FilterDigObjectsMixin, FormView):
    '''
    An abstract View class for sorting and selecting values and records.  Overidden below.
    '''
    def get_pages(self, items):
        paginator = Paginator(items, self.request.GET.get('items_per_page', 20))
        return paginator

    def get(self, request, *args, **kwargs):
        # clean up stale session variables; we only want these using POST
        self.request.session.pop('current_query_objs', None)
        self.request.session.pop('parent_objs', None)
        self.request.session.pop('child_objs', None)

        return super(BrowseView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        page_change = request.POST.get('page_change', False)
        items_per_page = request.POST.get('items_per_page', False)
        current_url = request.get_full_path()
        current_query_string = request.GET.copy()

        # for switching pages: select items, pass to session
        if request.POST.get('pager_selections'):
            # convert string to list; map strings to integers.
            pager_selections = list(map(int,
                request.POST.get('pager_selections','').split(',')))
            if page_change or items_per_page:
                request.session['pager_selections'] = pager_selections
                request.session['items_submitted'] = []
            else: # must be submission
                request.session['pager_selections'] = []
                request.session['items_submitted'] = pager_selections
        else:
            # don't keep anything selected if it's not directly from POST.
            request.session['pager_selections'] = []
            request.session['items_submitted'] = []

        # pager intercept redirect
        if page_change:
            current_query_string['page'] = page_change
            new_query_string = current_query_string.urlencode()
            new_url = '{}?{}'.format( request.path, new_query_string)
            return redirect(new_url)
        if items_per_page:
            current_query_string['items_per_page'] = items_per_page
            current_query_string['page'] = 1
            new_query_string = current_query_string.urlencode()
            new_url = '{}?{}'.format( request.path, new_query_string)
            return redirect(new_url)
        else:
            response = super(BrowseView, self).post(request, *args, **kwargs)
            return response

    def get_context_data(self, form, **kwargs):
        context = super(BrowseView, self).get_context_data(**kwargs)

        action_buttons = [
            ['review',  'Review Records'],
            ['add',     'Add Tags'],
            ['preview', 'Preview'],
            ['publish', 'Publish'],
            ['delete',  'Delete'],
        ]

        # column sorting
        sort_order = {}

        title_sort = self.request.GET.copy()
        if title_sort.get('sort', False) == 'title':
            title_sort['sort'] = '-title'
            sort_order['title'] = 'reverse'
        else:
            title_sort['sort'] = 'title'

        status_sort = self.request.GET.copy()
        if status_sort.get('sort', False) == 'status':
            status_sort['sort'] = '-status'
            sort_order['status'] = 'reverse'
        else:
            status_sort['sort'] = 'status'

        value_sort = self.request.GET.copy()
        if value_sort.get('sort', False) == 'value':
            value_sort['sort'] = '-value'
            sort_order['value'] = 'reverse'
        else:
            value_sort['sort'] = 'value'

        sort_urls = {
            'title':    title_sort.urlencode(),
            'status':   status_sort.urlencode(),
            'value':    value_sort.urlencode(),
        }

        # this makes the pager selections disappear after use
        pager_selections = self.request.session.pop('pager_selections', None)

        context.update({
            'title': 'Browse',
            'list_unit': 'record',
            'action_buttons': action_buttons,
            # 'filter_form': DigObjectFilterForm(),
            # 'importedvalue_form': ImportedValueFilterForm(),
            'search_form': form,
            'pager_selections': pager_selections,

            'current_params': self.request.GET.copy(),
            'sort_urls': sort_urls,
            'sort_order': sort_order,
        })
        return context


class BrowseRecordsView(BrowseView):
    '''
    By default, without filtering, this view provides a list of every record in context.
    '''
    template_name = "browse/browse_records.html"
    form_class = RecordForm

    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsView, self).get_form_kwargs()

        objs = self.filter_digobjects(self.request)
        pages = self.get_pages(objs)

        kwargs.update({
            'request': self.request,
            'pages': pages,
            'page': self.request.GET.get('page', 1)
        })
        return kwargs

    def title(self):
        return 'Browse Records'

    def subtitle(self):
        return 'Browse, filter, select and review digital object records.'

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsView, self).get_context_data(form, **kwargs)

        action_buttons = [
            ['review', 'Review Records'],
            ['add', 'Add Tags'],
            ['preview', 'Preview'],
            ['publish', 'Publish'],
            ['delete', 'Delete'],
        ]

        # pager controls at top of form/table
        # this controls the checkbox selecting items across all pages
        pager_items_all = list(
            form.page.paginator.object_list.values_list('id', flat=True))

        # allows relative navigation in records
        self.request.session['latest_browse_query_record_list'] = pager_items_all
        self.request.session['latest_browse_query'] = self.request.build_absolute_uri()

        context.update({
            'title': self.title(),
            'action_buttons': action_buttons,
            'form': form,
            'pager_items_all': pager_items_all,
            'subtitle': self.subtitle(),
            'filter_form': DigObjectSearchForm(),
            })
        return context

    def form_valid(self, form, *args, **kwargs):

        objs = self.filter_digobjects(self.request)\
           .values_list('pk', flat=True)\
           .distinct()\
           .filter(pk__in = self.request.session.get('items_submitted', None))

        self.request.session['objs'] = list(objs)

        action = self.request.POST.get('action', None)

        # further action on selected items
        # save to session for use in subsequent views
        if action in ['connect', 'remove']:
            self.request.session['objs_active'] = list(
                form.cleaned_data['items'])
        # cleanup
        elif action in ['connect_cancel']:
            del self.request.session['objs_active']


        # redirections
        if action == 'add':
            return redirect('batch_add_tags')
        elif action == 'review':
            return redirect('review_records')
        elif action == 'delete':
            return redirect('delete_records')
        elif action == 'connect':
            return redirect('connect')
        elif action == 'remove':
            return redirect('remove_tag', self.kwargs['pk'])
        elif action == 'connect_cancel':
            return redirect('connect')
        elif action == 'preview':
            return redirect('preview_confirm')
        elif action == 'publish':
            return redirect('publish_confirm')
        else:
            raise Http404


class ReviewRecordsView(BrowseRecordsView):
    def get_form_kwargs(self):
        kwargs = super(ReviewRecordsView, self).get_form_kwargs()

        objs = self.filter_digobjects(self.request).filter(pk__in=self.request.session['objs'])
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

class BrowseRecordsByFacetValueView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular facet_value
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByFacetValueView, self).get_form_kwargs()

        facet_value = FacetValue.objects.get(pk=self.kwargs['pk'])
        objs = self.filter_digobjects(self.request).filter(digobjectfacetvalue__facet_value=facet_value)
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def title(self):
        facet_value = FacetValue.objects.get(pk=self.kwargs['pk'])
        return 'Browse Records'

    def subtitle(self):
        facet_value = FacetValue.objects.get(pk=self.kwargs['pk'])
        return 'Records Tagged "<strong>{}</strong>"'.format(facet_value)

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByFacetValueView, self).get_context_data(form, **kwargs)

        action_buttons = [
            ['review', 'Review Records'],
            ['add', 'Add Tags'],
            ['remove', 'Untag'],
            ['preview', 'Preview'],
            ['publish', 'Publish'],
            ['delete', 'Delete'],
        ]

        context.update({
            'title': self.title(),
            'subtitle': self.subtitle(),
            'action_buttons': action_buttons
        })
        return context

class BrowseRecordsByFacetGroupView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular facet_group
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByFacetGroupView, self).get_form_kwargs()

        self.facet_group = FacetGroup.objects.get(pk=self.kwargs['pk'])

        objs = self.filter_digobjects(self.request)\
                   .filter(digobjectfacetvalue__facet_value__facet_group=self.facet_group)\
                   .distinct()
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def subtitle(self):
        return 'Records in the Facet Group "<strong>{}</strong>"'.format(self.facet_group)

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByFacetGroupView, self)\
            .get_context_data(form, **kwargs)

        context.update({'subtitle': self.subtitle()})
        return context

class BrowseRecordsByValueView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular imported_value/value_element
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByValueView, self).get_form_kwargs()

        self.element = ValueElement.objects.get(pk=self.kwargs['pk'])
        values = ImportedValue.objects.filter(value=self.element)
        objs = self.filter_digobjects(self.request)\
                   .filter(importedvalue__in=values)
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByValueView, self).get_context_data(form, **kwargs)

        context.update({'subtitle': 'Records imported with the value "<strong>{}</strong>"'.format(self.element)})
        return context

class BrowseRecordsByFormatView(BrowseRecordsView):
    '''
    Provides a view of all records filtered by a particular dcxml_format
    '''
    def get_form_kwargs(self):
        kwargs = super(BrowseRecordsByFormatView, self).get_form_kwargs()

        self.format = DCXMLFormat.objects.get(pk=self.kwargs['pk'])

        objs = self.filter_digobjects(self.request)\
                   .filter(importedvalue__dcxml_format=self.format)\
                   .distinct()
        pages = self.get_pages(objs)

        kwargs.update({'pages': pages})
        return kwargs

    def title(self):
        return 'Browse Records'

    def subtitle(self):
        return 'Records imported with the field label "<strong>{}</strong>"'.format(self.format.field_label)

    def get_context_data(self, form, **kwargs):
        context = super(BrowseRecordsByFormatView, self).get_context_data(form, **kwargs)

        context.update({'title': self.title(),
                        'subtitle': self.subtitle(),})
        return context


class BrowseValuesView(BrowseView):
    template_name = "browse/browse_values.html"
    form_class = SelectValueElementsForm

    def get_form_kwargs(self):
        kwargs = super(BrowseValuesView, self).get_form_kwargs()

        objs = self.filter_digobjects(self.request)
        value_elements = self.filter_value_elements(self.request, self.kwargs.get('pk', False))

        pages = self.get_pages(value_elements)

        kwargs.update({'objs': objs,
                       'pages': pages,
                       'page': self.request.GET.get('page', 1)})
        return kwargs

    def title(self):
        return "Browse Metadata"

    def subtitle(self):
        try:
            dcxml_field_name = self.dcxml_format.field_label
            return "Metadata filtered by <strong>{}</strong>".format(dcxml_field_name)
        except:
            return 'All Imported Values'

    def get_context_data(self, form):
        context = super(BrowseValuesView, self).get_context_data(form)

        action_buttons = [
                ['review', 'Review Records'],
                ['add', 'Add Tags'],
                ['delete', 'Delete Records'],
            ]

        pager_items_all = list(form.page.paginator.object_list.values_list('pk', flat=True))

        context.update({'title': self.title(),
                        'subtitle': self.subtitle(),
                        'list_unit': 'value',
                        'action_buttons': action_buttons,
                        'form': form,
                        'filter_form': ValueElementFilterForm(),
                        'pager_items_all': pager_items_all,})
        return context

    def form_valid(self, form):
        value_element_pks = self.request.session.get('items_submitted', None)
        objs = self.filter_digobjects(self.request)\
                   .filter(importedvalue__value__pk__in=value_element_pks)\
                   .values_list('pk', flat=True)\
                   .distinct()
        self.request.session['objs'] = list(objs)

        if form.cleaned_data['action'] == 'delete':
            return redirect('delete_records')
        elif form.cleaned_data['action'] == 'add':
            self.request.session['dcxml_format'] = self.dcxml_format.pk
            self.request.session['elements'] = value_element_pks
            return redirect('batch_add_tags')
        elif form.cleaned_data['action'] == 'review':
            return redirect('review_records')
        else:
            self.form_invalid(form)


class BrowseTagsView(BrowseView):
    template_name = "browse/browse_tags.html"
    form_class = SelectFacetValuesForm

    def get_form_kwargs(self):
        kwargs = super(BrowseTagsView, self).get_form_kwargs()

        objs = DigObject.objects\
                        .filter(profile__institution=self.request.session['institution'],
                                digobjectcontext__context=self.request.session['context'])
        facet_values = self.filter_facet_values(self.request)

        if self.kwargs.get('pk'):
            # filter: to given field if the user has chosen one
            self.dcxml_format = DCXMLFormat.objects.get(facetgroup__pk=self.kwargs['pk'])
            facet_values = facet_values.filter(facet_group__dcxml_format=self.dcxml_format)

        pages = self.get_pages(facet_values)

        kwargs.update({'objs': objs,
                       'pages': pages,
                       'page': self.request.GET.get('page', 1)})
        return kwargs

    def title(self):
        return "Browse Tags"

    def subtitle(self):
        try:
            group_name = FacetGroup.objects.get(pk=self.kwargs['pk'])
            return 'Tags filtered with the Facet Group "<strong>{}</strong>"'.format(group_name)
        except:
            return 'All Tags'

    def get_context_data(self, form, **kwargs):
        context = super(BrowseTagsView, self).get_context_data(form, **kwargs)

        action_buttons = [
            ['review',  'Review Records'],
            ['add', 'Add Tags'],
            ['remove', 'Untag'],
            ['delete', 'Delete Records'],
        ]

        pager_items_all = list(form.page.paginator.object_list.values_list('pk', flat=True))

        context.update({'title': self.title(),
                        'subtitle': self.subtitle(),
                        # 'list_unit': 'value',
                        'filter_form': FacetValueFilterForm(),
                        'form': form,
                        'action_buttons': action_buttons,
                        'pager_items_all': pager_items_all})
        return context

    def form_valid(self, form):
        facet_value_pks = self.request.session.get('items_submitted', None)
        objs = self.filter_digobjects(self.request)\
                   .filter(digobjectfacetvalue__facet_value__pk__in=facet_value_pks)\
                   .values_list('pk', flat=True).distinct()
        self.request.session['objs'] = list(objs)

        if form.cleaned_data['action'] == 'delete':
            return redirect('delete_records')
        elif form.cleaned_data['action'] == 'add':
            self.request.session['dcxml_format'] = self.dcxml_format.pk
            # self.request.session['elements'] = facet_value_pks
            return redirect('batch_add_tags')
        elif form.cleaned_data['action'] == 'review':
            return redirect('review_records')
        elif form.cleaned_data['action'] == 'remove':
            return redirect('remove_tags')
        else:
            self.form_invalid(form)


class RecordView(DetailView):
    template_name = "record/record.html"
    model = DigObject

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)

        if 'title' in request.POST:
            title = request.POST.get('title', 0)
            self.object.title = title
            self.object.save()

        if 'reset_title' in request.POST:
            obj = self.object
            try:
                val = ImportedValue.objects.get(digobject=obj.pk, dcxml_format=1).value
            except:
                val = '<Untitled>'
            self.object.title = val.element
            self.object.save()

        cid = request.session['context']
        DigObjectContext.objects.get(digobject=self.object, context__pk=cid).set_dirty(True)

        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(RecordView, self).get_context_data(**kwargs)
        obj = context['object']
        cid = self.request.session['context']
        doc = DigObjectContext.objects.get(context=cid,
                                           digobject=obj)
        status = doc.digobject_status

        tags = obj.digobjectfacetvalue_set.filter(facet_value__facet_group__context=cid)

        parent_connections = Connection.objects.filter(child=obj)
        child_connections = Connection.objects.filter(parent=obj)

        # for display, to create a dict without repeating elements
        record_fields = {}
        for field in context['object'].importedvalue_set.all():
            key = field.dcxml_format.field_label
            val = field.value
            if not key in record_fields:
                record_fields[key] = [val]
            else:
                record_fields[key] += [val]

        if doc.context.preview_url and (status.status == 'Previewed' or status.status == 'Published'):
            preview_url = utils.get_obj_url(obj, doc.context.preview_url, "preview")
        else:
            preview_url = False

        if doc.context.publish_url and status.status == 'Published':
            publish_url = utils.get_obj_url(obj, doc.context.publish_url)
        else:
            publish_url = False

        # for relative navigation ('prev', 'up', 'next' buttons)
        pk_list = self.request.session.get('latest_browse_query_record_list', None)
        up = self.request.session.get('latest_browse_query', None)
        relative_nav = {'next': None, 'up': None, 'prev': None}
        if up:
            relative_nav['up'] = up
        if pk_list and self.object.pk in pk_list:
            index = pk_list.index(self.object.pk)
            if index != len(pk_list) -1:
                relative_nav['next'] = pk_list[index + 1]
            if index != 0:
                relative_nav['prev'] = pk_list[index - 1]

        context.update({'tab': 'metadata',
                        'tags': tags,
                        'parent_connections': parent_connections,
                        'child_connections': child_connections,
                        'can_be_parent': self.object.digobject_form.can_be_parent,
                        'status': status,
                        'record_fields': sorted(record_fields.items()),
                        'preview_url': preview_url,
                        'publish_url': publish_url,
                        'relative_nav': relative_nav,})
        return context

class UpdateRecordView(DetailView, FormMixin):
    template_name = "record/update_record.html"
    model = DigObject
    form_class = UploadForm

    def get_initial(self):
         return {'upload_dir': tempfile.mkdtemp()}

    def get_context_data(self, **kwargs):
        context = super(UpdateRecordView, self).get_context_data(**kwargs)
        obj = context['object']
        cid = self.request.session['context']
        status = DigObjectContext.objects.get(context=cid, digobject=obj).digobject_status
        path = utils.digobj_path(obj.id, cid).format('media') + 'thumbs/{}.jpg'.format(obj.ark)
        thumb = path.replace(settings.MEDIA_ROOT, settings.MEDIA_URL) if os.path.isfile(path) else False

        form = self.get_form(self.form_class)

        context.update({'accepted_mime_types': settings.ALLOWED_EXTS[obj.profile.digobject_form._type] + ["xml"],
                        'tags': obj.digobjectfacetvalue_set.filter(facet_value__facet_group__context=cid),
                        'form': form,
                        'upload_dir': form.initial['upload_dir'],
                        'thumb': thumb,
                        'status': status})
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form(self.form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        obj = self.get_object()
        update_digobject.delay(self.request.user.pk,
                               self.request.session['context'],
                               obj.pk,
                               form.cleaned_data['upload_dir'],
                               self.request.get_host())
        return redirect(self.get_object().get_absolute_url())

class DeleteRecordsView(FormView):
    template_name = 'record/delete_records.html'
    form_class = HiddenDigObjectsForm
    success_url = reverse_lazy('records_deleted')

    def get_initial(self):
        return {'objs': self.request.session.get('objs', [])}

    def get_context_data(self, **kwargs):
        context = super(DeleteRecordsView, self).get_context_data(**kwargs)
        context.update({'objs': DigObject.objects.filter(pk__in=self.request.session.get('objs', [])),
                        'context': Context.objects.get(pk=self.request.session['context'])})
        return context

    def form_valid(self, form):
        self.request.session['goners'] = []
        self.request.session['survivors'] = []
        for obj in form.cleaned_data['objs']:
            obj.remove_from_context(self.request.session['context'])
            if obj.digobjectcontext_set.count() == 0:
                self.request.session['goners'].append(obj.pk)
            else:
                self.request.session['survivors'].append(obj.pk)
        delete_objects.delay(self.request.session['goners'])
        return redirect(self.get_success_url())

class DeleteRecordView(DeleteView):
    model = DigObject
    template_name = "record/delete_record.html"
    success_url = reverse_lazy('records_deleted')

    def get_context_data(self, **kwargs):
        context = super(DeleteRecordView, self).get_context_data(**kwargs)
        context.update({'context': Context.objects.get(pk=self.request.session['context'])})
        return context

    def delete(self, request, *args, **kwargs):
        self.request.session['goners'] = []
        self.request.session['survivors'] = []
        self.object = self.get_object()
        self.object.remove_from_context(self.request.session['context'])
        if self.object.digobjectcontext_set.count() == 0:
            self.request.session['goners'].append(self.object.pk)
        else:
            self.request.session['survivors'].append(self.object.pk)
        delete_objects.delay(self.request.session['goners'])
        return redirect(self.get_success_url())

class RecordsDeletedView(TemplateView):
    template_name = "record/records_deleted.html"

    def get_context_data(self, **kwargs):
        context = super(RecordsDeletedView, self).get_context_data(**kwargs)
        context.update({'goners': DigObject.objects.filter(pk__in=self.request.session.get('goners', [])),
                        'survivors': DigObject.objects.filter(pk__in=self.request.session.get('survivors', [])),
                        'context': Context.objects.get(pk=self.request.session['context'])})
        return context

class DCXMLView(DetailView):
    model = DigObject

    def get_mimetype(self):
        return 'application/xml'

    def get(self, *args, **kwargs):
        digobj = self.get_object()
        full_path = processor.generate_dc_xml(digobj.pk, self.request.session['context'])

        filename = "{}.dc.xml".format(digobj.ark)

        if os.path.isfile(full_path):
            f = open(full_path, 'rb')
            response = HttpResponse(f.read(), mimetype=self.get_mimetype())
            f.close()
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
            response['Content-Length'] = os.path.getsize(full_path)
            return response
        else:
            raise Http404

class OriginalMetadataView(DetailView):
    model = DigObject

    def get_mimetype(self):
        return 'application/xml'

    def get(self, *args, **kwargs):
        digobj = self.get_object()
        context = Context.objects.get(pk=self.request.session['context'])

        path = utils.digobj_path(digobj.pk, context.pk)
        filename    = digobj.ark + '.xml'
        full_path   = (path + filename).format('data')

        if os.path.isfile(full_path):
            f = open(full_path, 'rb')
            response = HttpResponse(f.read(), mimetype=self.get_mimetype())
            f.close()
            response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
            response['Content-Length'] = os.path.getsize(full_path)
            return response
        else:
            raise Http404

