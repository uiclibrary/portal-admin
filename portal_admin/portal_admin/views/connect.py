from django.views.generic import TemplateView, DeleteView
from django.shortcuts import redirect, get_object_or_404

from portal_admin.views.browse import BrowseRecordsView
from portal_admin.models import Connection, DigObject, DigObjectContext, DigObjectForm


class ConnectView(BrowseRecordsView):
    template_name = "connect/connect.html"

    def clean_up_session(self):
        self.request.session.pop('items_submitted', None)
        self.request.session.pop('parent_objs', None)
        self.request.session.pop('child_objs', None)

    def post(self, request, *args, **kwargs):
        if "connect_cancel" in self.request.POST.values():
            self.request.session.pop('objs_active', None)
            self.request.session.pop('parent_objs', None)
            self.request.session.pop('child_objs', None)
            return redirect('connect')
        return super(ConnectView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ConnectView, self).get_context_data(**kwargs)
        return context


class ConnectSelectParentView(ConnectView):

    def get_context_data(self, **kwargs):
        context = super(ConnectView, self).get_context_data(**kwargs)
        objs = self.filter_digobjects(self.request)

        context.update({
            'title': 'Connect: Select Parent',
            'subtitle': 'Select a Parent Record from the List Below',
            'action_buttons': [['select_parents',
                                'Select a Parent Record',
                                'primary']],
            'single_select': True})

        return context

    def form_valid(self, form):
        parent_pk = form.cleaned_data['items'][0]
        return redirect('connect_select_children', parent_pk)

    def get_form_kwargs(self):
        # get_form_kwargs() helps to build the form
        kwargs = super(ConnectSelectParentView, self).get_form_kwargs()
        objs = self.filter_digobjects(self.request)

        # limit objects to those which can be parents
        formats = DigObjectForm.objects.filter(can_be_parent=True)
        objs = objs.filter(digobject_form__in=formats)

        kwargs.update({
            'pages': self.get_pages(objs),
            'page': self.request.GET.get('page', 1)})
        return kwargs


class ConnectSelectChildrenView(ConnectView):

    def get_parent(self, objs):
        # parent object comes from URL
        return get_object_or_404(DigObject.objects.all(), pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(ConnectSelectChildrenView, self).get_context_data(**kwargs)
        objs = self.filter_digobjects(self.request)

        parent = self.get_parent(objs)

        context.update({
            'title': 'Connect: Select Children',
            'subtitle': 'Select Child Records from the List Below',
            'action_buttons': [
                ['select_children',
                 'Select Child Records',
                 'primary'],
                ['connect_cancel', 'Cancel'],
            ],
            'objs_flat': [parent]
        })
        return context

    def form_valid(self, form):
        objs = self.filter_digobjects(self.request)

        pager_selections = self.request.POST.get('pager_selections', []).split(',')

        # pass parent to confirm view via session
        parent = self.get_parent(objs)

        self.request.session['parent_objs'] = [parent.pk]
        self.request.session['child_objs'] = pager_selections

        return redirect('connect_confirm')

class ConnectConfirmView(TemplateView):
    template_name = "connect/connect_confirm.html"

    def clean_up_session(self):
        self.request.session.pop('items_submitted', None)
        self.request.session.pop('parent_objs', None)
        self.request.session.pop('child_objs', None)

    def get_context_data(self, **kwargs):
        context = super(ConnectConfirmView, self).get_context_data(**kwargs)

        cid = self.request.session['context']

        # something hasn't been selected so clean up and start over
        if not 'parent_objs' in self.request.session or not 'child_objs' in self.request.session:
            self.clean_up_session()
            return redirect('connect')

        # We need to get parent and children records from the form
        # (actually, the session, since the Form only returns what's on the
        # web page; we often need more than just one page of records.)
        # Parents is/are easy: just get from the session.
        parent_objs = DigObjectContext.objects.filter(digobject__in=self.request.session['parent_objs'],
                                                      context__pk=cid)
        child_objs = DigObjectContext.objects.filter(digobject__in=self.request.session['child_objs'],
                                                     context__pk=cid)

        # These lists sort out the results for human consumption
        prohibited_connections = []
        existing_connections = []
        new_connections = []

        for parent in parent_objs:
            pobj = parent.digobject
            for child in child_objs:
                cobj = child.digobject
                if pobj == cobj:
                    prohibited_connections.append({
                        'parent': pobj,
                        'child': cobj,
                        'reason': "a record cannot be connected to itself"})
                elif Connection.objects.filter(parent=cobj, child=pobj).exists():
                    prohibited_connections.append({
                        'parent': pobj,
                        'child': cobj,
                        'reason': "these records are already connected in reverse"})
                elif Connection.objects.filter(parent=pobj, child=cobj).exists():
                    existing_connections.append({
                        'parent': pobj,
                        'child': cobj})
                else:
                    new_connections.append(pobj.add_child(cobj))
                    parent.set_dirty(True)
                    child.set_dirty(True)

        context.update({
            'title': 'Confirm Connections',
            'prohibited_connections': prohibited_connections,
            'existing_connections': existing_connections,
            'new_connections': new_connections
        })

        self.clean_up_session()
        return context

class ConnectDeleteView(DeleteView):
    model = Connection

    def get_success_url(self):
        return self.request.META['HTTP_REFERER']

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        cid = self.request.session['context']
        DigObjectContext.objects.get(digobject=obj.parent, context__pk=cid).set_dirty(True)
        DigObjectContext.objects.get(digobject=obj.child, context__pk=cid).set_dirty(True)
        return super(ConnectDeleteView, self).delete(request, *args, **kwargs)

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(ConnectDeleteView, self).get_object()
        cid = self.request.session['context']

        return obj
