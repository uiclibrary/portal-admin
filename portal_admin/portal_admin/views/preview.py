from django.views.generic import TemplateView, UpdateView

from portal_admin.views.browse import FilterDigObjectsMixin, BrowseRecordsView
from portal_admin.models import Context, DigObject, DigObjectContext

from portal_admin.tasks import start_indexer, reindex_institution
from django.db.models import Q

class ReindexConfirmView(TemplateView):
    template_name = "preview/reindex_confirm.html"

    def get_context_data(self, **kwargs):
        context = super(ReindexConfirmView, self).get_context_data(**kwargs)
        context_id = self.request.session['context']
        institution_id = self.request.session['institution']
        records = DigObjectContext.objects\
                                  .filter(context__pk=context_id,
                                          digobject__profile__institution__pk=institution_id,
                                          dirty=True)\
                                  .filter(Q(digobject_status__status="Previewed") |
                                          Q(digobject_status__status="Published"))
        record_ids = list(records.values_list('pk', flat=True))
        for r in records:
            r.set_dirty(False)
        if len(record_ids) > 0:
            start_indexer.delay(self.request.user.id, context_id, record_ids, self.request.get_host())
        context.update({'title': 'Records Reindexed'})
        return context

class PreviewView(BrowseRecordsView):
    template_name = "preview/preview.html"

    def get_context_data(self, **kwargs):
        context = super(PreviewView, self).get_context_data(**kwargs)

        action_buttons = [
            ['preview', 'Preview'],
        ]

        context.update({
            'title': 'Preview Records',
            'subtitle': 'Preview your records',
            'action_buttons': action_buttons
        })
        return context

class PreviewRecordView(UpdateView):
    template_name = "preview/preview_confirm.html"
    model = DigObject

    def get_context_data(self, **kwargs):
        context = super(PreviewRecordView, self).get_context_data(**kwargs)
        obj = context['object']
        hcid = self.request.session['context']
        doc = obj.digobjectcontext_set.get(context__pk=hcid)

        preview_objs, reindex_objs, neither_objs, index_id = [], [], [], []

        if doc.digobject_status.status == "Deposited":
            preview_objs = [obj]
            index_id = [obj.pk]
        elif doc.digobject_status.status == 'Previewed' or doc.digobject_status.status == 'Published':
            reindex_objs = [obj]
            index_id = [obj.pk]
        elif doc.digobject_status.status == 'Retracted' or doc.digobject_status.status == 'Deleted':
            neither_objs = [obj]

        if len(index_id) > 0:
            start_indexer.delay(self.request.user.id,
                                hcid,
                                index_id,
                                self.request.get_host(),
                                'Previewed')

        context.update({'title': 'Records Previewed',
                        'preview_objs': preview_objs,
                        'reindex_objs': reindex_objs,
                        'neither_objs': neither_objs})
        return context

class PreviewConfirmView(FilterDigObjectsMixin, TemplateView):
    template_name = "preview/preview_confirm.html"

    def get_context_data(self, **kwargs):
        context = super(PreviewConfirmView, self).get_context_data(**kwargs)

        hopper_context_id = self.request.session['context']

        contexts = DigObjectContext.objects\
                                   .filter(digobject__in=self.request.session['objs'],
                                           context__pk=hopper_context_id)

        preview_objs = contexts.filter(digobject_status__status="Deposited")\
                               .values_list('pk', flat=True)

        reindex_objs = contexts.filter(Q(digobject_status__status="Previewed") |
                                       Q(digobject_status__status="Published"))\
                               .values_list('pk', flat=True)

        neither_objs = contexts.filter(Q(digobject_status__status="Retracted") |
                                       Q(digobject_status__status="Deleted"))\
                               .values_list('digobject', flat=True)

        if len(preview_objs) > 0 or len(reindex_objs) > 0:
            start_indexer.delay(self.request.user.id,
                                hopper_context_id,
                                list(preview_objs) + list(reindex_objs),
                                self.request.get_host(),
                                'Previewed')

        context.update({'title': 'Records Previewed',
                        'context': Context.objects.get(pk=hopper_context_id),
                        'preview_objs': DigObjectContext.objects.filter(pk__in=preview_objs),
                        'reindex_objs': DigObjectContext.objects.filter(pk__in=reindex_objs),
                        'neither_objs': DigObjectContext.objects.filter(pk__in=neither_objs)})
        return context
