import os, shutil, zipfile, tempfile
import datetime, time

import logging
logr = logging.getLogger(__name__)

from xml.dom import minidom
from xml.etree.ElementTree import Comment, Element, parse, tostring

from django.conf import settings

from django.contrib.auth.models import User
from portal_admin.models import MetaEvent, Event, EventType, Context, Profile, DigObject

from portal_admin import inspection
from portal_admin import ingestion
from portal_admin.context_managers import clean_room
from portal_admin import custom_logging as errlog

class Record(object):
    """
    Simple utility class, used to collect information when processing.
    The messages[] list is used to collect error information about the
    XML record; to provide a more descriptive set of data.
    """

    def __init__(self, record, filename, path):
        self.record = record
        self.filename = filename
        self.path = path
        self.messages = []
        self.digobj_id = None

def create_events(meta_event, event_type, _records):
    """
    Given an event type create events for each of the records that contains that error
    """
    _records = [r for r in _records if event_type in [m[0] for m in r.messages]]
    if _records:
        for x in _records:
            if x.digobj_id:
                Event.objects.create(
                    meta_event=meta_event, event_type=event_type,
                    digobject=DigObject.objects.get(id=x.digobj_id))
            else:
                Event.objects.create(meta_event=meta_event, event_type=event_type)


def write_xml_logfile(log_root, log_text, records, filename):
    """
    Given a path to write the logfile, the text, and a set of records;
    will create a file, at log_root, of the name DATETIME_filename;
    containing the log_text, as well as any Record.messages[] information.

    Used for the case when profile.split_node is true.
    of many records.
    """
    index = filename.rfind(".")
    log_path = log_root + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + '_{}.xml'.format(filename[:index] if index > -1 else filename)
    while os.path.exists(log_path):
        log_path = log_root + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + '.xml'
        time.sleep(1)

    if len(records) == 1 and not records[0].record: # didn't parse the xml so copy it as a string
        outfile = open(log_path, 'wb')
        r = records[0]
        _msg = ''
        for m in r.messages:
            _msg += "\n{}\n\t{}\n".format(m[0].log_help_text, m[1])
        outfile.write(tostring(Comment(_msg), 'utf-8'))
        with open(r.path + "/" + r.filename, 'rb') as f:
            outfile.write(f.read())
        outfile.close()
    else: # did parse XML, insert comments
        outfile = open(log_path, 'w', encoding="utf-8")
        root = Element('metadata')
        root.append(Comment(log_text))
        for r in records:
            _msg = ''
            for m in r.messages:
                _msg += "\n{}\n\t{}\n".format(m[0].log_help_text, m[1])
            root.append(Comment(_msg))
            root.append(r.record)
        _string = tostring(root, 'utf-8')
        _parsed = minidom.parseString(_string)
        outfile.write(_parsed.toprettyxml(newl='\n', indent="\t"))
        outfile.close()
    return log_path.replace(settings.FILE_ROOT, "")

def write_zip_logfile(log_root, log_text, records):
    """
    Given a path to write the logfile, the text, and a set of records;
    will create a file, at log_root, of the name DATETIME_filename;
    containing the log_text, as well as any Record.messages[] information.

    Used for the case when profile.split_node is false.
    """

    log_name = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M") + '_metadata.zip'
    tempdir = tempfile.mkdtemp() + '/'
    outfile = open(tempdir+ 'readme.txt', 'w', encoding="utf-8")
    outfile.write(log_text)
    outfile.write("\n")
    outfile.close()
    for r in records:
        _msg = ''
        for m in r.messages:
            _msg += "\n{}\n\t{}\n".format(m[0].log_help_text, m[1])
        if r.record: # successfully parsed the xml so we can insert comments
            outfile = open(tempdir + r.filename, 'w', encoding="utf-8")
            r.record.insert(0, Comment(_msg))
            _string = tostring(r.record, 'utf-8')
            _parsed = minidom.parseString(_string)
            outfile.write(_parsed.toprettyxml(newl='\n', indent='\t'))
        else: # did not read xml so just copy over the text
            outfile = open(tempdir + r.filename, 'wb')
            outfile.write(tostring(Comment(_msg), 'utf-8'))
            with open(r.path + "/" + r.filename, 'rb') as f:
                outfile.write(f.read())
        outfile.close()
    cwd = os.getcwd() # keep starting directory
    os.chdir(tempdir) # move to the tempdir to create the zipfile
    _zip = zipfile.ZipFile(log_root + log_name, 'a')
    for f in os.listdir(tempdir):
        if not f.endswith('.zip'):
            _zip.write(f)
    _zip.close()
    os.chdir(cwd)  # back to starting directory
    shutil.rmtree(tempdir)
    return (log_root + log_name).replace(settings.FILE_ROOT, "")

def write_logfile(log_path, log_text, profile_id, records, original_xml_file=None):
    """
    Writes a detailed logfile to disk.
    """

    profile = Profile.objects.get(id=profile_id)
    if profile.split_node and len(profile.split_node):
        return write_xml_logfile(log_path, log_text, records, original_xml_file)
    else:
        return write_zip_logfile(log_path, log_text, records)

def fatal_error(user_id, context_id, digobject, _dir, event_name, original_xml_file, records=[]):
    log_text = errlog._get_logfile_header(context_id, digobject.profile.pk)
    log_root = "{}logs/{}/{}/".format(settings.FILE_ROOT, digobject.profile.institution.xtf_name, digobject.profile.pk)
    os.makedirs(log_root, exist_ok=True)

    event_type, created = EventType.objects.get_or_create(name=event_name)
    _log_text = log_text + "\n{}".format(event_type.log_help_text)
    img_files = [x for x in os.listdir(_dir) if x.split(".")[-1] in settings.ALLOWED_EXTS['images']]
    if len(img_files) > 0:
        for i in img_files:
            _log_text += "{}\n".format(i)
    log_path = write_logfile(log_root, _log_text, digobject.profile.pk, records, original_xml_file)
    meta_event = MetaEvent.objects.create(user=User.objects.get(pk=user_id),
                                          context=Context.objects.get(pk=context_id),
                                          profile=digobject.profile,
                                          error_log_url=log_path)
    if event_name != None:
        Event.objects.create(meta_event=meta_event,
                             digobject=digobject,
                             event_type=event_type)

    return meta_event

def update_digobject(user_id, context_id, input_dir, digobj_id):
    digobj = DigObject.objects.get(pk=digobj_id)

    # Verify XML Metadata exists
    files = os.listdir(input_dir)
    xml_files = [f for f in files if f.endswith('.xml')]
    
    if len(xml_files) < 1:
        return fatal_error(user_id, context_id, digobj, input_dir,
                           "Update Failed: Metadata File Not Found", 'file_not_found'), []
    elif len(xml_files) > 1:
        return fatal_error(user_id, context_id, digobj, input_dir,
                           "Update Failed: Multiple XML Files", 'too_many_files'), []

    xml_file = xml_files[0]
    xml_path = input_dir + xml_file

    try:
        inspection.verify_xml_structure(xml_path)
    except errlog.XMLStructureInvalid:
        return fatal_error(user_id, context_id, digobj, input_dir,
                           "Update Failed: XML Validation Failed", xml_file), []

    if digobj.profile.split_node:
        try:
            inspection.verify_split_node(xml_path, digobj.profile.split_node)
        except errlog.InvalidSplitNode:
            # Cannot continue split node not found
            return fatal_error(user_id, context_id, digobj, input_dir,
                               "Update Failed: Invalid Record Separator", xml_file), []

    with clean_room(input_dir, digobj.profile.pk) as _dir:
        xml_files = [x for x in os.listdir(_dir) if x.endswith('xml')]
        with open(_dir + xml_files[0], 'r', encoding="utf-8") as f:
            record = Record(parse(f).getroot(), xml_files[0], _dir)

        unexpected_exceptions = []
        unexpected_msg = "Unexpected Exception occured during {}\n\tFilename: {}\n\tProfile ID: {}\n\tType: {}"

        try:
            inspection.xml_fields(record, digobj.profile.pk, "Update")
        except Exception as e:
            msg = unexpected_msg.format("inspect xml fields", r.filename, digobj.profile.pk, type(e))
            logr.info(msg)
            unexpected_exceptions.append(msg)

        try:
            inspection.content(record, digobj.profile.pk, "Update")
        except Exception as e:
            msg = unexpected_msg.format("inspect content", record.filename, digobj.profile.pk, type(e))
            logr.info(msg)
            unexpected_exceptions.append(msg)

        try:
            if len(record.messages) < 1:
                ingestion.update_digobj(digobj.pk, record, context_id)
        except Exception as e:
            msg = unexpected_msg.format("update dig objects", record.filename, digobj.profile.pk, type(e))
            logr.info(msg)
            unexpected_exceptions.append(msg)

        if len(record.messages) > 0:
            meta_event = fatal_error(user_id, context_id, digobj, _dir,
                                     record.messages[0][0], xml_file, [record])
        else:
            meta_event = MetaEvent.objects.create(user=User.objects.get(pk=user_id),
                                                  context=Context.objects.get(pk=context_id),
                                                  profile=digobj.profile,
                                                  error_log_url=None)
            for m in record.messages:
                Event.objects.create(meta_event=meta_event, event_type=m[0], digobject=digobj)

            for c in digobj.digobjectcontext_set.all():
                c.set_dirty(True)

    return meta_event, unexpected_exceptions


def process_input(user_id, profile_id, context_id, input_dir, location_id=None):
    """
    Given an archivist upload, and identifying information (profile, context);
    first inspects the upload for data integrity, and then follows by
    attempting to process input and generate digital objects.

    Generates Event(s) and logfiles
    """

    profile=Profile.objects.get(id=profile_id)
    context=Context.objects.get(id=context_id)

    user=User.objects.get(pk=user_id)

    log_text = errlog._get_logfile_header(context_id, profile_id)
    log_root = settings.FILE_ROOT + 'logs/' + profile.institution.xtf_name + '/' + str(profile.id) + '/'
    os.makedirs(log_root, exist_ok=True)

    unexpected_msg = "Unexpected Exception occured during {}\n\tFilename: {}\n\tProfile ID: {}\n\tType: {}"

    # Verify XML Metadata exists
    try:
        inspection.verify_has_xml(input_dir)
    except errlog.NoXMLFound:
        event_type = EventType.objects.get(name="Deposit Failed: Metadata File Not Found")
        _log_text = log_text + "\n{}".format(event_type.log_help_text)
        log_path = write_logfile(log_root, _log_text, profile_id, '', 'file_not_found')
        meta_event = MetaEvent.objects.create(
            user=user,
            context=context,
            profile=profile,
            error_log_url=log_path)
        Event.objects.create(meta_event=meta_event,
                             event_type=event_type)
        return meta_event, [] # Cannot continue, no XML exists

    # Inspection
    invalid_xml = []
    invalid_xml_dir = tempfile.mkdtemp()

    infiles = [f for f in os.listdir(input_dir) if f.endswith('.xml')]

    if len(infiles) > 1 and profile.split_node:
        event_type = EventType.objects.get(name="Deposit Failed: Multiple XML Files")
        _log_text = log_text + "\n{}".format(event_type.log_help_text)
        log_path = write_logfile(log_root, _log_text, profile_id, '', 'too_many_files')
        meta_event = MetaEvent.objects.create(
            user=user,
            context=context,
            profile=profile,
            error_log_url=log_path)
        Event.objects.create(meta_event=meta_event,
                             event_type=event_type)
        return  meta_event, [] # Cannot continue too many XML files

    # Verify XML Structure, remove bad files
    for f in infiles:
        try:
            inspection.verify_xml_structure(input_dir+f)
        except errlog.XMLStructureInvalid:
            shutil.move(input_dir+f, invalid_xml_dir)
            invalid_xml.append(Record(None, f, invalid_xml_dir))
    if invalid_xml:
        event_type = EventType.objects.get(name="Deposit Failed: XML Validation Failed")
        [r.messages.append( (event_type, event_type.log_help_text) ) for r in invalid_xml]
        if len(infiles) == 1: # our only file is invalid
            log_path = write_logfile(log_root, log_text, profile_id, invalid_xml, infiles[0])
            meta_event = MetaEvent.objects.create(
                user=user,
                context=context,
                profile=profile,
                error_log_url=log_path)
            create_events(meta_event, event_type, invalid_xml)
            shutil.rmtree(invalid_xml_dir)
            return meta_event, []

    if profile.split_node:
        assert len(infiles) == 1
        original_xml_file = infiles[0]
        try:
            inspection.verify_split_node(input_dir + original_xml_file, profile.split_node)
        except errlog.InvalidSplitNode:
            event_type = EventType.objects.get(name="Deposit Failed: Invalid Record Separator")
            _log_text = log_text + "\n{}".format(event_type.log_help_text)
            log_path = write_logfile(log_root, _log_text, profile_id, '', 'too_many_files')
            meta_event = MetaEvent.objects.create(
                user=user,
                context=context,
                profile=profile,
                error_log_url=log_path)
            Event.objects.create(meta_event=meta_event,
                                 event_type=event_type)
            return  meta_event, [] # Cannot continue split node not found
    else:
        original_xml_file = None

    with clean_room(input_dir, profile_id) as _dir:
        xml_files = [x for x in os.listdir(_dir) if x.endswith('xml')]
        records = []
        for x in xml_files:
            with open(_dir+x, 'r', encoding="utf-8") as f:
                records.append(Record(parse(f).getroot(), x, _dir))

        unexpected_exceptions = []

        # Inspect given XML record for data integrity
        for r in records:
            try:
                r = inspection.xml_fields(r, profile_id)
            except Exception as e:
                msg = unexpected_msg.format("inspect xml fields", r.filename, profile_id, type(e))
                logr.info(msg)
                unexpected_exceptions.append(msg)
            try:
                r = inspection.content(r, profile_id)
            except Exception as e:
                msg = unexpected_msg.format("inspect content", r.filename, profile_id, type(e))
                logr.info(msg)
                unexpected_exceptions.append(msg)
            try:
                r = inspection.verify_no_duplicate(r, profile_id)
            except Exception as e:
                msg = unexpected_msg.format("verify no duplicates", r.filename, profile_id, type(e))
                logr.info(msg)
                unexpected_exceptions.append(msg)
        valid_records = [r for r in records if not r.messages]
        invalid_records = [r for r in records if r.messages]

        # Create DigObjects
        _records = []
        for r in valid_records:
            try:
                event_type = EventType.objects.get(name="Deposited")
                digobj_id = ingestion.create_digobj(r, profile_id, context_id, location_id)
                ingestion.import_values(digobj_id, profile_id, r)
                ingestion.create_files(digobj_id, profile_id, context_id, r)
                r.digobj_id = digobj_id
                r.messages.append((event_type, None))
                _records.append(r)
            except Exception as e:
                msg = unexpected_msg.format("create dig objects", r.filename, profile_id, type(e))
                logr.info(msg)
                unexpected_exceptions.append(msg)
        valid_records = _records

        img_files = [x for x in os.listdir(_dir) if x.split(".")[-1] in settings.ALLOWED_EXTS['images'] and not inspection.find_metadata(x, invalid_records, profile_id)]
        if len(img_files) > 0:
            event_type = EventType.objects.get(name="Deposit Failed: Metadata File Not Found")
            log_text += "\n{}\n".format(event_type.log_help_text)
            for i in img_files:
                log_text += "{}\n".format(i)

        # Log Errors
        # Only create a log file if we have something to write to it ev 2014-8-22
        if len(invalid_records) > 0 or len(invalid_xml) > 0 or len(img_files) > 0:
            log_path = write_logfile(log_root, log_text, profile_id, invalid_records + invalid_xml, original_xml_file)
        else:
            log_path = None

        meta_event = MetaEvent.objects.create(user=user, context=context, profile=profile, error_log_url=log_path)

        # Event Creation
        event_type = EventType.objects.get(name="Deposit Failed: Duplicate Records")
        create_events(meta_event, event_type, invalid_records)

        event_type = EventType.objects.get(name='Deposit Failed: Media File Not Found')
        create_events(meta_event, event_type, invalid_records)

        event_type = EventType.objects.get(name='Deposit Failed: Media URL Not Found')
        create_events(meta_event, event_type, invalid_records)

        event_type = EventType.objects.get(name='Deposit Failed: Missing Required Values')
        create_events(meta_event, event_type, invalid_records)

        event_type = EventType.objects.get(name='Deposit Failed: Missing Required Elements')
        create_events(meta_event, event_type, invalid_records)

        event_type = EventType.objects.get(name='Deposit Failed: Title Too Long')
        create_events(meta_event, event_type, invalid_records)

        # Success Events
        event_type = EventType.objects.get(name="Deposited")
        create_events(meta_event, event_type, valid_records)

        # Invalid XML
        event_type = EventType.objects.get(name="Deposit Failed: XML Validation Failed")
        create_events(meta_event, event_type, invalid_xml)
        shutil.rmtree(invalid_xml_dir)

        # Missing metadata
        if len(img_files) > 0:
            event_type = EventType.objects.get(name="Deposit Failed: Metadata File Not Found")
            for i in img_files:
                Event.objects.create(meta_event=meta_event,
                                     event_type=event_type)

        return meta_event, unexpected_exceptions

def generate_dc_xml(digobj_id, context_id):
    """
    Given a digital object, and context information;
    creates a dc.xml file on disk, for use by the XTF Portal.
    """

    return ingestion.create_dc_xml(digobj_id, context_id)

def generate_location_dcxml(location_id):
    """
    Given a location creates a dc.xml file on disk, for use by the XTF Portal.
    """

    return ingestion.create_location_dcxml(location_id)

def generate_institution_dcxml(institution_id):
    """
    Given an institution creates a dc.xml file on disk, for use by the XTF Portal.
    """

    return ingestion.create_institution_dcxml(institution_id)
