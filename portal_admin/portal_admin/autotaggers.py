import re

from portal_admin.models import (DigObjectFacetValue,
                                 Event,
                                 EventType,
                                 FacetGroup,
                                 FacetValue,
                                 ImportedValue,
                                 MatchType,
                                 TriggerTerm,
                                 ValueElement)

class DefaultAutotagger(object):
    def autotag(self, digobjects, dcxml_format, trigger_terms, meta_event):
        facet_counts = {}
        for t in trigger_terms:
            elements = ValueElement.objects.filter(**{'element__{}'.format(t.match_type.query): t.term})
            values = ImportedValue.objects.filter(**{'digobject__in': digobjects,
                                                     'dcxml_format': t.dcxml_format,
                                                     'value__in': elements})
            for v in values:
                if DigObjectFacetValue.objects.filter(digobject=v.digobject, facet_value=t.facet_value).count() == 0:
                    dfv = DigObjectFacetValue.objects.create(digobject=v.digobject,
                                                             facet_value=t.facet_value,
                                                             is_autoassigned=True,
                                                             trigger_term=t)
                    g = dfv.facet_value.facet_group.label
                    facet_counts[g] = facet_counts[g] + 1 if g in facet_counts else 1
                    meta_event.apply_tag_event(v.digobject)
        return facet_counts, []

class NamesAutotagger(object):
    def extract_date(self, name):
        date_res = [{"re": r'(\d{4}\-\d{4})', "suffix": ""},
                    {"re": r'(\d{4}\-)', "suffix": ""},
                    {"re": r'b. (\d{4})', "suffix": "-"}]

        for r in date_res:
            m = re.search(r["re"], name)
            if m != None:
                return m.groups()[0] + r["suffix"]
        return False

    def normalize(self, name):
        chunks = re.sub(r'\([^)]*\)', '', re.sub(r'\[[^)]*\]', '', name)).split("--")
        persname = r'^[\w\s]*, [\w\s\.]*(?:, \d{4}\-\d{4}|, \d{4}\-|, b. \d{4}|, d. \d{4}|,|)$'

        if re.match(persname, chunks[0]):
            names = chunks[0].split(",")
            date = self.extract_date(name)
            if date:
                return [names[0].strip("., "), names[1].strip("., "), date]

            return [names[0].strip("., "), names[1].strip("., ")]
        else:
            return [chunks[0].strip("., ")]

    def autotag(self, digobjects, dcxml_format, trigger_terms, meta_event):
        facet_counts = {}
        fvalues = []
        facet_group = FacetGroup.objects.get(label="Names")
        predicates = {'startswith': '^{}',
                      'exact': '^{}$',
                      'contains': '{}',
                      'istartswith': "(?i)^{}",
                      'iexact': "(?i)^{}$",
                      'icontains': "(?i){}"}

        trigger_terms = trigger_terms.filter(dcxml_format=dcxml_format)

        for v in ImportedValue.objects.filter(digobject__in=digobjects, dcxml_format=dcxml_format):
            tag_added = False
            # if there is a doubt signifier, [?] or (?), skip it.
            if re.search(r'(?:\[\?\]|\(\?\))', v.value.element):
                continue

            clean_value = ", ".join(self.normalize(v.value.element))

            for t in trigger_terms:
                if re.search(predicates[t.match_type.query].format(re.escape(t.term)), clean_value):
                    dfv, created = DigObjectFacetValue.objects.get_or_create(digobject=v.digobject,
                                                                             facet_value=t.facet_value,
                                                                             is_autoassigned=True,
                                                                             trigger_term=t)
                    if created:
                        tag_added = True
                        g = dfv.facet_value.facet_group.label
                        facet_counts[g] = facet_counts[g] + 1 if g in facet_counts else 1
                        meta_event.apply_tag_event(v.digobject)

            if not tag_added:
                fvalue, created = FacetValue.objects.get_or_create(value=clean_value,
                                                                   facet_group=facet_group)
                if created:
                    fvalues.append(fvalue.value)

                tag, created = DigObjectFacetValue.objects.get_or_create(digobject=v.digobject,
                                                                         facet_value=fvalue,
                                                                         is_autoassigned=True)
                if created:
                    g = tag.facet_value.facet_group.label
                    facet_counts[g] = facet_counts[g] + 1 if g in facet_counts else 1
                    meta_event.apply_tag_event(v.digobject)
        return facet_counts, fvalues
