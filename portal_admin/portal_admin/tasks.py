from __future__ import absolute_import
from django.utils.module_loading import import_by_path
from django.conf import settings

from celery.task import task

import shutil, subprocess, re, redis

from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.core.mail import send_mail

from django.db.models import Count

from django.contrib.auth.models import User
from portal_admin.models import (Context,
                                 DigObject,
                                 DigObjectContext,
                                 DigObjectStatus,
                                 Event,
                                 EventType,
                                 FacetGroup,
                                 ImportedValue,
                                 InstitutionContext,
                                 MetaEvent,
                                 Profile,
                                 TriggerTerm,)

from portal_admin import processor, autotag
from portal_admin.utils import ensure_symlinks, logfile_path, update_ark_url, get_obj_url, update_ark_status

from django.db.models import Q

def index_profile(profile_id, context_id, user_id, objs, status_name=None):
    profile = Profile.objects.get(pk=profile_id)
    context = Context.objects.get(pk=context_id)
    user = User.objects.get(pk=user_id)
    index_failed, created = EventType.objects.get_or_create(name="Indexing Failed: File Not Indexed")
    target_status = DigObjectStatus.objects.get(status=status_name) if status_name else False

    # Locked region
    #   1. update dc.xml files and symlinks in a single directory (both preview and publish)
    #   2. index both preview and publish if relevant
    # updating directory and indexing needs to be atomic so the output will confirm that all
    # the objects were, indeed indexed
    with redis.Redis().lock('indexer'):
        if status_name == "Published":
            published_objs = objs
        else:
            published_objs = objs.filter(digobject_status__status="Published")

        for obj in objs:
            dcxml_path = processor.generate_dc_xml(obj.digobject.id, context_id)
            ensure_symlinks(obj.digobject.id, context_id, 'Previewed')

        for obj in published_objs:
            ensure_symlinks(obj.digobject.id, context_id, 'Published')

        data_path = "{}/{}/{}/".format(profile.digobject_form._type,
                                       profile.institution.xtf_name,
                                       profile.pk)

        preview_arks, preview_out, preview_err = perform_reindex('preview', data_path)
        if published_objs.count() > 0:
            publish_arks, publish_out, publish_err = perform_reindex('publish', data_path)
        else:
            publish_arks, publish_out, publish_err = [], '', ''

    # *Not* in locked region
    # check for errors, update statuses
    if len(preview_arks) < objs.count() or len(publish_arks) < published_objs.count():
        logpath = logfile_path(profile.institution.xtf_name, profile.pk)
    else:
        logpath = None

    meta_event = MetaEvent.objects.create(profile=profile,
                                          context=context,
                                          user=user,
                                          error_log_url=logpath)

    preview_successful = 0
    publish_successful = 0
    preview_failed = 0
    publish_failed = 0

    for obj in objs.all():
        if obj.digobject.ark in preview_arks:
            preview_successful += 1
            if status_name == "Previewed" and obj.digobject_status.status == "Deposited":
                obj.digobject.set_status(context_id, target_status, meta_event)
            obj.set_dirty(False)
        else:
            preview_failed +=1
            obj.set_dirty(True)
            Event.objects.create(meta_event=meta_event,
                                 event_type=index_failed,
                                 digobject=obj.digobject)
        if status_name == "Published" or obj.digobject_status.status == "Published":
            if obj.digobject.ark in publish_arks:
                publish_successful += 1
                if status_name == "Published" and obj.digobject_status.status == "Previewed":
                    obj.digobject.set_status(context_id, target_status, meta_event)
                obj.set_dirty(False)
            else:
                publish_failed +=1
                obj.set_dirty(True)
                Event.objects.create(meta_event=meta_event,
                                     event_type=index_failed,
                                     digobject=obj.digobject)
                
    if logpath:
        with open((settings.FILE_ROOT + logpath), 'w') as err_log:
            err_log.write(render_to_string('preview/error_log.txt',
                                           {'preview_out': preview_out,
                                            'preview_err': preview_err,
                                            'publish_out': publish_out,
                                            'publish_err': publish_err,
                                            'errors': meta_event.event_set.filter(event_type=index_failed)}))
    return meta_event, preview_successful, publish_successful, preview_failed, publish_failed

def index_confirmation(context_id, user_id, meta_events,
                       preview_successful, publish_successful,
                       preview_failed, publish_failed,
                       host, status_name=None):
    context = Context.objects.get(pk=context_id)
    user = User.objects.get(pk=user_id)

    # We're all done, send the results.
    TO = list(context.userrole_set.filter(role__rolename="manager").values_list('user__email', flat=True))
    TO.append(user.email)

    data = {'context': context,
            'user': user.get_full_name(),
            'status': status_name,
            'previewed': preview_successful,
            'published': publish_successful,
            'preview_failed': preview_failed,
            'publish_failed': publish_failed,
            'meta_events': meta_events,
            'host': host}

    if settings.EMAIL_NOTIFICATIONS:
        send_mail(subject="{} Indexing Complete ({} errors)".format(context.name, (preview_failed + publish_failed)),
                  message=render_to_string('preview/preview_email.txt', data),
                  from_email=context.help_email,
                  recipient_list=TO)

@task(priority=9)
def reindex_institution(user_id, context_id, institution_id, host):
    meta_events = []
    preview_successful = 0
    publish_successful = 0
    preview_failed = 0
    publish_failed = 0
    for p in Profile.objects.filter(institution=institution_id):
        objs = DigObjectContext.objects.filter(digobject__profile=p,
                                               context=context_id,
                                               dirty=True)\
                                       .filter(Q(digobject_status__status="Previewed") |
                                               Q(digobject_status__status="Published"))
        if objs.count() > 0:
            meta_event, ps1, ps2, pf1, pf2 = index_profile(p.pk, context_id, user_id, objs)
            meta_events.append(meta_event)
            preview_successful += ps1
            publish_successful += ps2
            preview_failed += pf1
            publish_failed += pf2

    index_confirmation(context_id, user_id, meta_events,
                       preview_successful, publish_successful,
                       preview_failed, publish_failed, host)

@task(priority=9)
def start_indexer(user_id, context_id, objs_list, host, status_name=False):
    meta_events = []
    preview_successful = 0
    publish_successful = 0
    preview_failed = 0
    publish_failed = 0
    objs = DigObjectContext.objects.filter(pk__in=objs_list, context__pk=context_id)
    for p in Profile.objects.filter(pk__in=objs.values_list('digobject__profile', flat=True).distinct()):
        meta_event, ps1, ps2, pf1, pf2 = index_profile(p.pk, context_id, user_id,
                                                       objs.filter(digobject__profile=p,
                                                                   context=context_id),
                                                       status_name)
        meta_events.append(meta_event)
        preview_successful += ps1
        publish_successful += ps2
        preview_failed += pf1
        publish_failed += pf2

    index_confirmation(context_id, user_id, meta_events,
                       preview_successful, publish_successful,
                       preview_failed, publish_failed, host)

@task(priority=3)
def delete_objects(objs_list):
    objs = DigObject.objects.filter(pk__in=objs_list)
    dirs = {}
    for obj in objs:
        tclass = import_by_path(settings.IMPORT_TRANSFER_BACKEND)
        transfer = tclass()
        dirs.update(transfer.delete_files(obj, DigObjectStatus.objects.all()))
        update_ark_status(obj.ark, "unavailable")
        obj.delete()

    for d in dirs:
        status = dirs[d]
        if status in settings.INDEX_NAMES:
            path = d.split("/")
            arks, out, err = perform_reindex(settings.INDEX_NAMES[status], "/".join(path[-4:]))

def perform_reindex(index, directory):
    proc = subprocess.Popen([settings.XTF_INDEXER,
                             '-incremental',
                             '-rotate',
                             ('-dir ' + directory),
                             '-index ' + index],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)

    out, err = proc.communicate()
    out = out.decode('utf-8')
    err = err.decode('utf-8')
    indexed_files = re.findall(r'\([0-9]*%\)[ ]*Indexing \[([a-zA-z0-9\/]*)\.xml\] \.\.\. \([0-9]* stored keys\) \.\.\. Done.', out)
    arks = [x[x.rindex("/") + 1:] for x in indexed_files]
    return arks, out, err

@task(priority=0)
def update_digobject(user_id, context_id, digobj_id, tempdir, url):
    meta_event, unknown_exceptions = processor.update_digobject(user_id,
                                                                context_id,
                                                                (tempdir + "/"),
                                                                digobj_id)
    update_confirmation(user_id, context_id, meta_event.id, digobj_id, url)
    shutil.rmtree(tempdir)
    # if we got unexpected exceptions on the production server send an email to the portal managers so they
    # know to follow up with the user ev 2014-12-18
    if len(unknown_exceptions) > 0:
        send_unknown_error(user_id, DigObject.objects.get(pk=digobj_id).profile.pk, context_id, unknown_exceptions)


def update_confirmation(user_id, context_id, meta_event_id, digobject_id, url):
    user = User.objects.get(id=user_id)
    meta_event = MetaEvent.objects.get(id=meta_event_id)
    context = Context.objects.get(id=context_id)
    errors = meta_event.update_error_events()
    digobj = DigObject.objects.get(pk=digobject_id)

    ivalues = digobj.importedvalue_set.all()\
                                      .values("dcxml_format__field_label")\
                                      .annotate(count=Count("id"))\
                                      .order_by('dcxml_format__field_label')

    data = {'digobject': digobj,
            'context': context,
            'user': user.get_full_name(),
            'errors': errors.values("event_type__name").annotate(count=Count("id")).order_by("event_type__name"),
            'ivalues': ivalues}

    if errors.count() > 0:
        data['error_log'] = "http://{}{}".format(url, reverse('error_log', kwargs={'pk': meta_event.pk}))

    msg = render_to_string('deposit/update_email.txt', data)
    TO = [user.email]
    if settings.PRODUCTION_SERVER:
        TO.extend(context.userrole_set.filter(role__rolename="manager")\
                  .values_list('user__email', flat=True))

    if settings.EMAIL_NOTIFICATIONS:
        send_mail(subject="{} Update Completed ({} errors)".format(context.name, errors.count()),
                  message=msg,
                  from_email=context.help_email,
                  recipient_list=TO)


@task(priority=0)
def process_input(user_id, profile_id, context_id, tempdir, location_id, url):
    meta_event, unknown_exceptions = processor.process_input(user_id,
                                                             profile_id,
                                                             context_id,
                                                             (tempdir + "/"),
                                                             location_id)
    profile = Profile.objects.get(pk=profile_id)
    if InstitutionContext.objects.get(institution=profile.institution, context=context_id).use_auto_classify:
        facet_counts, fvalues = autotag.autotag([e.digobject for e in meta_event.event_set.exclude(digobject=None)],
            TriggerTerm.objects.filter(context__pk=context_id, is_active=True), meta_event)
    else:
        facet_counts = {}
        fvalues = {}
    send_email(user_id, profile_id, context_id, meta_event.id, url, facet_counts, fvalues)
    shutil.rmtree(tempdir)
    # if we got unexpected exceptions on the production server send an email to the portal managers so they
    # know to follow up with the user ev 2014-12-18
    if len(unknown_exceptions) > 0:
        send_unknown_error(user_id, profile_id, context_id, unknown_exceptions)

def send_unknown_error(user_id, profile_id, context_id, exceptions):
    user = User.objects.get(id=user_id)
    context = Context.objects.get(id=context_id)
    TO = context.userrole_set.filter(role__rolename="manager").values_list('user__email', flat=True)
    data = {'profile': Profile.objects.get(id=profile_id),
            'context': context,
            'user': user.get_full_name(),
            'errors': exceptions}
    if settings.EMAIL_NOTIFICATIONS:
        # send the errors to the portal managers
        send_mail(subject="Unexpected Exceptions",
                  message=render_to_string('deposit/error_email.txt', data),
                  from_email=context.help_email,
                  recipient_list=TO)
        # let the user know that something happened and we're working on it
        send_mail(subject="{} unexpected error".format(context.name),
                  message=render_to_string('deposit/unknown_error_email.txt', {}),
                  from_email=context.help_email,
                  recipient_list=[user.email])


def send_email(user_id, profile_id, context_id, meta_event_id, url, facet_counts, fvalues):
    user = User.objects.get(id=user_id)
    meta_event = MetaEvent.objects.get(id=meta_event_id)
    context = Context.objects.get(id=context_id)
    deposits = meta_event.deposit_events()
    errors = meta_event.deposit_error_events()

    ivalues = ImportedValue.objects.filter(digobject__in=deposits.values_list('digobject', flat=True))\
                                   .values("dcxml_format__field_label")\
                                   .annotate(count=Count("id"))\
                                   .order_by('dcxml_format__field_label')

    facets = []
    for f in facet_counts:
        facets.append({'label': f, 'count': facet_counts[f]})

    data = {'profile': Profile.objects.get(id=profile_id),
            'context': context,
            'user': user.get_full_name(),
            'total_deposits': deposits.count(),
            'total_errors': errors.count(),
            'errors': errors.values("event_type__name").annotate(count=Count("id")).order_by("event_type__name"),
            'ivalues': ivalues,
            'facets': facets,
            'facet_values': fvalues}

    if errors.count() > 0:
        data['error_log'] = "http://{}{}".format(url, reverse('error_log', kwargs={'pk': meta_event.pk}))

    msg = render_to_string('deposit/deposit_email.txt', data)
    TO = [user.email]
    if settings.PRODUCTION_SERVER:
        TO.extend(context.userrole_set.filter(role__rolename="manager")\
                  .values_list('user__email', flat=True))

    if settings.EMAIL_NOTIFICATIONS:
        send_mail(subject="{} Deposit Completed ({} errors)".format(context.name, errors.count()),
                  message=msg,
                  from_email=context.help_email,
                  recipient_list=TO)


