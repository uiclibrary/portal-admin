from django.test import TestCase
from portal_admin import autotag
from portal_admin.models import Context, DigObject, DigObjectFacetValue, ImportedValue, MetaEvent, Profile, TriggerTerm, ValueElement
from django.contrib.auth.models import User

class Autotag(TestCase):
    fixtures = ['initial_data.json',
                'test/test_ccc.json',
                'test/test_users.json']

    def execute_test(self, tt, _value):
        context = Context.objects.get(name="Chicago Collections Consortium")
        profile = Profile.objects.get(name="AIC EAD Finding Aids")
        user = User.objects.get(pk=1)
        obj = DigObject.objects.create(ark='0123456789',
                                       title='Title',
                                       profile=profile,
                                       digobject_form=profile.digobject_form)
        element, created = ValueElement.objects.get_or_create(element = _value)
        ImportedValue.objects.create(value=element,
                                     digobject=obj,
                                     dcxml_format=tt.dcxml_format)
        autotag.autotag([obj], 
                        TriggerTerm.objects.filter(context=context), 
                        MetaEvent.objects.create(context=context, user=user))
        return obj

    def match_test(self, term, _value):
        tt = TriggerTerm.objects.get(term=term)
        obj = self.execute_test(tt, _value)
        values = DigObjectFacetValue.objects.filter(digobject=obj)
        self.assertEqual(values.count(), 1)
        self.assertEqual(values[0].digobject, obj)
        self.assertEqual(values[0].facet_value, tt.facet_value)
        self.assertEqual(values[0].is_autoassigned, True)
        self.assertEqual(values[0].trigger_term, tt)
        self.assertEqual(obj.event_set.count(), 1)

    def mismatch_test(self, term, _value):
        tt = TriggerTerm.objects.get(term=term)
        obj = self.execute_test(tt, _value)
        values = DigObjectFacetValue.objects.filter(digobject=obj)
        self.assertEqual(values.count(), 0)
        self.assertEqual(obj.event_set.count(), 0)

    def test_topics_contains(self):
        self.match_test("Crime", "Foo Crime Foo")
        self.mismatch_test("Crime", "Foo Crim Foo")

    def test_topics_exact(self):
        self.match_test("Disaster", "Disaster")
        self.mismatch_test("Disaster", "Disaster Foo")

    def test_neighborhoods_exact(self):
        self.match_test("Back of the Yards", "Back of the Yards")
        self.mismatch_test("Back of the Yards", "Back of the Yards Foo")

    #def test_cities_contains(self):
    #    self.match_test("Chicago", "Foo Chicago Foo")
    #    self.mismatch_test("Chicago", "Foo Chicag Foo")

    def test_names_exact(self):
        self.match_test("South Side Community Art Center", "South Side Community Art Center")

    def test_names_homesandhaunts(self):
        self.match_test("Faggi, Alfeo", "Faggi, Alfeo -- Homes and Haunts")

    def execute_namestest(self, _value, result):
        tt = TriggerTerm.objects.get(term="Faggi, Alfeo")
        obj = self.execute_test(tt, _value)
        values = DigObjectFacetValue.objects.filter(digobject=obj)
        self.assertEqual(values.count(), 1)
        self.assertEqual(values[0].digobject, obj)
        self.assertEqual(values[0].facet_value.value, result)
        self.assertEqual(values[0].is_autoassigned, True)
        self.assertEqual(values[0].trigger_term, None)
        self.assertEqual(obj.event_set.count(), 1)        

    def test_names_createfacet(self):
        self.execute_namestest("A.C. McClurg & Co. -- Fire, 1899 -- Photographs.", "A.C. McClurg & Co")

    def test_names_parens(self):
        self.execute_namestest("A. O. Smith Building (Milwaukee, Wis.)", "A. O. Smith Building")

    def test_names_doubt(self):
        tt = TriggerTerm.objects.get(term="Faggi, Alfeo")
        obj = self.execute_test(tt, "Allgeier, C. [Chicago] [?]")
        values = DigObjectFacetValue.objects.filter(digobject=obj)
        self.assertEqual(values.count(), 0)

    def test_names_dates(self):
        self.execute_namestest("Angle, Paul M. -- (Paul McClelland), -- 1900-1975 -- Archives.",
                               "Angle, Paul M, 1900-1975")
        self.execute_namestest("Agnew, Frances, b. 1891",
                               "Agnew, Frances, 1891-")
        self.execute_namestest("Anderson, Eleanor Copenhaver, d. 1985",
                               "Anderson, Eleanor Copenhaver")

    def test_names_companynames(self):
        self.execute_namestest("Babcock, H.P., & Wilcox Boilers [contractors]",
                               "Babcock, H.P., & Wilcox Boilers")
        self.execute_namestest("Booth, Nagle & Hartray", "Booth, Nagle & Hartray")
        self.execute_namestest("Caretti, John, & Company", "Caretti, John, & Company")

    def test_names_unicode(self):
        self.execute_namestest("Moholy-Nagy, László, 1895-1946.",
                               "Moholy-Nagy, László, 1895-1946")

    def test_names_startswith(self):
        t = TriggerTerm.objects.get(term="Addams, Jane")
        obj = self.execute_test(t, "Addams, Jane.")
        values = DigObjectFacetValue.objects.filter(digobject=obj)
        self.assertEqual(values.count(), 1)
        self.assertEqual(values[0].digobject, obj)
        self.assertEqual(values[0].facet_value.value, "Addams, Jane")
        self.assertEqual(values[0].is_autoassigned, True)
        self.assertEqual(values[0].trigger_term, t)
        self.assertEqual(obj.event_set.count(), 1)        
