import os, shutil
from django.test import TestCase
from django.core.urlresolvers import reverse
from selenium import webdriver
from .selenium.SeleniumTest import SeleniumTest
from portal_admin.models import DigObject, Connection, DigObjectStatus, Profile, Context, Event, EventType, MetaEvent
from portal_admin.processor import process_input
from django.conf import settings
from django.contrib.auth.models import User
from portal_admin.tasks import start_indexer

class PublishMain(TestCase):
    '''
    Tests to ensure proper execution of conversion of records from Deposited to Publish,
    with all necessary subroutines.
    '''
    fixtures = ['initial_data.json', 'test/test_ccc.json', 'test/test_users.json']

    def setUp(self):
        self.status_deposited = DigObjectStatus.objects.get(status="Deposited")
        self.status_previewed = DigObjectStatus.objects.get(status="Previewed")
        self.status_published = DigObjectStatus.objects.get(status="Published")

        self.test_dir1 = 'portal_admin/tests/test_data/ccc_idot/preview/'
        self.profile1 = Profile.objects.get(name='IDOT content dm images')

        self.test_dir2 = 'portal_admin/tests/test_data/uic_ead/'
        self.profile2 = Profile.objects.get(name='EAD')

        self.context = Context.objects.get(name='Chicago Collections Consortium')
        self.location = self.profile1.institution.location_set.all()[0]
        self.user = User.objects.get(username="kate")

        # make sure files don't exist before deposit
        file_root = settings.FILE_ROOT
        if os.path.isdir(file_root):
            shutil.rmtree(file_root)

        process_input(self.user.id, self.profile1.id, self.context.id, self.test_dir1, self.location.id)
        process_input(self.user.id, self.profile2.id, self.context.id, self.test_dir2, self.location.id)

        self.obj1 = DigObject.objects.filter(profile=self.profile1)[0]
        self.obj2 = DigObject.objects.filter(profile=self.profile2)[0]

        start_indexer(self.user.id,
                      self.context.id,
                      [self.obj1.pk, self.obj2.pk],
                      "http://localhost/",
                      "Previewed")


    def tearDown(self):
        # clean up after deposit
        #file_root = settings.FILE_ROOT
        #if os.path.isdir(file_root):
        #    shutil.rmtree(file_root)
        pass

    def test_previewed(self):
        '''object should have been properly deposited'''
        deposited = DigObject.objects.filter(digobjectcontext__digobject_status__status="Deposited")
        previewed = DigObject.objects.filter(digobjectcontext__digobject_status__status="Previewed")
        published = DigObject.objects.filter(digobjectcontext__digobject_status__status="Published")
        self.assertEqual(deposited.count(), 32)
        self.assertEqual(previewed.count(), 2)
        self.assertEqual(published.count(), 0)

    def test_change_status_to_publish(self):
        '''object status should change to "Published" from "Previewed"'''

        self.assertEqual(self.obj1.get_status(self.context), self.status_previewed)
        self.assertEqual(self.obj2.get_status(self.context), self.status_previewed)

        start_indexer(self.user.id,
                      self.context.id,
                      [self.obj1.pk, self.obj2.pk],
                      "http://localhost/",
                      "Published")

        self.assertEqual(self.obj1.get_status(self.context), self.status_published)
        self.assertEqual(self.obj2.get_status(self.context), self.status_published)

        deposited = DigObject.objects.filter(digobjectcontext__digobject_status__status="Deposited")
        previewed = DigObject.objects.filter(digobjectcontext__digobject_status__status="Previewed")
        published = DigObject.objects.filter(digobjectcontext__digobject_status__status="Published")
        self.assertEqual(deposited.count(), 32)
        self.assertEqual(previewed.count(), 0)
        self.assertEqual(published.count(), 2)

    def test_duplicate_directory_structure(self):
        '''proper directory structure should be created for published objects'''

        deposited_dirs = ["{}Deposited/data/images/uic/1".format(settings.FILE_ROOT),
                          "{}Deposited/media/images/uic/1/thumbs".format(settings.FILE_ROOT)]

        published_dirs = ["{}Published/data/images/uic/1".format(settings.FILE_ROOT),
                          "{}Published/media/images/uic/1/thumbs".format(settings.FILE_ROOT)]

        for d in deposited_dirs:
            self.assertTrue(os.path.isdir(d))
        for d in published_dirs:
            self.assertFalse(os.path.isdir(d))

        start_indexer(self.user.id,
                      self.context.id,
                      [self.obj1.pk, self.obj2.pk],
                      "http://localhost/",
                      "Published")

        for d in deposited_dirs:
            self.assertTrue(os.path.isdir(d))
        for d in published_dirs:
            self.assertTrue(os.path.isdir(d))


    def test_event_created(self):
        '''publish events and meta_events should execute properly'''

        # cursory examination of event quantities
        events_0      = Event.objects.all()
        meta_events_0 = MetaEvent.objects.all()

        events_quantity = len(events_0)
        meta_events_quantity = len(meta_events_0)

        start_indexer(self.user.id,
                      self.context.id,
                      [self.obj1.pk],
                      "http://localhost/",
                      "Published")

        events_1      = Event.objects.all()
        meta_events_1 = MetaEvent.objects.all()
        self.assertEqual(len(events_1), events_quantity + 1)
        self.assertEqual(len(meta_events_1), meta_events_quantity + 1)

        start_indexer(self.user.id,
                      self.context.id,
                      [self.obj2.pk],
                      "http://localhost/",
                      "Published")

        events_2      = Event.objects.all()
        meta_events_2 = MetaEvent.objects.all()
        self.assertEqual(len(events_2), events_quantity + 2)
        self.assertEqual(len(meta_events_2), meta_events_quantity + 2)

        events_new = []
        for event in events_2:
            if event not in events_0:
                events_new.append(event)
        self.assertEqual(len(events_new), 2)

        meta_events_new = []
        for meta_event in meta_events_2:
            if meta_event not in meta_events_0:
                meta_events_new.append(meta_event)
        self.assertEqual(len(meta_events_new), 2)

        event1 = Event.objects.get(event_type__name='Status Change: Published',
                                   digobject=self.obj1)
        event2 = Event.objects.get(event_type__name='Status Change: Published',
                                   digobject = self.obj2)
        self.assertTrue(event1)
        self.assertTrue(event1)

    def test_symlinks(self):
        '''proper file structure should be created for each published object'''
        start_indexer(self.user.id,
                      self.context.id,
                      [self.obj1.pk, self.obj2.pk],
                      "http://localhost/",
                      "Published")

        i_data = "{}Deposited/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, self.obj1.ark)
        i_media = "{}Deposited/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, self.obj1.ark)
        i_thumb = "{}Deposited/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, self.obj1.ark)
        p_data = "{}Published/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, self.obj1.ark)
        p_media = "{}Published/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, self.obj1.ark)
        p_thumb = "{}Published/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, self.obj1.ark)

        self.assertTrue(os.path.isfile(i_data))
        self.assertTrue(os.path.isfile(i_media))
        self.assertTrue(os.path.isfile(i_thumb))
        self.assertTrue(os.path.isfile(p_data))
        self.assertTrue(os.path.isfile(p_media))
        self.assertTrue(os.path.isfile(p_thumb))

        i_data = "{}Deposited/data/archivalcollections/uic/{}/{}.xml".format(settings.FILE_ROOT, self.profile2.pk, self.obj2.ark)
        p_data = "{}Published/data/archivalcollections/uic/{}/{}.xml".format(settings.FILE_ROOT, self.profile2.pk, self.obj2.ark)

        self.assertTrue(os.path.isfile(i_data))
        self.assertTrue(os.path.isfile(p_data))

        obj3 = DigObject.objects.filter(digobjectcontext__digobject_status__status="Deposited")[0]
        obj3_i_xml = "{}Deposited/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, obj3.ark)
        obj3_i_img = "{}Deposited/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, obj3.ark)
        obj3_i_thumb = "{}Deposited/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, obj3.ark)
        obj3_p_xml = "{}Published/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, obj3.ark)
        obj3_p_img = "{}Published/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, obj3.ark)
        obj3_p_thumb = "{}Published/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, obj3.ark)

        self.assertTrue(os.path.isfile(obj3_i_xml))
        self.assertTrue(os.path.isfile(obj3_i_img))
        self.assertTrue(os.path.isfile(obj3_i_thumb))
        self.assertFalse(os.path.isfile(obj3_p_xml))
        self.assertFalse(os.path.isfile(obj3_p_img))
        self.assertFalse(os.path.isfile(obj3_p_thumb))
