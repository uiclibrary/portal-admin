from .ProfileTest import ProfileTest

from portal_admin.forms.deposit import ProfileFieldFormSet, ElementSourceFormSet

from portal_admin.models import DCXMLFormat

class ProfileField(ProfileTest):

    def verify_element(self, element, xpath, instance_number=-1, delimiter=None, default_text=None):
        self.assertEqual(element.xpath, xpath)
        self.assertEqual(element.dcxml_format, self.dcxml_format)
        self.assertEqual(element.instance_number, instance_number)
        self.assertEqual(element.delimiter, delimiter)
        self.assertEqual(element.default_text, default_text)

    def test_formset(self):
        profile_fields = [self.create_profile_field(),
                          self.create_profile_field(attribute=True),
                          self.create_profile_field(attribute=True, attribute_value=True),
                          self.create_profile_field(instance=True)]

        data = {"form-TOTAL_FORMS": 4,
                "form-INITIAL_FORMS": 0,
                "form-MAX_NUM_FORMS": 1000,
                "form-0-TOTAL_FORMS": 1,
                "form-0-INITIAL_FORMS": 1,
                "form-0-MAX_NUM_FORMS": 1,
                "form-1-TOTAL_FORMS": 1,
                "form-1-INITIAL_FORMS": 1,
                "form-1-MAX_NUM_FORMS": 1,
                "form-2-TOTAL_FORMS": 1,
                "form-2-INITIAL_FORMS": 1,
                "form-2-MAX_NUM_FORMS": 1,
                "form-3-TOTAL_FORMS": 1,
                "form-3-INITIAL_FORMS": 1,
                "form-3-MAX_NUM_FORMS": 1,
                "form-0-0-xpath": "/title/0",
                "form-1-0-xpath": "/title/1",
                "form-1-0-attribute": "date",
                "form-2-0-xpath": "/title/2",
                "form-2-0-attribute": "date",
                "form-2-0-attribute_value": "12/12/12",
                "form-3-0-xpath": "/title/4",
                "form-3-0-instance": "2"}

        formset = ProfileFieldFormSet(data, profile_fields=profile_fields)

        self.assertTrue(formset.is_valid())

        elements = formset.save(self.profile)
                
        self.verify_element(elements[0][0], "/title/0")
        self.verify_element(elements[1][0], "/title/1/@date")
        self.verify_element(elements[2][0], "/title/2[@date=\"12/12/12\"]")
        self.verify_element(elements[3][0], "/title/4", instance_number=2)

    def test_formset_invalid(self):
        profile_fields = [self.create_profile_field()]
        data = {"form-TOTAL_FORMS": 1,
                "form-INITIAL_FORMS": 0,
                "form-MAX_NUM_FORMS": 1000,
                "form-0-TOTAL_FORMS": 1,
                "form-0-INITIAL_FORMS": 1,
                "form-0-MAX_NUM_FORMS": 1}

        formset = ProfileFieldFormSet(data, profile_fields=profile_fields)

        self.assertFalse(formset.is_valid())
        self.assertEqual(len(formset.errors), 1)

    def test_formset_non_required(self):
        pf = self.create_profile_field(instance=True)
        pf.dcxml_format = DCXMLFormat(dcxml_purpose="description",
                                      field_label="description",
                                      element_name="description",
                                      is_required=False,
                                      use_dcxml_element=False,
                                      concat_order=None,
                                      separator=None)
        profile_fields = [self.create_profile_field(), pf]
        data = {"form-TOTAL_FORMS": 2,
                "form-INITIAL_FORMS": 0,
                "form-MAX_NUM_FORMS": 1000,
                "form-0-TOTAL_FORMS": 1,
                "form-0-INITIAL_FORMS": 1,
                "form-0-MAX_NUM_FORMS": 1,
                "form-1-TOTAL_FORMS": 1,
                "form-1-INITIAL_FORMS": 1,
                "form-1-MAX_NUM_FORMS": 1,
                "form-0-0-xpath": "/title/0"}

        formset = ProfileFieldFormSet(data, profile_fields=profile_fields)

        self.assertTrue(formset.is_valid())

        elements = formset.save(self.profile)

        self.assertEqual(len(elements), 1)

        self.verify_element(elements[0][0], "/title/0")

    def test_add_fields(self):
        pf = self.create_profile_field()
        pf.can_add_widget = True
        data = {"form-TOTAL_FORMS": 2,
                "form-INITIAL_FORMS": 1,
                "form-MAX_NUM_FORMS": 10,
                "form-0-xpath": "foo",
                "form-1-xpath": "boo"}
        f = ElementSourceFormSet(data, profile_field=pf)
        self.assertTrue(f.is_valid())
        e = f.save(self.profile)
        self.assertTrue(len(e), 2)
        self.verify_element(e[0], "foo")
        self.verify_element(e[1], "boo")

