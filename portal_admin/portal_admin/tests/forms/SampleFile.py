from .ProfileTest import ProfileTest

from django.core.files.uploadedfile import SimpleUploadedFile

from portal_admin.forms.deposit import SampleFileForm

class SampleFile(ProfileTest):

    def test_samplefileform(self):
        upload_file = open("portal_admin/tests/test_data/uchicago/processor/apf3-00010.xml", "rb")
        form = SampleFileForm({}, {'sample_file': SimpleUploadedFile(upload_file.name, upload_file.read())})
        self.assertTrue(form.is_valid())

    def test_samplefileform_notxml(self):
        upload_file = open("portal_admin/tests/test_data/uchicago/processor/apf3-00010.jpg", "rb")
        form = SampleFileForm({}, {'sample_file': SimpleUploadedFile(upload_file.name, upload_file.read())})
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)
        self.assertIn("File is not XML", str(form.errors))

    def test_samplefileform_invalidxml(self):
        upload_file = open("portal_admin/tests/test_data/ccc_idot/local_bad_xml/ccc_uic_idot_test.xml", "rb")
        form = SampleFileForm({}, {'sample_file': SimpleUploadedFile(upload_file.name, upload_file.read())})
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)
        self.assertIn("File is not valid XML", str(form.errors))
