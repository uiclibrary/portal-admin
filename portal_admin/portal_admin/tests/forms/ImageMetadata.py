from .ProfileTest import ProfileTest

from portal_admin.forms.deposit import ImageMetadataForm

class ImageMetadata(ProfileTest):

    def test_imagemetadataform(self):
        form = ImageMetadataForm({'record_separator': "record",
                                  'image_filename': "image/filename",
                                  'filename_instance': 0,
                                  'use_filename': False,
                                  'image_suffix': "-img",
                                  'has_extension': True,
                                  'image_submission': False})
        
        self.assertTrue(form.is_valid())
        fs, profile = form.save(self.profile)
        
        self.assertEqual(profile.split_node, "record")
        self.assertFalse(profile.is_remote)
        self.assertEqual(fs.xpath, "image/filename")
        self.assertEqual(fs.instance_number, 0)
        self.assertEqual(fs.profile, profile)
        self.assertEqual(fs.has_extension, True)
        self.assertEqual(fs.suffix, "-img")

    def test_imagemetadataform_invalid(self):
        form = ImageMetadataForm({})

        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 2)

    def test_imagemetadataforms_withsplitnode_invalid(self):
        form = ImageMetadataForm({}, use_splitnode=True)

        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 3)

    def test_imagemetadata_remote_invalid(self):
        form = ImageMetadataForm({}, is_remote=True)
        
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)    

    def test_imagemetadata_remote_splitnode_invalid(self):
        form = ImageMetadataForm({}, use_splitnode=True, is_remote=True)
        
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 3)
