<?xml version="1.0" standalone="no"?>
<!DOCTYPE ead PUBLIC "+//ISBN 1-931666-00-8//DTD ead.dtd (Encoded Archival Description (EAD) Version 2002)//EN" "ead.dtd">
<ead relatedencoding="MARC21"> 
  <eadheader langencoding="iso639-2b" audience="internal" findaidstatus="edited-full-draft" countryencoding="iso3166-1" scriptencoding="iso15924" dateencoding="iso8601"> 
	 <eadid countrycode="us" mainagencycode="ica" publicid="ica200806">PUBLIC "-//The Art Institute
		of Chicago::Ryerson and Burnham Archives//TEXT(US::ICA::2008.6::JOHN B. FISCHER PAPERS, c.1900-c.1920s//EN)" "ica200806.xml"</eadid> 
	 <filedesc> 
		<titlestmt> 
		  <titleproper encodinganalog="245$a">Fischer, John B., Papers, 
		    <date era="ce" calendar="gregorian" type="inclusive" normal="1890/1930">c.1900-c.1920s</date></titleproper> 
		  <author encodinganalog="245$c">Inventory prepared by Staff of the </author> 
		</titlestmt>
		 
		<publicationstmt> 
		  <publisher encodinganalog="260$b">Ryerson and Burnham Archives, Ryerson and Burnham Libraries,
			 The Art Institute of Chicago,</publisher> 
		  <date era="ce" calendar="gregorian" encodinganalog="260$c" normal="2009">2009</date> 
		</publicationstmt> 
	 </filedesc>
	  
	 <profiledesc> 
		<creation encodinganalog="500">Text tagged in EAD by Jennifer Kirmer 
		  <date era="ce" calendar="gregorian" normal="200905"> May 2009</date>
		  </creation> 
		<langusage>Finding aid written in <language encodinganalog="546" langcode="eng">English</language>
		  </langusage> 
	 </profiledesc>
	  
	 <revisiondesc encodinganalog="583" audience="internal"> 
		<change> 
<date normal="2009" era="ce" calendar="gregorian">2009</date>		   
		  <item>Finding aid revised by Jennifer Kirmer.</item> 
		</change> 
		<change> 
		  <date normal="20090529" era="ce" calendar="gregorian">May 29, 2009
			</date> 
		  <item>Edited in Oxygen 9.1 by Jennifer Kirmer. </item> 
		</change> 
	 </revisiondesc>	  
  </eadheader>
   
  <frontmatter> 
	 <titlepage>
		<p>&#169;Ryerson and Burnham Archives, The Art Institute of Chicago.
		All rights reserved.</p> 
	 </titlepage> 
  </frontmatter>
   
  <archdesc level="collection" type="inventory"> 
	 <did> 
		<unittitle encodinganalog="245$a">John B. Fischer Papers, </unittitle> 
	   <unitdate encodinganalog="245$f" era="ce" calendar="gregorian" type="inclusive" normal="1890/1930">c.1900-c.1920s</unitdate>		
		 
		<unitid countrycode="us" repositorycode="ica" encodinganalog="099">2008.6</unitid>
		 
		<physdesc encodinganalog="300"><extent>.5 linear foot</extent>(<extent>1 box</extent>) and <extent>1 portfolio</extent></physdesc>
		 
		<repository encodinganalog="852"> 
		  <corpname>Ryerson and Burnham Archives, <subarea>Ryerson and Burnham
			 Libraries, </subarea> <subarea>The Art Institute of Chicago</subarea></corpname> 
		  <address> 
			 <addressline>111 S. Michigan Ave.</addressline> 
			 <addressline>Chicago, IL 60603-6110</addressline> 
			 <addressline>(312) 443-7292</addressline> 
			 <addressline>rbarchives@artic.edu</addressline> 
		  </address> 
		  <extref href="http://www.artic.edu/aic/libraries/rbarchives/rbarchives.html">http://www.artic.edu/aic/libraries/rbarchives/rbarchives.html</extref></repository>
		
		<abstract encodinganalog="520$a">Photo albums, correspondence and a manuscript relating to projects worked on or supervised by John Baptiste Fischer.
</abstract>
 
<origination> 
		  <persname role="collector" encodinganalog="600">Fischer, John Baptiste</persname></origination>

<physdesc encodinganalog="300"><genreform>Photographs</genreform>, <genreform>manuscripts</genreform>, and <genreform>correspondence</genreform>.</physdesc>
 
<physloc encodinganalog="852$z">The collection is housed in the Ryerson and Burnham Libraries' on-site stacks.</physloc>
		   
<langmaterial encodinganalog="546"> <language langcode="eng">English.</language> </langmaterial> 
	 </did>
	  
<bioghist encodinganalog="545">
<p>
  John Baptiste Fischer worked as Head Draughtsman for the firm of Shepley, Rutan, and Coolidge in Chicago, Illinois from 1901-1909. 
  Around this time he became affilitated with Postle and Fischer and worked in this firm as a lead designer and engineer for many projects from 
  during the 1910s and 1920s. Although Fischer was based in Chicago, he worked on projects throughout the country in locations such as Phoenix, 
  Arizona and Casper, Wyoming. Fischer designed many buildings on the University of Chicago campus including the Law School, 
  Gymnasium, and William Rainey Harper Memorial Library. Fischer designed many Gothic buildings, some of which are documented in 
  this collection. Fischer was also contributing editor for the Architectural Record in February 1905. 
</p>
</bioghist>
 
	 <scopecontent encodinganalog="520"> 
		<p>This collection documents a number of projects on which John Baptiste Fischer worked as designer and engineer. 
		  The firms Shepley, Rutan, and Coolidge and Postle and Fischer are well represented in this collection. Series I, Photographs, 
		  consists of four photo albums, a large portfolio, and four unidentified photos. These photos include floor plans, construction views, 
		  built views, and some recommendation letters. A number of the photographs have John B. Fischer's signature on the verso. Series II, Mauscripts, is 
		  comprised of an speech draft aimed at an unidentified court in a plea for the formation of a "League of Nations." Series III, 
		  Correspondence, contains a fan letter written to Fischer by artist A.L. van den Berghen in 1909 in reference to Fischer's book on 
		  missions. Also included is an undated New Year's card from Marion Mahony Griffin.</p>
	 </scopecontent>
	  
	 <arrangement encodinganalog="351">
<p>SERIES I: PHOTOGRAPHS. Arranged first by album with the photographs in their original order within the albums.	
</p>
<p>SERIES II: MANUSCRIPTS. Arranged first alphabetically by title, then chronologically.	
</p>
<p>SERIES III: CORRESPONDENCE. Arranged first alphabetically by correspondent, then chronologically.	
</p>
</arrangement>
 
	 <controlaccess> 
	   <p>This collection and other related materials may be found under the following headings in online catalogs:</p>

<p><persname encodinganalog="100">Fischer, John Baptiste</persname></p>

<p><corpname encodinganalog="610">Postle and Fischer</corpname></p>
	   
<p><corpname encodinganalog="610">Postle &amp; Fischer</corpname></p>
	   
<p><corpname encodinganalog="610">Shepley, Rutan, and Coolidge</corpname></p>
</controlaccess>
 
	 <note type="abbr"> 
<p><abbr expan="Art Institute of Chicago">AIC</abbr></p>
<p><abbr expan="autograph letter signed">AL</abbr></p>	 
<p><abbr expan="Box #, Folder #">BOX.FF</abbr></p>
<p><abbr expan="Portfolio">Portf.</abbr></p>
<p><abbr expan="circa">c.</abbr></p>
<p><abbr expan="not dated">n.d.</abbr></p> 
	 </note> 

<accessrestrict encodinganalog="506"> 
  <p>This collection may be used by qualified readers in the Reading Room of the Ryerson and Burnham Libraries at The Art Institute of Chicago. Collections maintained on-site are available for patron use without prior arrangement or appointment. Collections maintained in off-site storage will be retrieved with advance notification; please consult the Archivist for the current retrieval schedule. For further information, consult <extref href="http://www.artic.edu/aic/libraries/rbarchives/rbarchaccess.html">http://www.artic.edu/aic/libraries/rbarchives/rbarchaccess.html</extref></p>
</accessrestrict> 
    
<userestrict encodinganalog="540"> 
  <p>The Art Institute of Chicago is providing access to the materials in the Libraries' collections solely for noncommercial educational and research purposes. The unauthorized use, including, but not limited to, publication of the materials without the prior written permission of the Art Institute is strictly prohibited. All inquiries regarding permission to publish should be submitted in writing to the Archivist, Ryerson and Burnham Archives, The Art
Institute of Chicago. In addition to permission from the Art Institute, permission of the copyright owner (if not the Art Institute) and/or any holder of other rights (such as publicity and/or privacy rights) may also be required for reproduction, publication, distribution, and other uses. Responsibility for making an independent legal assessment of any item and securing any necessary permissions rests with the persons desiring to publish the item. The Art Institute makes no warranties as to the accuracy of the materials or their fitness for a particular purpose.</p> 
</userestrict>
	  
<prefercite encodinganalog="524"> 
		<p>John B. Fischer Papers, Ryerson and Burnham Archives, The Art Institute of Chicago.</p> 
</prefercite>
	  
<acqinfo encodinganalog="541">
		<p>The majority of this collection was purchased by the Ryerson &amp; Burnham Libraries in 2008. One piece of correspondence was found in September 2008 in a book previously owned by John B. Fischer, now in the possession of the Ryerson &amp; Burnham Libraries. </p> 
</acqinfo>
	 
<processinfo encodinganalog="583">
		<p>This collection was processed by Jennifer Kirmer of the Ryerson and Burnham Archives in May 2009. The finding aid was created and revised by Jennifer Kirmer in May 2009.</p> 
</processinfo>
	  
<dsc type="in-depth"> 

<c01 level="series">
  <did>
    <unittitle>SERIES I: PHOTOGRAPHS</unittitle>
  </did>
  <c02 level="item">
    <did> 
      <container type="box-folder">1.1</container> 
			<unittitle>Photo album 1.</unittitle> 
			<unitdate>n.d.</unitdate> 
			<physdesc><extent unit="items">1</extent><genreform>Album</genreform>.</physdesc>			 
    </did>
    <scopecontent><p>Including but not limited to these projects designed or supervised by John B. Fischer, Postle &amp; Fischer, Architects &amp; Engineers, 
      and Shepley, Rutan &amp; Coolidge, Architects: Apache Hotel (Phoenix, Ariz.), Borden's Condensed Milk Plant (Elgin, Ill.), 
      Borland Office Building (Chicago, Ill.), Corn Exchange Bank Building (Chicago, Ill.), Saint John's Lutheran Church (Elgin, Ill.), 
      and University of Oklahoma buildings. Multiple buildings for the University of Chicago including: Law School, Leon Mandle Hall, 
      Reynolds Clark and Founders Tower, William Rainey Harper Memorial Library, and Women's Quadrangle. 44 photographs total.</p></scopecontent>		   
  </c02>
  
  <c02 level="item">
    <did> 
      <container type="box-folder">1.2</container> 
      <unittitle>Photo album 2.</unittitle> 
      <unitdate>n.d.</unitdate> 
      <physdesc><extent unit="items">1</extent><genreform>Album</genreform>.</physdesc>			 
    </did>
    <scopecontent><p>Including but not limited to these projects designed or supervised by John B. Fischer, Postle &amp; Fischer, Architects &amp; Engineers, and Shepley, 
      Rutan &amp; Coolidge, Architects: Borland Office Building (Chicago, Ill.), City National Bank (Evansville, Ind.), 
      Garfield School Building (Elgin, Ill.), Gymnasium (University of Chicago, Chicago, Ill.), Holt Manufacturing Company Building (Peoria, Ill.), 
      Hotel (Casper, Wyo.), Lake Shore Country Club (Chicago, Ill.?), Roseland Subway Station (Chicago, Ill.), 
      Sefton Manufacturing Company Building (Chicago, Ill.), Store and Office Building (Kankakee, Ill.), Western Shade Cloth 
      Company (Chicago, Ill.), and YWCA Building (Elgin, Ill.). 47 photographs total.</p></scopecontent>		   
  </c02>
  
  <c02 level="item">
    <did> 
      <container type="box-folder">1.3</container> 
      <unittitle>Photo album 3.</unittitle> 
      <unitdate>n.d.</unitdate> 
      <physdesc><extent unit="items">1</extent><genreform>Album</genreform>.</physdesc>			 
    </did>
    <scopecontent><p>Including but not limited to these projects designed or supervised by John B. Fischer, Postle &amp; Fischer, 
      Architects &amp; Engineers, and Shepley, Rutan &amp; Coolidge, Architects: City National Bank (Evansville, Ind.), 
      Healy Specialty Shop (Detroit, Mich.), Holt Manufacturing Company Machine Shop (Peoria, Ill.), Samual Bingham Company Printing 
      Press Plant, and Sefton Maunfacturing Company Machine Shop (Chicago, Ill.) 31 photographs total.</p></scopecontent>		   
  </c02>
  
  <c02 level="item">
    <did> 
      <container type="box-folder">1.4</container> 
      <unittitle>Photo Album 4.</unittitle> 
      <unitdate>n.d.</unitdate> 
      <physdesc><extent unit="items">1</extent><genreform>Album</genreform>.</physdesc>			 
    </did>
    <scopecontent><p>Including but not limited to these projects designed or supervised by John B. Fischer, Postle &amp; Fischer, 
      Architects &amp; Engineers, and Shepley, Rutan &amp; Coolidge, Architects: Apache Hotel (Phoenix, Ariz.), 
      Auburn Park Hospital (Chicago, Ill.), Garfield School Building (Elgin, Ill.), Holt Manufacturing Company (Peoria, Ill.), 
      Law School (University of Chicago, Chicago, Ill.), National Warehouse (Peoria, Ill.), Western Shade Cloth Company (Chicago, Ill.), 
      and William Rainey Harper Memorial Library (University of Chicago, Chicago, Ill.). 18 photographs total.</p></scopecontent>		   
  </c02>
  
  <c02 level="item">
    <did> 
      <container type="portfolio">1</container> 
      <unittitle>Portfolio.</unittitle> 
      <unitdate>n.d.</unitdate> 
      <physdesc><extent unit="items">1</extent><genreform>Portfolio</genreform>.</physdesc>			 
    </did>
    <scopecontent><p>Larger photographs and one watercolor of many of the same images from Photo Albums 1-4. 
      Includes: Apache Hotel (Phoenix, Ariz.), Borland Office Building (Chicago, Ill.), Gymnasium (University of Chicago, Chicago, Ill.), 
      Law School (Unviersity of Chicago, Chicago, Ill.), Western Shade Cloth Compmany (Chicago, Ill.), and William Rainey Harper 
      Memorial Library (University of Chicago, Chicago, Ill.). 30 photographs, 1 watercolor total.</p></scopecontent>		   
  </c02>
  
  <c02 level="item">
    <did> 
      <container type="box-folder">1.4</container> 
      <unittitle>Unidentified office views (possibly Postle &amp; Fischer).</unittitle> 
      <unitdate>n.d.</unitdate> 
      <physdesc><extent unit="items">4</extent><genreform>Photocopies</genreform>.</physdesc>			 
    </did>  
  </c02>
</c01>
	   
<c01 level="series">
  <did>
    <unittitle>SERIES II: MANUSCRIPTS</unittitle>
  </did>
  <c02 level="item">
    <did> 
      <container type="box-folder">1.5</container> 
      <unittitle>"An Appeal for a League of Nations by John B. Fischer".</unittitle> 
	    <unitdate>n.d.</unitdate> 
	    <physdesc><extent unit="items">1</extent><genreform>Manuscript</genreform>.</physdesc>			 
    </did>
    <scopecontent><p>Fischer's draft of a speech aimed at encouraging an unnamed court to vote to create a "League of Nations."</p></scopecontent>		   
	 </c02>
</c01>
	    
<c01 level="series">
  <did>
    <unittitle>SERIES III: CORRESPONDENCE</unittitle>
  </did>
  <c02 level="item">
	<did> 
	  <container type="box-folder">1.6</container> 
	  <unittitle>A.L. van den Berghen to John B. Fischer.</unittitle> 
	  <unitdate>09/06/1909</unitdate> 
	  <physdesc><extent unit="items">1</extent><genreform>ALS</genreform>.</physdesc>			 
	</did>
    <scopecontent><p>Letter of admiration regarding Fischer's book on missions.</p></scopecontent>		  
    <acqinfo><p>Found in a copy of Frank Lloyd Wright's book "Dana House", previously owned by John B. Fischer, in September 2008. The book is now in the possession of the Ryerson &amp; Burnham Libraries.</p></acqinfo>
  </c02>
  
  <c02 level="item">
    <did> 
      <container type="box-folder">1.6</container> 
      <unittitle>Marion Mahony Griffin to John B. Fischer.</unittitle> 
      <unitdate>n.d.</unitdate> 
      <physdesc><extent unit="items">1</extent><genreform>Photocopies</genreform>.</physdesc>			 
    </did>
    <scopecontent><p>New Year's card.</p></scopecontent>		   
  </c02>
</c01>
	    
</dsc> 
</archdesc>  
</ead>
