from portal_admin.models import DigObject, ImportedValue, FacetValue, DigObjectFacetValue, ValueElement

def load_digobj_fvalues():
    fvalues = {
        'ica000004': ['Arts', 'Photography', 'Chicago',
            #'C&#233;zanne, Paul',
            'Landseer, Edwin Henry, Sir',
            'Bonheur, Rosa',
            'Larsson, Carl',
            'Chagall, Marc',
            'Nicholson, William, Sir',
            'Magrath, William',
            'De Longpre, Paule',
            'Munkacsy, Mihaly',
            'Jan-Monchablon',
            'Leloir, Maurice',
            'Earle, Lawrence',
            'Sargent, John',
            'Aoki, T.',
            'Tissot, James',
            'Evans, de Scott',
            'Vibert, Jehan',
            'Jacques, Charles-Emile',
            'Chevilliard, Vincent-Jean-Baptiste',
            'Klempke, Annae',
            'Delacroix, Eugene',
            'Stuart, Gilbert',
            'O\'Keeffe, Georgia',
            'Clark, Allan',
            'Roberts, David',
            'Weber, Max',
            'Caldecott, Randolph',
            'Lorenc, Zdeneke',
            #'Me&#353;trovi&#263;, Ivan',
            ],
        'ica000001': ['Graham, Burnham, and Company', 'D.H. Burnham and Company',
            'Architecture', 'Chicago', 'Photography', 'Continental and Commercial National Bank'],
        'ica000007': ['Macmonnies, Mary', 'Cassatt, Mary', 'French, William',
            'Elliott, John', 'Hallowell, Sara', 'Chicago', 'Fairs and Conventions'],
        'ica000002': ['Architecture', 'Chicago', 'Mundie and Jensen Architects', 'Schillo Motor Sales Company'],
        'ica000003': ['Chicago', 'Architecture', 'Journalism', 'Chicago Tribune', 'Hood, Raymond', 'Howells, John'],
        'ica000012': ['Chicago', 'Architecture', 'Fairs and Conventions'],
        'ica000011': ['Chicago', 'Architecture'],
        'ica000008': ['Chicago', 'Photography', 'South Side Community Art Center'],
        'ica000010': ['Arts', 'Faggi, Alfeo', 'Chicago'],
    }

    for k in fvalues.keys():
        element, created = ValueElement.objects.get_or_create(element=k)
        imported_value = ImportedValue.objects.get(value=element)
        digobj = imported_value.digobject
        for fvalue in fvalues[k]:
            facet_value = FacetValue.objects.get(value=fvalue)
            DigObjectFacetValue.objects.create(digobject=digobj, facet_value=facet_value)


