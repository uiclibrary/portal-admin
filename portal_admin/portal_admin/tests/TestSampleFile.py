import os

from django.test import TestCase
from django.conf import settings

from portal_admin import validation

class TestSampleFile(TestCase):
    sample_file = os.path.join(settings.BASE_DIR, 'portal_admin/tests/test_data/uchicago/processor/apf3-00010.xml')

    def validate_xpath(self, filename, xpath, instance, delimiter, result):
        self.assertEqual(result, validation.validate_xpath(filename, None, xpath, instance, delimiter))

    def test_valid_xpath(self):
        self.validate_xpath(self.sample_file, "img/orig-titleName", 0, None, ["World's Columbian Exposition"])

    def test_invalid_xpath(self):
        self.assertRaises(validation.ValidationError, validation.validate_xpath, self.sample_file, None, "img/orig-tileName", 0, None)

    def test_default(self):
        default = "This is the default value"
        self.assertEqual([default], validation.validate_with_sample_file("fake_file.xml", {'use_xpath': "False", 'default': default}))

    def test_use_filename(self):
        self.assertEqual("Using filename", validation.validate_with_sample_file("fake_file.xml", {'use_xpath': "False"}))
        
