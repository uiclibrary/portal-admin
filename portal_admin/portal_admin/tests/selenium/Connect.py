import time
from selenium import webdriver

from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.test import TestCase

from portal_admin.views.connect import ConnectConfirmView
from portal_admin.models import Connection, DigObject, DigObjectForm, Profile

from .SeleniumTest import SeleniumTest

from django.db.models import Q

class ConnectUnit(TestCase):
    # Connect also has a substantial set of selenium tasks.
    fixtures = [
        'initial_data.json',
        'test/test_medium.json'] # comprehensive fixture

    def setUp(self):
        self.dig_objects = DigObject.objects.all()
        self.connections = Connection.objects.all()

        self.objs_can_be_child = DigObject.objects.filter(digobject_form__can_be_child=True)
        self.objs_can_be_parent = DigObject.objects.filter(digobject_form__can_be_parent=True)
        self.objs_cannot_be_parent = DigObject.objects.filter(digobject_form__can_be_parent=False)
        self.objs_cannot_be_child = DigObject.objects.filter(digobject_form__can_be_child=False) # length = 0
        self.objs_can_be_parent_or_child = DigObject.objects.filter(digobject_form__can_be_child=True,
                                                                    digobject_form__can_be_parent=True)

        self.assertEqual(self.objs_can_be_child.count(), 34)
        self.assertEqual(self.objs_can_be_parent.count(), 24)
        self.assertEqual(self.objs_cannot_be_parent.count(), 10)
        self.assertEqual(self.objs_cannot_be_child.count(), 0)

        self.formats_can_be_child = DigObjectForm.objects.filter(can_be_child=True)
        self.formats_can_be_parent = DigObjectForm.objects.filter(can_be_parent=True)
        self.formats_cannot_be_child = DigObjectForm.objects.filter(can_be_child=False)
        self.formats_cannot_be_parent = DigObjectForm.objects.filter(can_be_parent=False)

        self.assertEqual(self.formats_can_be_child.count(), 7)
        self.assertEqual(self.formats_can_be_parent.count(), 4)
        self.assertEqual(self.formats_cannot_be_child.count(), 0)
        self.assertEqual(self.formats_cannot_be_parent.count(), 3)


    def tearDown(self):
        pass

    def test_validate_parent(self):
        can_be_parent = self.objs_can_be_parent.first()
        can_be_child = self.objs_can_be_child.first()
        cannot_be_parent = self.objs_cannot_be_parent.first()

        self.assertFalse(Connection.objects.all()) # no connections
        with self.assertRaises(ValidationError):
            cannot_be_parent.add_child(can_be_child)
        self.assertFalse(Connection.objects.all()) # still no connections
        can_be_parent.add_child(can_be_child)
        self.assertEqual(Connection.objects.all().count(), 1) # finally

    def test_can_be_parent(self):
        '''Records should be able to connect to other records as parent.'''
        self.assertFalse(self.connections)

        can_be_parent_1 = self.objs_can_be_parent[0]
        can_be_child_1 = self.objs_can_be_child[1]

        can_be_parent_2 = self.objs_can_be_parent[2]
        can_be_child_2 = self.objs_can_be_child[3]

        can_be_parent_1.add_child(can_be_child_1) # should make connection
        self.connections = Connection.objects.all()
        self.assertTrue(self.connections)
        c = Connection.objects.get(pk=1)
        self.assertEqual(c.pk, 1)
        self.assertEqual(c.parent, can_be_parent_1)
        self.assertEqual(c.child, can_be_child_1)
        self.assertEqual(Connection.objects.count(), 1)

        can_be_parent_2.add_child(can_be_child_2)
        self.assertEqual(Connection.objects.count(), 2)

    def test_cannot_be_parent(self):
        '''Records with "Un-childable" formats should not be able to add children.  Others should be OK.'''
        self.assertEqual(Connection.objects.count(), 0)
        cannot_be_parent = self.objs_cannot_be_parent.first()
        can_be_child = self.objs_can_be_child.first()

        with self.assertRaises(ValidationError):
            cannot_be_parent.add_child(can_be_child)
        with self.assertRaises(ValidationError):
            can_be_child.add_parent(cannot_be_parent)
        self.assertEqual(Connection.objects.count(), 0)

    def test_can_be_child(self):
        '''Records should be able to connect to other records as child.'''
        can_be_parent = self.objs_can_be_parent.first()
        can_be_child = self.objs_can_be_child.first()

        self.assertFalse(Connection.objects.count())
        can_be_child.add_parent(can_be_parent) # should make connection
        self.assertTrue(Connection.objects.count())
        self.assertEqual(Connection.objects.count(), 1)
        c = Connection.objects.get(pk=1)
        self.assertEqual(c.pk, 1)
        self.assertEqual(c.child, can_be_child)
        self.assertEqual(c.parent, can_be_parent)

    def test_cannot_be_child(self):
        '''Records with "Un-parentable" formats should not be able to add parents.  Others should be OK.'''
        can_be_parent = self.objs_can_be_parent.first()
        # Currently, non-parentable formats have not been implemented, se we have to fake one.
        format_cannot_be_child = DigObjectForm.objects.create(
            pk=9876543210,
            dcxml_value = 'Archival Collections',
            can_be_parent = True,
            can_be_child = False)
        profile = Profile.objects.first()
        cannot_be_child = DigObject.objects.create(
            pk=1234567890,
            ark="abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHI",
            title="Test DigObject",
            profile=profile,
            digobject_form = format_cannot_be_child
            )

        self.assertEqual(Connection.objects.count(), 0)
        with self.assertRaises(ValidationError):
            can_be_parent.add_child(cannot_be_child)
        with self.assertRaises(ValidationError):
            cannot_be_child.add_parent(can_be_parent)
        self.assertEqual(Connection.objects.count(), 0)

    def test_can_be_either_parent_or_child(self):
        '''Some records can be either a parent or a child (but not both).'''
        self.assertGreater(len(self.objs_can_be_parent_or_child), 1)

    def test_parent_connections_must_be_unique(self):
        '''Parents cannot be made twice.'''
        obj1 = self.objs_can_be_parent_or_child.first()
        obj2 = self.objs_can_be_parent_or_child.last()

        obj1.add_parent(obj2)
        with self.assertRaises(ValidationError):
            obj1.add_parent(obj2)

    def test_cannot_be_parent_to_itself(self):
        '''A record can't be parent to itself.'''
        obj1 = self.objs_can_be_parent_or_child.first()
        with self.assertRaises(ValidationError):
            obj1.add_parent(obj1)

    def test_cannot_be_child_to_itself(self):
        '''A record can't be parent to itself.'''
        obj1 = self.objs_can_be_parent_or_child.first()
        with self.assertRaises(ValidationError):
            obj1.add_child(obj1)

    def test_cannot_form_multiple_connections(self):
        '''A record cannot form more than a single connection with another record.'''

        obj1 = self.objs_can_be_parent_or_child.first()
        obj2 = self.objs_can_be_parent_or_child.last()

        obj1.add_parent(obj2)

        with self.assertRaises(ValidationError):
            obj1.add_parent(obj2)
        with self.assertRaises(ValidationError):
            obj2.add_child(obj1)
        with self.assertRaises(ValidationError):
            obj2.add_parent(obj1)
        with self.assertRaises(ValidationError):
            obj1.add_child(obj2)

    def test_delete_parent_connection(self):
        '''A record with a parent should be able to delete the connection to its parent.'''
        child = self.objs_can_be_child.first()
        can_be_parent = self.objs_can_be_parent.first()

        self.assertEqual(Connection.objects.count(), 0)
        child.add_parent(can_be_parent)
        self.assertEqual(Connection.objects.count(), 1)
        parents = child.get_parents()
        parent = parents.first()
        self.assertEqual(parent, can_be_parent)
        connection = Connection.objects.get(parent=parent, child=child)
        self.assertTrue(connection)

        child.abandon_parent(parent)

        # check objects still exist; check connection does not
        self.assertTrue(child)
        self.assertTrue(parent)
        with self.assertRaises(ObjectDoesNotExist):
            connection = Connection.objects.get(parent=parent, child=child)

    def test_delete_child_connection(self):
        '''A record with a child should be able to delete the connection to its child.'''
        parent = self.objs_can_be_parent.first()
        can_be_child = self.objs_can_be_child.first()

        self.assertEqual(Connection.objects.count(), 0)
        parent.add_child(can_be_child)
        self.assertEqual(Connection.objects.count(), 1)
        children = parent.get_children()
        child = children.first()
        self.assertEqual(child, can_be_child)
        connection = Connection.objects.get(parent=parent, child=child)
        self.assertTrue(connection)

        parent.abandon_child(child)

        # check objects still exist; check connection does not
        self.assertTrue(parent)
        self.assertTrue(child)
        with self.assertRaises(ObjectDoesNotExist):
            connection = Connection.objects.get(parent=parent, child=child)

    def check_connected(self, obj1, obj2):
        return Connection.objects.filter(Q(parent=obj1, child=obj2) |
                                         Q(parent=obj2, child=obj1)).exists()

    def test_connect(self):
        obj1 = DigObject.objects.get(pk=1)
        obj2 = DigObject.objects.get(pk=2)
        obj3 = DigObject.objects.get(pk=3)
        obj4 = DigObject.objects.get(pk=4)
        obj5 = DigObject.objects.get(pk=5)
        obj6 = DigObject.objects.get(pk=6)
        obj7 = DigObject.objects.get(pk=7)
        connect_1_2 = Connection(parent=obj1, child=obj2)
        connect_1_3 = Connection(parent=obj1, child=obj3)
        connect_3_4 = Connection(parent=obj3, child=obj4)
        # the following adds a second parent to obj3: controversial
        connect_3_5 = Connection(parent=obj5, child=obj3)
        connect_5_6 = Connection(parent=obj5, child=obj6)
        connect_1_2.save()
        connect_1_3.save()
        connect_3_4.save()
        connect_3_5.save()
        connect_5_6.save()

        self.assertTrue(self.check_connected(obj1, obj2))
        self.assertTrue(self.check_connected(obj2, obj1))
        self.assertTrue(self.check_connected(obj3, obj4))
        self.assertTrue(self.check_connected(obj4, obj3))
        self.assertTrue(self.check_connected(obj5, obj6))
        self.assertTrue(self.check_connected(obj6, obj5))
        self.assertFalse(self.check_connected(obj2, obj3))
        self.assertFalse(self.check_connected(obj1, obj4))
        self.assertFalse(self.check_connected(obj4, obj1))
        self.assertFalse(self.check_connected(obj4, obj2))
        self.assertFalse(self.check_connected(obj2, obj4))
        self.assertFalse(self.check_connected(obj1, obj7))
        self.assertFalse(self.check_connected(obj7, obj1))
        self.assertFalse(self.check_connected(obj4, obj6))
        self.assertFalse(self.check_connected(obj1, obj1))
        self.assertFalse(self.check_connected(obj7, obj7))

        self.assertEqual(list(obj2.get_parents()), [obj1])
        self.assertEqual(list(obj3.get_parents()), [obj1, obj5])
        self.assertEqual(list(obj6.get_parents()), [obj5])
        self.assertEqual(list(obj4.get_parents()), [obj3])
        self.assertEqual(list(obj1.get_parents()), [])
        self.assertNotEqual(list(obj1.get_parents()), [obj2])
        self.assertNotEqual(list(obj2.get_parents()), [obj3])
        self.assertNotEqual(list(obj3.get_parents()), [obj1])

        self.assertEqual(list(obj2.get_children()), [])
        self.assertEqual(list(obj3.get_children()), [obj4])
        self.assertEqual(list(obj1.get_children()), [obj2, obj3])
        self.assertNotEqual(list(obj1.get_children()), [obj4])
        self.assertNotEqual(list(obj3.get_children()), [obj1])


class ConnectSelenium(SeleniumTest):
    fixtures = ['initial_data.json',
                'test/test_ccc.json',
                'test/test_medium.json',
                'test/test_users.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.login("kate", "admin")
        self.select_context("Chicago Collections Consortium",
                            "University of Illinois at Chicago")
        self.browser.get(self.live_server_url + reverse('connect'))

    def tearDown(self):
        self.browser.quit()

    def test_only_eligible_records(self):
        '''When connecting records to other records, only those records which are eligible for connections appear in the interface.'''
        child_only = ["Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)"]
        parents = ["Charles G. Dawes papers: An inventory of the collection at the University of Illinois at Chicago",
                   "Russell Ward Ballard collection: An inventory of the collection at the University of Illinois at Chicago"]
        self.verify_body(parents, child_only)

    def test_self_not_in_eligible_lists(self):
        '''TODO: When connecting record A to other records, record A should not be a choice in the interface.'''
        pass

    def test_self_not_in_eligible_lists(self):
        '''TODO: When connecting record A to other records, record A should not be a choice in the interface.'''
        pass

    def test_connect_records(self):
        '''Connecting many records at once should be possible.  This test makes sure it's possible through the back end.'''
        objs = DigObject.objects.filter(profile__institution__xtf_name="uic")
        self.assertEqual(objs.count(), 34)

        connections = Connection.objects.all()
        self.assertEqual(connections.count(), 0)

        parent = objs.get(title="Charles G. Dawes papers: An inventory of the collection at the University of Illinois at Chicago")
        children = objs.filter(profile=1)[:10]

        for child in children:
            parent.add_child(child)
        self.assertEqual(Connection.objects.all().count(), 10)

    def test_connect_many_records(self):
        '''Connecting many records at once should be possible.  This test makes sure it's possible through the front end.'''

        connections = Connection.objects.all()
        self.assertEqual(connections.count(), 0)

        parent = self.browser.find_element_by_id("id_items_0")
        parent.click()


        btn = self.browser.find_element_by_id("id_action_button_select_parents")
        btn.click()
        self.verify_body(["Parent: Charles G. Dawes papers: An inventory of the collection at the University of Illinois at Chicago",
                          "Charles MacMurray Design collection: An inventory of the collection at the University of Illinois at Chicago",
                          "Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)",
                          "Polish Falcons of America collection: An inventory of its records at the University of Illinois at Chicago"],
                         ["Russell Ward Ballard collection: An inventory of the collection at the University of Illinois at Chicago",
                          "Traffic Intersection at Stockton Drive and LaSalle Street (image 06)"])

        select_all_pages = self.browser.find_element_by_id("select_all_pages_checkbox")
        select_all_pages.click()

        self.verify_body(["34 of 34 records selected"])

        action_button = self.browser.find_element_by_id("id_action_button_select_children")
        action_button.click()

        self.verify_body(["Charles MacMurray Design collection: An inventory of the collection at the University of Illinois at Chicago",
                          "Construction/Drives and Road at Jeffery Blvd and 74th St (image 02)",
                          "Polish Falcons of America collection: An inventory of its records at the University of Illinois at Chicago",
                          "Russell Ward Ballard collection: An inventory of the collection at the University of Illinois at Chicago",
                          "Traffic Intersection at Stockton Drive and LaSalle Street (image 06)"])

        connections = Connection.objects.all()
        self.assertEqual(connections.count(), 33)

