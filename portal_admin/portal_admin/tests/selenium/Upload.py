import os

from selenium import webdriver

from django.core.urlresolvers import reverse
from django.conf import settings

from .SeleniumTest import SeleniumTest

class Upload(SeleniumTest):
    fixtures = ['initial_data.json',
    'test/test_ccc.json',
    'test/test_users.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.login("kate", "admin")

    def tearDown(self):
        self.browser.quit()

    def test_aic_deposit(self):
        self.select_context("Chicago Collections Consortium",
            "Art Institute of Chicago")
        self.browser.get(self.live_server_url + reverse('deposit'))
        upload = os.path.join(settings.BASE_DIR,
            'portal_admin/tests/test_data/aic/ead_ica/aic.zip')
        #self.click_link("Archival Collections")
        self.click_link("AIC EAD Finding Aids")
        self.click("next")
        self.set_field('id_2-upload', upload)
        self.click("next")
        self.verify_body(["Upload complete"])

    def test_aic_missing_element(self):
        self.select_context("Chicago Collections Consortium",
            "Art Institute of Chicago")
        self.browser.get(self.live_server_url + reverse('deposit'))
        upload = os.path.join(settings.BASE_DIR,
            'portal_admin/tests/test_data/aic/ead_ica/aic_missing_element.zip')
        #self.click_link("Archival Collections")
        self.click_link("AIC EAD Finding Aids")
        self.click("next")
        self.set_field('id_2-upload', upload)
        self.click("next")
        self.verify_body(["Upload complete"])

    def test_uic_deposit(self):
        self.select_context("Chicago Collections Consortium", "University of Illinois at Chicago")
        self.browser.get(self.live_server_url + reverse('deposit'))
        upload = os.path.join(settings.BASE_DIR,
            'portal_admin/tests/test_data/ccc_idot/uic_file_not_found.zip')
        self.click_link("Digital Images")
        self.click_link("IDOT content dm images")
        self.click("next")
        self.select("id_1-location",
            "Daley Library Special Collections and University Archives")
        self.click("next")
        self.set_field('id_2-upload', upload)
        self.click("next")
        self.verify_body(["Upload complete"])
