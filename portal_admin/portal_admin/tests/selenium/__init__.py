from .CreateRules import *
#from .Upload import * # turned off: Issue #253
from .MultiPage import *
from .Login import *
from .BrowseRecords import *
from .BrowseTags import *
from .ObjectRecord import *
from .Connect import *
from .SeleniumTest import *
from .Contexts import *
