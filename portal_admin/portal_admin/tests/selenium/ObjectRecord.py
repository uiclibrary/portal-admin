from selenium import webdriver
from django.core.urlresolvers import reverse
from .SeleniumTest import SeleniumTest
from portal_admin.models import DigObject

class ObjectRecord(SeleniumTest):
    fixtures = ['initial_data.json', 'test/test_ccc.json', 'test/test_users.json', 'test/test_digobjects.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        obj = DigObject.objects.get(title="Traffic Intersection at South Parkway and 51st Street (image 28)")
        self.browser.get(self.live_server_url + reverse("record", kwargs={'pk': obj.pk})) #/records/1/
        self.set_field("id_username", "kate")
        self.set_field("id_password", "admin")
        self.click("login")
        self.select_context("Chicago Collections Consortium", "University of Illinois at Chicago")

    def tearDown(self):
        self.browser.quit()

    def test_metadata(self):
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "Digital Images: XML/using a single XML file",
            "Deposited",
            "IDOT_2v_176_383",
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "1936-01-13",
            "Chicago Park District Traffic Section",
            "Traffic Intersections",
            "South Parkway",
            "41.80249",
            "-87.61665",
            "Illinois Department of Transportation Chicago Traffic photographs"])

    def test_history(self):
        self.click_link("View History")
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "Digital Images: XML/using a single XML file",
            "Deposited by on Sept. 16, 2014, 3:14 p.m."])

    def test_actions(self):
        self.click_link("Take Actions")
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "Digital Images: XML/using a single XML file",
            "Add tags"])

    # This test will only pass if you are running celery at the same time.
    def test_delete(self):
        self.click_link("Take Actions")
        self.click_link("Delete record")
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "This record will be deleted completely from the system including all related files."])
        self.click("id_delete")
        self.verify_body([
            "The following record has been marked for deletion and will be deleted completely from the system including all related metadata and media files.",
            "Traffic Intersection at South Parkway and 51st Street (image 28)"])

    def test_add_tags(self):
        self.click_link("Take Actions")
        self.click_link("Add tags")
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "Neighborhoods",
            "Albany Park"])
        self.click("id_form-0-values_0")
        self.click_link("Cities")
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "Cities",
            "Chicago"])
        self.click("id_form-1-values_0")
        self.click("id_submit")
        self.verify_body([
            "Traffic Intersection at South Parkway and 51st Street (image 28)",
            "Digital Images: XML/using a single XML file",
            "Deposited",
            "Albany Park",
            "Chicago"])

    def test_change_title(self):
        self.verify_source([
            "<h2>Traffic Intersection at South Parkway and 51st Street (image 28)</h2>",])
        self.set_field('title_change_field', 'This is new title')
        self.click("title_change_submit")
        self.verify_source(
            ['This is new title',],
            ["<h2>Traffic Intersection at South Parkway and 51st Street (image 28)</h2>",])
        self.click("title_change_reset")
        self.verify_source(
            ["<h2>Traffic Intersection at South Parkway and 51st Street (image 28)</h2>",],
            ['This is new title',])

    def test_remove_tags(self):
        self.verify_source_by_element([
            'Type', 'Status', 'Tags', 'Chicago', 'Transportation'],
            [],
            '#id_above_fold_metadata_list')
        tag1 = self.browser.find_element_by_css_selector(
            '#id_above_fold_metadata_list .digobject_tag_list .digobject_tag .digobject_tag_remove')
        tag1.click()
        tag2 = self.browser.find_element_by_css_selector(
            '#id_above_fold_metadata_list .digobject_tag_list .digobject_tag .digobject_tag_remove')
        tag2.click()
        self.verify_source_by_element(
            ['Type', 'Status', 'Tags', 'None'], ['Chicago', 'Transportation'],
            '#id_above_fold_metadata_list')
