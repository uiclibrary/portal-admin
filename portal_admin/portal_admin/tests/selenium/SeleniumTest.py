import os

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

from django.core.urlresolvers import reverse
from django.conf import settings

from django.test import LiveServerTestCase

class SeleniumTest(LiveServerTestCase):
    def select_context(self, context, institution):
        self.select("id_context", context)
        self.select("id_institution", institution)
        self.click("login")

    def set_field(self, _id, value):
        name_field = self.browser.find_element_by_id(_id)
        name_field.send_keys(value)

    def click(self, _id):
        clickee = self.browser.find_element_by_id(_id)
        clickee.click()

    def click_css(self, selector):
        clickee = self.browser.find_element_by_css_selector(selector)
        clickee.click()

    def select(self, _id, text):
        s = Select(self.browser.find_element_by_id(_id))
        s.select_by_visible_text(text)

    def select_css(self, selector, text):
        s = Select(self.browser.find_element_by_css_selector(selector))
        s.select_by_visible_text(text)

    def click_link(self, text):
        self.browser.find_element_by_link_text(text).click()

    def verify_body(self, text, not_in=[]):
        body = self.browser.find_element_by_tag_name('body')
        for t in text:
            self.assertIn(t, body.text)
        for n in not_in:
            self.assertNotIn(n, body.text)

    def verify_source(self, text, not_in=[]):
        source = self.browser.page_source
        for t in text:
            self.assertIn(t, source)
        for n in not_in:
            self.assertNotIn(n, source)

    def verify_source_by_element(self, text, not_in=[], element='body'):
        element_source = self.browser.find_element_by_css_selector(element).get_attribute('innerHTML')
        for t in text:
            self.assertIn(t, element_source)
        for n in not_in:
            self.assertNotIn(n, element_source)


    def login(self, username, password):
        self.browser.get(self.live_server_url + reverse("login"))
        self.set_field("id_username", username)
        self.set_field("id_password", password)
        self.click("login")

