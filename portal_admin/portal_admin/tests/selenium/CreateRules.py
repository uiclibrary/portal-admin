import os
from selenium import webdriver
from .SeleniumTest import SeleniumTest

from django.core.urlresolvers import reverse

from django.conf import settings

from portal_admin.models import DigObjectForm, ElementSource, FileSource, Profile

class CreateRules(SeleniumTest):
    fixtures = ['initial_data.json', 'test/test_ccc.json', 'test/test_users.json']

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.login("kate", "admin")
        self.select_context("Chicago Collections Consortium", "University of Chicago")

    def tearDown(self):
        self.browser.quit()

    def sample_file_test(self, fsindex, findex, c, msg):
        test_button = self.browser.find_element_by_id('id_5-{}-{}-test'.format(fsindex, findex))
        test_button.click()
        alert_msg = self.browser.find_element_by_id('id_5-{}-{}-alert'.format(fsindex, findex))
        self.assertEqual("alert alert-{}".format(c), alert_msg.get_attribute("class"))
        self.assertEqual(msg, alert_msg.text)

    def test_create_profile_multixml(self):
        sample_file = os.path.join(settings.BASE_DIR, 'portal_admin/tests/test_data/uchicago/processor/apf3-00010.xml')
        dof = DigObjectForm.objects.get(manifest_format="separate XML files")
        self.browser.get(self.live_server_url + reverse('create_profile', kwargs={'pk': dof.id}))

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 1 of 6: Prepare"])

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 2 of 6: Name & Describe"])

        self.set_field('id_1-name', 'Test Profile')
        self.set_field('id_1-description', 'This is the description of the test profile.')

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 3 of 6: Sample File Submission"])

        self.set_field('id_2-sample_file', sample_file)

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 4 of 6: Image Submission"])

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 5 of 6: Metadata Information"])

        self.click("id_4-use_filename_1")
        self.click("id_4-2-test")

        self.verify_body(["Using filename"])

        self.click("id_4-use_filename_0")
        self.set_field('id_4-image_filename', 'img/orig-identifierScanNumber')
        self.click("id_4-2-test")

        self.verify_body(["apf3-00010"])

        self.set_field('id_4-image_suffix', '-img')

        self.select('id_4-has_extension', 'No')

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 6 of 6: Define Import Fields"])

        self.set_field('id_5-0-0-xpath', 'title')

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Confirmation - Rules Created",
                          "Test Profile",
                          "Archival Photographic Files"])

        profile = Profile.objects.get(name="Test Profile")
        self.assertEqual("This is the description of the test profile.", profile.description)

        fs = FileSource.objects.filter(profile=profile)
        self.assertEqual(fs.count(), 1)
        self.assertEqual(fs[0].xpath, "img/orig-identifierScanNumber")

        es = ElementSource.objects.filter(profile=profile)
        self.assertEqual(es.count(), 1)
        self.assertEqual(es[0].xpath, "title")

    def test_create_profile_singlexml(self):
        sample_file = os.path.join(settings.BASE_DIR,
                                   'portal_admin/tests/test_data/ccc_idot/local_single_xml/ccc_uic_idot_test.xml')
        dof = DigObjectForm.objects.get(manifest_format="using a single XML file")
        self.browser.get(self.live_server_url + reverse('create_profile', kwargs={'pk': dof.id}))

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 1 of 6: Prepare"])
        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 2 of 6: Name & Describe"])

        self.set_field('id_1-name', 'Test Profile')
        self.set_field('id_1-description',
            'This is the description of the test profile.')

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 3 of 6: Sample File Submission"])

        self.set_field('id_2-sample_file', sample_file)

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 4 of 6: Image Submission"])

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 5 of 6: Metadata Information"])

        self.set_field('id_4-record_separator', 'recor')
        self.click("id_4-1-test")
        self.verify_body(["Separator not found"])

        self.set_field('id_4-record_separator', 'd')
        self.click("id_4-1-test")
        self.verify_body(["File contains 10 records"])

        self.set_field('id_4-image_filename', 'record/Identifier')
        self.click("id_4-2-test")
        self.verify_body(["IDOT_2v_176_383"])

        self.select('id_4-has_extension', 'No')

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Step 6 of 6: Define Import Fields"])

        # title
        self.set_field('id_5-0-0-xpath', 'record/Title')
        self.sample_file_test(0, 0, "success", "Traffic Intersection at South Parkway and 51st Street (image 28)")

        # identifier
        self.set_field('id_5-1-0-xpath', 'record/Identifier')
        self.sample_file_test(1, 0, "success", "IDOT_2v_176_383")

        # digital collection name
        self.set_field('id_5-9-0-xpath', 'record/Source')
        self.sample_file_test(9, 0, "success", "Illinois Department of Transportation Chicago Traffic photographs")

        # date
        self.set_field('id_5-2-0-xpath', 'record/ISODate')

        self.sample_file_test(2, 0, "success", "1936-01-13")

        # names
        self.set_field('id_5-5-0-xpath', 'record/Creator')
        self.select('id_5-5-0-has_delimiter', "No")
        self.sample_file_test(5, 0, "success", "Chicago Park District Traffic Section")

        # subjects
        self.set_field('id_5-6-0-xpath', 'record/Subject')
        self.set_field('id_5-6-0-delimiter', ';')
        self.sample_file_test(6, 0, "success", "Traffic Intersections")

        # geographic coverage
        self.set_field('id_5-7-0-xpath', 'record/GeographicLocation')
        self.set_field('id_5-7-0-delimiter', ';')
        self.sample_file_test(7, 0, "success", "2 values found\nSouth Parkway\n51st Street")

        self.click('id_5-7-add-another')
        self.set_field('id_5-7-1-xpath', 'record/Latitude')
        self.select('id_5-7-1-has_delimiter', "No")
        self.sample_file_test(7, 1, "success", "41.80249")

        self.click('id_5-7-add-another')
        self.set_field('id_5-7-2-xpath', 'record/Longitude')
        self.select('id_5-7-2-has_delimiter', "No")
        self.sample_file_test(7, 2, "success", "-87.61665")

        # rights
        self.set_field('id_5-11-0-xpath', 'record/Rights')
        self.sample_file_test(11, 0, "success", "This image may be used freely, with attribution, for research, study and educational purposes. For permission to publish, distribute, or use this image for any other purpose, please contact Special Collections and University Archives, University of Illinois at Chicago Library, 801 S. Morgan St., Chicago, IL 60607. Phone: (312) 996-2742; email: lib-permissions@uic.edu.")

        self.click("next")

        #pdb.set_trace()

        self.verify_body(["Create Import Mapping Rules: Digital Images",
                          "Confirmation",
                          "Your rules have been saved",
                          "Test Profile"],
                         ["IDOT content dm images"])

        profile = Profile.objects.get(name="Test Profile")
        self.assertEqual(profile.description, "This is the description of the test profile.")
        self.assertEqual(profile.split_node, "record")
        fs = FileSource.objects.filter(profile=profile)
        self.assertEqual(fs.count(), 1)
        self.assertEqual(fs[0].xpath, "record/Identifier")

        es = ElementSource.objects.filter(profile=profile)
        self.assertEqual(es.count(), 10)

    def test_create_profile_ead(self):
        sample_file = os.path.join(settings.BASE_DIR, 'portal_admin/tests/test_data/aic/ead_ica/ica000001.xml')
        dof = DigObjectForm.objects.get(_format="EAD")
        self.browser.get(self.live_server_url + reverse('create_profile', kwargs={'pk': dof.id}))

        self.verify_body(["Create Import Mapping Rules: Archival Collections",
                          "Step 1 of 4: Prepare"])
        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Archival Collections",
                          "Step 2 of 4: Name & Describe"])

        self.set_field('id_1-name', 'Test Profile')
        self.set_field('id_1-description', 'This is the description of the test profile.')

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Archival Collections",
                          "Step 3 of 4: Sample File Submission"])

        self.set_field('id_2-sample_file', sample_file)

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Archival Collections",
                          "Step 4 of 4: Define Import Fields"])

        # identifier
        self.set_field('id_5-4-0-xpath', 'ead/eadheader/eadid')
        self.set_field('id_5-4-0-attribute', 'publicid')
        self.sample_file_test(4, 0, "success", "ica000001")

        # title
        self.set_field('id_5-0-0-xpath', 'ead/eadheader/filedesc/titlestmt/titleproper')
        self.sample_file_test(0, 0, "success", "Continental and Commercial National Bank Photographs Collection")

        # title (date)
        self.set_field('id_5-2-0-xpath', 'ead/eadheader/filedesc/titlestmt/titleproper/date')
        self.set_field('id_5-2-0-attribute', 'type')
        self.set_field('id_5-2-0-attribute_value', 'inclusive')
        self.sample_file_test(2, 0, "success", "c.1914")

        # date
        self.set_field('id_5-3-0-xpath', 'ead/eadheader/filedesc/titlestmt/titleproper/date')
        self.set_field('id_5-3-0-attribute', 'type')
        self.set_field('id_5-3-0-attribute_value', 'inclusive')
        self.sample_file_test(3, 0, "success", "c.1914")

        # description
        self.set_field('id_5-5-0-xpath', 'ead/archdesc/did/abstract')
        self.sample_file_test(5, 0, "success", "Black and white photographs of the Continental and Commercial National Bank in Chicago, Illinois, designed by D.H. Burnham and Company in 1912.")

        # names
        self.set_field('id_5-6-0-xpath', 'ead/archdesc/controlaccess/p/persname')
        self.sample_file_test(6, 0, "danger", "Field not found")

        #subjects
        self.set_field('id_5-7-0-xpath', 'ead/archdesc/controlaccess/p/subject')
        self.sample_file_test(7, 0, "danger", "Field not found")

        self.click("next")

        self.verify_body(["Create Import Mapping Rules: Archival Collections",
                          "Confirmation - Rules Created",
                          "Test Profile"], ["AIC EAD Finding Aids"])

        profile = Profile.objects.get(name="Test Profile")
        self.assertEqual(profile.description, "This is the description of the test profile.")

        fs = FileSource.objects.filter(profile=profile)
        self.assertEqual(fs.count(), 0)

        es = ElementSource.objects.filter(profile=profile)
        self.assertEqual(es.count(), 7)
