from django.test import TestCase
from xml.etree.ElementTree import fromstring

from portal_admin.utils import get_content_pointer
from portal_admin.models import FileSource, Profile

from portal_admin.processor import Record

class Filenames(TestCase):
    
    def get_filename(self, value, has_extension=False, suffix=None):
        record = fromstring("<record><filename>{}</filename></record>".format(value))
        p = Profile(is_remote=False)
        f = FileSource(profile=p,
                       xpath="record/filename",
                       suffix=suffix,
                       has_extension=has_extension)
        return get_content_pointer(Record(record, "my_filename", "my_path"), f)
    
    def test_filename(self):
        self.assertEqual(self.get_filename("my_filename"), "my_filename")

    def test_filename_with_dots(self):
        self.assertEqual(self.get_filename("my.filename"), "my.filename")

    def test_filename_with_extension(self):
        self.assertEqual(self.get_filename("my_filename.ext", True), "my_filename")

    def test_filename_with_dots_extension(self):
        self.assertEqual(self.get_filename("my.filename.ext", True), "my.filename")

    def test_filename_with_suffix(self):
        self.assertEqual(self.get_filename("my_filename", False, "-suf"), "my_filename-suf")

    def test_filename_with_suffix_extension(self):
        self.assertEqual(self.get_filename("my_filename.ext", True, "-suf"), "my_filename-suf")

    def test_filename_with_suffix_dots(self):
        self.assertEqual(self.get_filename("my.filename", False, "-suf"), "my.filename-suf")

    def test_filename_with_suffix_dots_extension(self):
        self.assertEqual(self.get_filename("my.filename.ext", True, "-suf"), "my.filename-suf")
