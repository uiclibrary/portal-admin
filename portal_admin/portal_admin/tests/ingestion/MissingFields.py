from .IngestTestCase import BadIngest

from portal_admin.models import Context, Profile
from portal_admin.custom_logging import RequiredFieldNotFound

class MissingFields(BadIngest):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        BadIngest.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='IDOT content dm images').id
        self.test_dir = "portal_admin/tests/test_data/ccc_idot/local_missing_fields/"
        self._tests = ['required_fields']
        self.expected_error = RequiredFieldNotFound
        self.test_name = __name__
