from .IngestTestCase import IngestTestCase

from portal_admin.models import Context, Profile, ElementSource

class Instance2(IngestTestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        IngestTestCase.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='IDOT content dm images').id

        element_source = ElementSource.objects.get(profile__id=self.profile_id, xpath="record/Subject")
        element_source.instance_number = 2  # technically the 3rd, index-2
        element_source.save()
        self.test_dir = "portal_admin/tests/test_data/ccc_idot/instance_2/"
        self.test_name = __name__

