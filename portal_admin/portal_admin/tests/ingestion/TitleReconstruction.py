import os
from xml.etree.ElementTree import parse

from django.test import TestCase

from portal_admin import ingestion
from portal_admin.context_managers import clean_room
from portal_admin.models import Context, DigObject, Profile, DCXMLFormat, ElementSource
from portal_admin.processor import Record

class TitleReconstruction(TestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        self.profile = Profile.objects.get(name='AIC EAD Finding Aids')
        self.context = Context.objects.get(name='Chicago Collections Consortium')
        self.test_dir = 'portal_admin/tests/test_data/aic/title_reconstruction/'
        xml_files = [x for x in os.listdir(self.test_dir) if x.endswith('.xml')]
        records = [parse(open(self.test_dir+f,'r')).getroot() for f in xml_files]
        assert len(records) == 1
        self.record = Record(records[0], xml_files[0], self.test_dir)

    def test_title(self):
        with clean_room(self.test_dir, self.profile.id) as _dir:
            digobj_id = ingestion.create_digobj(self.record, self.profile.id, self.context.id)
        digobj = DigObject.objects.get(id=digobj_id)
        assert digobj.title == 'TITLE, DATE'

    def test_title_main_only(self):
        title_2 = DCXMLFormat.objects.get(element_name='title', concat_order=2)
        esource = ElementSource.objects.get(profile__id=self.profile.id, dcxml_format=title_2)
        esource.delete()
        with clean_room(self.test_dir, self.profile.id) as _dir:
            digobj_id = ingestion.create_digobj(self.record, self.profile.id, self.context.id)
        digobj = DigObject.objects.get(id=digobj_id)
        assert digobj.title == 'TITLE'

    def test_title_main_subtitle(self):
        title_2 = DCXMLFormat.objects.get(element_name='title', concat_order=2)
        esource = ElementSource.objects.get(profile__id=self.profile.id, dcxml_format=title_2)
        esource.delete()
        title_1 = DCXMLFormat.objects.get(element_name='title', concat_order=1)
        ElementSource.objects.create(
            profile=self.profile,
            dcxml_format=title_1,
            xpath='ead/eadheader/filedesc/titlestmt/titleproper')
        with clean_room(self.test_dir, self.profile.id) as _dir:
            digobj_id = ingestion.create_digobj(self.record, self.profile.id, self.context.id)
        digobj = DigObject.objects.get(id=digobj_id)
        assert digobj.title == 'TITLE: TITLE'

    def test_title_all(self):
        title_1 = DCXMLFormat.objects.get(element_name='title', concat_order=1)
        ElementSource.objects.create(
            profile=self.profile,
            dcxml_format=title_1,
            xpath='ead/eadheader/filedesc/titlestmt/titleproper')
        with clean_room(self.test_dir, self.profile.id) as _dir:
            digobj_id = ingestion.create_digobj(self.record, self.profile.id, self.context.id)
        digobj = DigObject.objects.get(id=digobj_id)
        assert digobj.title == 'TITLE: TITLE, DATE'


