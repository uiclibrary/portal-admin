from .IngestTestCase import IngestTestCase

from portal_admin.models import Context, Profile
from portal_admin.tests.test_data.aic.ead_ica import load_digobj_fvalues

class AICEADFindingAid(IngestTestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        IngestTestCase.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='AIC EAD Finding Aids').id
        self.test_dir = 'portal_admin/tests/test_data/aic/ead_ica/'
        #self.load_digobj_fvalues = load_digobj_fvalues
        self.dc_xml_control_dir = 'portal_admin/tests/test_data/dc_xml/aic/'
        self.test_name = __name__
