import os, shutil
from unittest import SkipTest
import tempfile

from xml.etree.ElementTree import parse, fromstring

from django.conf import settings
from django.test import LiveServerTestCase
from django.core.management import call_command

from portal_admin.models import Profile, Context, Location, DigObjectForm, ElementSource
from portal_admin.models import DigObject, ImportedValue, DigObjectContext, EventType, Event
from portal_admin.models import DCXMLFormat
from portal_admin import custom_logging as err
from portal_admin.context_managers import clean_room
from portal_admin.inspection import verify_xml_structure
from portal_admin.inspection import verify_has_xml
from portal_admin.inspection import verify_no_duplicate
from portal_admin.inspection import content
from portal_admin.inspection import xml_fields
from portal_admin.ingestion import create_digobj
from portal_admin.ingestion import import_values
from portal_admin.ingestion import create_files
from portal_admin.ingestion import create_dc_xml
from portal_admin import utils
from portal_admin.processor import Record

class MessagesFound(Exception):
    pass

class IngestTestCase(LiveServerTestCase):

    def setUp(self):
        self.load_digobj_fvalues = None
        self.dc_xml_control_dir = None
        LiveServerTestCase.setUp(self)

    @property
    def location(self):
        try:
            loc = Location.objects.get(id=self.location_id)
            return loc
        except:
            return None

    @property
    def context(self):
        return Context.objects.get(id=self.context_id)

    @property
    def profile(self):
        '''Ensure freshness.'''
        return Profile.objects.get(id=self.profile_id)

    @property
    def xml_files(self):
        return [x for x in os.listdir(self.test_dir) if x.endswith('xml')]

    @property
    def records(self):
        return [Record(parse(open(self.test_dir+x, 'r')).getroot(), x, self.test_dir) for x in self.xml_files]

    def test_has_xml(self):
        '''Verifies that an XML files exists.'''
        verify_has_xml(self.test_dir)

    def test_valid_xml(self):
        '''Exercise xmllint to ensure valid XML.'''
        for f in self.xml_files:
            verify_xml_structure(self.test_dir+f)

    def test_split_xml(self):
        '''Verify split to XML files functionality.'''
        _records = []
        _xml = [fromstring(utils.strip_ns(open(self.test_dir+f, 'r').read())) for f in self.xml_files]
        if self.profile.split_node:
            _records = [x.findall(self.profile.split_node) for x in _xml][0]
        else:
            _records = [x for x in _xml]
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            clean_xml = [x for x in os.listdir(self.test_dir) if x.endswith('xml')]
            assert len(clean_xml) == len(_records)

    def test_required_fields(self):
        '''Inspect XML for required fields.'''
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            for record in self.records:
                record = xml_fields(record, self.profile_id)
                if record.messages:
                    raise MessagesFound

    def test_content(self):
        '''Exercise the system to verify images.'''
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            for record in self.records:
                record = content(record, self.profile_id)
                if record.messages:
                    raise MessagesFound

    def test_digobject(self):
        '''Verifiy digital object creation.'''
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            for record in self.records:
                digobj_id = create_digobj(record, self.profile_id, self.context_id, self.location)

    def test_import_values(self):
        '''Verify imported values.'''
        num_digobjs = DigObject.objects.all().count()
        num_digobjcontexts = DigObjectContext.objects.all().count()
        num_events = Event.objects.all().count()
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            for record in self.records:
                digobj_id = create_digobj(record, self.profile_id, self.context_id, self.location)
                import_values(digobj_id, self.profile_id, record)
                current_num_digobjs = DigObject.objects.all().count()
                current_num_digobjcontexts = DigObjectContext.objects.all().count()
                current_num_events = Event.objects.all().count()
                assert current_num_digobjs == num_digobjs + 1
                assert current_num_digobjcontexts == num_digobjcontexts + 1
                # todo: assert current_num_events == num_events + 1
                num_digobjs = current_num_digobjs
                num_digobjcontexts = current_num_digobjcontexts
                num_events = current_num_events

    def test_files(self):
        '''Creates a digital object, then creates file records for that object.'''
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            for record in self.records:
                digobj_id = create_digobj(record, self.profile_id, self.context_id, self.location)
                import_values(digobj_id, self.profile_id, record)
                create_files(digobj_id, self.profile_id, self.context_id, record)

    def test_duplicates(self):
        '''Verify duplication check mechanism.'''
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            for record in self.records:
                record = verify_no_duplicate(record, self.profile_id)
                if record.messages:
                    raise MessagesFound

    def test_dc_xml(self):
        '''Create dc.xml file.'''
        if not self.load_digobj_fvalues or not self.dc_xml_control_dir:
            raise SkipTest

        created_objs = []
        with clean_room(self.test_dir, self.profile_id) as self.test_dir:
            for i, record in enumerate(self.records):
                digobj_id = create_digobj(record, self.profile_id, self.context_id, self.location)
                created_objs.append(digobj_id)
                import_values(digobj_id, self.profile_id, record)
                create_files(digobj_id, self.profile_id, self.context_id, record)
            self.load_digobj_fvalues()

            for digobj_id in created_objs:
                create_dc_xml(digobj_id, self.context_id)

            dc_xmls = {}
            for digobj_id in created_objs:
                digobj = DigObject.objects.get(id=digobj_id)
                path = utils.digobj_path(digobj_id, self.context_id)
                generated_file = path.format('data') + digobj.ark + '.dc.xml'
                ivalue = ImportedValue.objects.get(
                    digobject=digobj,
                    dcxml_format=DCXMLFormat.objects.get(element_name='identifier'))
                local_id = ivalue.value.element
                dc_xmls[local_id]= generated_file

            control_xmls = [x for x in os.listdir(self.dc_xml_control_dir) if x.endswith('.dc.xml')]
            for xml in control_xmls:
                control_id = xml.split(".")[0]
                control_etree = fromstring(utils.strip_ns(open(self.dc_xml_control_dir+xml, 'r').read()))

                test_etree = fromstring(utils.strip_ns(
                    open(dc_xmls[control_id], 'r').read()))
                control_len = len([x for x in control_etree.getchildren()])
                test_len = len([x for x in test_etree.getchildren()])
                assert control_len == test_len

class BadIngest(IngestTestCase):
    """
    This class' goal is to allow a developer to quickly and easily hook into the machinery above,
    while at the same time reducing the setup of a given test.  In particular, this class exists
    so that specific failures can be examined within the system.

    A subclass off BadIngest() need only modify the particulars it requires to force an error, and
    then define the expected failure.  Then, BadIngest() will exercise the specific piece of code
    and ensure that the exact error(s) occured, as expected.
    """
    # TODO: Update the MessagesFound mechanism.  Right now, the system will ensure that errors
    #       occured if they were expected to - but it does not verify the type of error.

    # This system could be expected to :
    #   1. Verify the type of error, aka message, received
    #   2. Allow sub-classes to define a set of expected messages, and verify against that

    # Current test results, while useful, may be ambiguous and not always 100% correct, for example:
    #   If a test exercised against content, and expected to fail because a value is not found;
    #   but rather it fails because a field does not exist - this will pass, and the test will not
    #   raise any alerts.  Of course, if no failure occurs, or if a truly unexpected error is thrown
    #   the test will, indeed, fail.

    # So, the MessagesFound mechanism needs updating.

    def test_has_xml(self):
        if 'has_xml' not in self._tests:
            raise SkipTest
        try:
            IngestTestCase.test_has_xml(self)
        except:
            self.expected_error

    def test_valid_xml(self):
        if 'valid_xml' not in self._tests:
            raise SkipTest
        try:
            IngestTestCase.test_valid_xml(self)
        except:
            self.expected_error

    def test_required_fields(self):
        if 'required_fields' not in self._tests:
            raise SkipTest
        try:
            IngestTestCase.test_required_fields(self)
            raise
        except MessagesFound:
            pass

    def test_content(self):
        if 'content' not in self._tests:
            raise SkipTest
        try:
            IngestTestCase.test_content(self)
        except MessagesFound:
            pass

    def test_duplicates(self):
        if 'duplicates' not in self._tests:
            raise SkipTest
        try:
            test_dir = self.test_dir
            IngestTestCase.test_files(self)
            self.test_dir = test_dir
            IngestTestCase.test_duplicates(self)
        except MessagesFound:
            pass

    def test_split_xml(self):
        if 'split_xml' not in self._tests:
            raise SkipTest
        IngestTestCase.test_split_xml(self)

    def test_digobject(self):
        if 'digobject' not in self._tests:
            raise SkipTest
        IngestTestCase.test_digobject(self)

    def test_import_values(self):
        if 'import_values' not in self._tests:
            raise SkipTest
        IngestTestCase.test_import_values(self)

    def test_files(self):
        if 'files' not in self._tests:
            raise SkipTest
        IngestTestCase.test_files(self)

    def test_dc_xml(self):
        if 'dc_xml' not in self._tests:
            raise SkipTest
        IngestTestCase.test_dc_xml(self)


