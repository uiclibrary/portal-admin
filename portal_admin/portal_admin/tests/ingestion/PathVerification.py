import os
from xml.etree.ElementTree import parse

from django.test import TestCase

from portal_admin import ingestion, utils
from portal_admin.context_managers import clean_room
from portal_admin.models import Context, DigObject, Profile, DCXMLFormat, ElementSource
from portal_admin.processor import Record
from django.conf import settings

class PathVerification(TestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        self.profile = Profile.objects.get(name='AIC EAD Finding Aids')
        self.context = Context.objects.get(name='Chicago Collections Consortium')
        self.test_dir = 'portal_admin/tests/test_data/aic/title_reconstruction/'
        xml_files = [x for x in os.listdir(self.test_dir) if x.endswith('.xml')]
        records = [parse(open(self.test_dir+f,'r')).getroot() for f in xml_files]
        assert len(records) == 1
        self.record = Record(records[0], xml_files[0], self.test_dir)

    def test_path(self):
        with clean_room(self.test_dir, self.profile.id) as _dir:
            digobj_id = ingestion.create_digobj(self.record, self.profile.id, self.context.id)
            digobj = DigObject.objects.get(id=digobj_id)
            path = utils.digobj_path(digobj_id, self.context.id)
            assert path.format('data') == (settings.FILE_ROOT + 'Deposited/data/archivalcollections/artic/3/')
