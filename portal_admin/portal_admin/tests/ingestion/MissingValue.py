from .IngestTestCase import BadIngest

from portal_admin.models import Context, Profile
from portal_admin.custom_logging import RequiredValueNotFound

class MissingValue(BadIngest):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        BadIngest.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='Archival Photographic Files').id
        self.test_dir = "portal_admin/tests/test_data/uchicago/local_missing_value/"
        self._tests = ['required_fields']
        self.expected_error = RequiredValueNotFound
        self.test_name = __name__

