from .IngestTestCase import BadIngest

from portal_admin.models import Context, Profile
from portal_admin.custom_logging import InvalidRemoteContent

class RemoteMissingImages(BadIngest):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        BadIngest.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='IDOT content dm images').id
        profile = Profile.objects.get(id=self.profile_id)
        profile.is_remote = True
        profile.save()

        self.test_dir = "portal_admin/tests/test_data/ccc_idot/remote_missing_images/"
        self.test_name = __name__
        self._tests = ['content']
        self.expected_error = InvalidRemoteContent


