from django.test import TestCase
from xml.etree.ElementTree import fromstring

from portal_admin.utils import get_clean_values

class GetCleanValues(TestCase):
    
    def test_empty_instances(self):
        record = fromstring("<record><title>Title</title><title/></record>")
        xpath = "record/title"
        values = [v for v in get_clean_values(record, xpath, 0)]
        self.assertEqual(values, ["Title"])    
    
    def test_delimiter_and_instances(self):
        record = fromstring("<record><subject>cats;dogs</subject><subject>hamsters;goldfish</subject></record>")
        xpath = "record/subject"
        values = [v for v in get_clean_values(record, xpath, -1, ";")]
        self.assertEqual(values, ["cats", "dogs", "hamsters", "goldfish"])
