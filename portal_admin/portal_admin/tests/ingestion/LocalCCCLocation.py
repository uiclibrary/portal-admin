from .IngestTestCase import IngestTestCase

from portal_admin.models import Context, Profile, Location

class LocalCCCLocation(IngestTestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        IngestTestCase.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='IDOT content dm images').id
        self.location_id = Location.objects.get(name='LHS-Chicago Special Collections and University Archives')
        self.test_dir = "portal_admin/tests/test_data/ccc_idot/local_w_location/"
        self.test_name = __name__

