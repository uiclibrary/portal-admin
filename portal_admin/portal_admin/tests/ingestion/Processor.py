import os, shutil, tempfile

from django.test import TestCase

from django.contrib.auth.models import User
from portal_admin.models import Context, Profile, Event, MetaEvent, DigObject, File
from portal_admin.processor import process_input

from django.conf import settings

class Processor(TestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json', 'test/test_users.json']

    def failed_in_log(self, meta_event, failed_events):
        self.assertIsNotNone(meta_event.error_log_url)
        with open(settings.FILE_ROOT + meta_event.error_log_url, "r") as f:
            log = f.read()
            for fe in failed_events:
                self.assertIn(fe.event_type.log_help_text.replace("\r\n", ""), log.replace("\n", ""))
 
    def test_multi_error_single_xml(self):
        test_dir = 'portal_admin/tests/test_data/ccc_idot/processor/'
        profile = Profile.objects.get(name='IDOT content dm images')
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 14)
        devents = events.filter(event_type__name="Deposited")
        self.assertEqual(devents.count(), 6)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 6)
        fevents = events.filter(event_type__name="Deposit Failed: Metadata File Not Found")
        self.assertEqual(fevents.count(), 3)
        self.failed_in_log(meta_event, fevents)
        fevents = events.filter(event_type__name="Deposit Failed: Missing Required Values")
        self.assertEqual(fevents.count(), 1)
        self.failed_in_log(meta_event, fevents)
        fevents = events.filter(event_type__name="Deposit Failed: Missing Required Elements")
        self.assertEqual(fevents.count(), 1)
        self.failed_in_log(meta_event, fevents)
        fevents = events.filter(event_type__name="Deposit Failed: Media File Not Found")
        self.assertEqual(fevents.count(), 3)
        self.failed_in_log(meta_event, fevents)

    def test_multi_error_multi_xml(self):
        test_dir = 'portal_admin/tests/test_data/uchicago/processor/'
        profile = Profile.objects.get(name='Archival Photographic Files')
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 12)
        devents = events.filter(event_type__name="Deposited")
        self.assertEqual(devents.count(), 8)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 8)
        fevents = events.filter(event_type__name="Deposit Failed: Metadata File Not Found")
        self.assertEqual(fevents.count(), 2)
        fevents = events.filter(event_type__name="Deposit Failed: Missing Required Values")
        self.assertEqual(fevents.count(), 1)
        fevents = events.filter(event_type__name="Deposit Failed: Missing Required Elements")
        self.assertEqual(fevents.count(), 1)

    def test_missing_xml(self):
        test_dir = 'portal_admin/tests/test_data/ccc_idot/empty_episode/'
        profile = Profile.objects.get(name='IDOT content dm images')
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 1)
        self.assertEqual(events.filter(event_type__name="Deposited").count(), 0)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 0)
        fevents = events.filter(event_type__name="Deposit Failed: Metadata File Not Found")
        self.assertEqual(fevents.count(), 1)
        self.failed_in_log(meta_event, fevents)

    def test_invalid_xml(self):
        test_dir = 'portal_admin/tests/test_data/ccc_idot/local_bad_xml/'
        tmpd = tempfile.mkdtemp() + '/'
        for f in os.listdir(test_dir):
            shutil.copy(test_dir + f, tmpd)
        profile = Profile.objects.get(name='IDOT content dm images')
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, tmpd, location.id)
        shutil.rmtree(tmpd)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 1)
        self.assertEqual(events.filter(event_type__name="Deposited").count(), 0)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 0)
        fevents = events.filter(event_type__name="Deposit Failed: XML Validation Failed")
        self.assertEqual(fevents.count(), 1)
        self.failed_in_log(meta_event, fevents)

    def test_wrong_profile(self):
        test_dir = 'portal_admin/tests/test_data/uchicago/processor/'
        profile = Profile.objects.get(name='IDOT content dm images')
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.filter(event_type__name="Deposited").count(), 0)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 0)
        fevents = events.filter(event_type__name="Deposit Failed: Multiple XML Files") 
        self.assertEqual(fevents.count(), 1)
        self.failed_in_log(meta_event, fevents)

    def test_missing_metadata(self):
        test_dir = 'portal_admin/tests/test_data/uchicago/metadata_missing/'
        profile = Profile.objects.get(name='Archival Photographic Files')
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 3)
        self.assertEqual(events.filter(event_type__name="Deposited").count(), 1)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 1)
        fevents = events.filter(event_type__name="Deposit Failed: Metadata File Not Found") 
        self.assertEqual(fevents.count(), 2)

    def test_namespace_split_node(self):
        test_dir = 'portal_admin/tests/test_data/vra/'
        context = Context.objects.get(name='Chicago Collections Consortium')
        profile = Profile.objects.get(name="Northwestern VRA Core")
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 1)
        self.assertEqual(events.filter(event_type__name="Deposited").count(), 0)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 0)
        fevents = events.filter(event_type__name="Deposit Failed: Missing Required Elements")
        self.assertEqual(fevents.count(), 1)

    def test_invalid_record_separator(self):
        test_dir = 'portal_admin/tests/test_data/vra/'
        context = Context.objects.get(name='Chicago Collections Consortium')
        profile = Profile.objects.get(name='IDOT content dm images')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        self.assertEqual(meta_event.profile, profile)
        self.assertEqual(meta_event.context, context)
        self.assertEqual(meta_event.user, user)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 1)
        self.assertEqual(events.filter(event_type__name="Deposited").count(), 0)
        self.assertEqual(DigObject.objects.filter(profile=profile).count(), 0)
        fevents = events.filter(event_type__name="Deposit Failed: Invalid Record Separator")
        self.assertEqual(fevents.count(), 1)

    def test_pdf_finding_aids(self):
        test_dir = 'portal_admin/tests/test_data/loyola_pdf/'
        context = Context.objects.get(name='Chicago Collections Consortium')
        profile = Profile.objects.get(name='Loyola PDF with MARC')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location.id)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 1)
        self.assertEqual(events.filter(event_type__name="Deposited").count(), 1)
        objs = DigObject.objects.filter(profile=profile)
        self.assertEqual(objs.count(), 1)
        files = File.objects.filter(digobject__in=objs)
        self.assertEqual(files.count(), 2)

    # The following 3 tests are general wrong profile tests to make sure
    # the system at least completes without a horrible error regardless of
    # the profile selected -- ev 2014-8-7
    def test_wrong_profiles1(self):
        test_dir = 'portal_admin/tests/test_data/uchicago/processor/'
        context = Context.objects.get(name='Chicago Collections Consortium')
        user = User.objects.get(username="kate")
        for profile in Profile.objects.all():
            location_id = None
            locations = profile.institution.location_set.all()
            if locations.count() > 0:
                location_id = locations[0].id
            meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location_id)
            self.assertEqual(meta_event.profile, profile)
            self.assertEqual(meta_event.context, context)
            self.assertEqual(meta_event.user, user)

    def test_wrong_profiles2(self):
        test_dir = 'portal_admin/tests/test_data/ccc_idot/processor/'
        context = Context.objects.get(name='Chicago Collections Consortium')
        user = User.objects.get(username="kate")
        for profile in Profile.objects.all():
            location_id = None
            locations = profile.institution.location_set.all()
            if locations.count() > 0:
                location_id = locations[0].id
            meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location_id)
            self.assertEqual(meta_event.profile, profile)
            self.assertEqual(meta_event.context, context)
            self.assertEqual(meta_event.user, user)

    def test_wrong_profiles3(self):
        test_dir = 'portal_admin/tests/test_data/aic/ead_ica/'
        context = Context.objects.get(name='Chicago Collections Consortium')
        user = User.objects.get(username="kate")
        for profile in Profile.objects.all():
            location_id = None
            locations = profile.institution.location_set.all()
            if locations.count() > 0:
                location_id = locations[0].id
            meta_event, unexpected = process_input(user.id, profile.id, context.id, test_dir, location_id)
            self.assertEqual(meta_event.profile, profile)
            self.assertEqual(meta_event.context, context)
            self.assertEqual(meta_event.user, user)
