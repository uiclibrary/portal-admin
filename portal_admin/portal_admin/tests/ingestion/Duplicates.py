from .IngestTestCase import BadIngest

from portal_admin.models import Context, File, Profile

from portal_admin.utils import get_checksum
from portal_admin.custom_logging import DuplicateFileFound

class Duplicates(BadIngest):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        BadIngest.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='IDOT content dm images').id
        self.test_dir = "portal_admin/tests/test_data/ccc_idot/local_single_xml/"
        self._tests = ['duplicates']
        self.expected_error = DuplicateFileFound
        self.test_name = __name__

