from .IngestTestCase import IngestTestCase

from portal_admin.models import Context, Profile

class MARCFindingAid(IngestTestCase):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        IngestTestCase.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='UIC MARCXML').id
        self.test_dir = 'portal_admin/tests/test_data/marc/finding_aid/'
        self.test_name = __name__
