from .IngestTestCase import BadIngest

from portal_admin.models import Profile, Context
from portal_admin.custom_logging import NoXMLFound

class EmptyEpisode(BadIngest):
    fixtures = ['initial_data.json', 'test/test_ccc.json']

    def setUp(self):
        BadIngest.setUp(self)

        self.context_id = Context.objects.get(name='Chicago Collections Consortium').id
        self.profile_id = Profile.objects.get(name='IDOT content dm images').id
        self.test_dir = "portal_admin/tests/test_data/ccc_idot/empty_episode"
        self._tests = ['has_xml']
        self.expected_error = NoXMLFound
        self.test_name = __name__


