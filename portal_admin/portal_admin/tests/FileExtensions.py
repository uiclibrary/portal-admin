import os, shutil, tempfile

from django.test import TestCase

from django.contrib.auth.models import User
from portal_admin.models import Context, Profile, Event, MetaEvent, DigObject, File
from portal_admin.processor import process_input

from django.conf import settings

class FileExtensions(TestCase):
    fixtures = [
        'initial_data.json',
        'test/test_ccc.json',
        'test/test_users.json',
        'test/extension_test.json'
        ]

    def test_ext_no_suffix(self):
        test_dir = "portal_admin/tests/test_data/file_extensions/"
        tmpd = tempfile.mkdtemp() + "/"
        shutil.copy(test_dir + "uic_idot_extension.xml", tmpd)
        shutil.copy(test_dir + "IDOT_0_0_51.jpg", tmpd)
        profile = Profile.objects.get(name="Has extension - IDOT")
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(
            user.id, profile.id, context.id, tmpd, location.id)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 1)
        self.assertEqual(
            events.filter(event_type__name="Deposited").count(), 1)
        self.assertIn("IDOT_0_0_51.jpg",
            File.objects.values_list('filename_original', flat=True))
        shutil.rmtree(tmpd)

    def test_ext_suffix(self):
        test_dir = "portal_admin/tests/test_data/file_extensions/"
        tmpd = tempfile.mkdtemp() + "/"
        shutil.copy(test_dir + "apf3-00002.xml", tmpd)
        shutil.copy(test_dir + "apf3-00002r.jpg", tmpd)
        profile = Profile.objects.get(
            name="Archival Photograph - Has extension")
        context = Context.objects.get(name='Chicago Collections Consortium')
        location = profile.institution.location_set.all()[0]
        user = User.objects.get(username="kate")
        meta_event, unexpected = process_input(
            user.id, profile.id, context.id, tmpd, location.id)
        events = meta_event.event_set.all()
        self.assertEqual(events.count(), 1)
        self.assertEqual(
            events.filter(event_type__name="Deposited").count(), 1)
        self.assertIn("apf3-00002r.jpg",
            File.objects.values_list('filename_original', flat=True))
        shutil.rmtree(tmpd)
