import os, shutil
from django.test import TestCase
from django.core.urlresolvers import reverse
from selenium import webdriver
from .selenium.SeleniumTest import SeleniumTest

from django.contrib.auth.models import User
from portal_admin.models import (DigObject,
                                 Connection,
                                 DigObjectStatus,
                                 Profile,
                                 Context,
                                 Event,
                                 EventType,
                                 MetaEvent,
                                 DigObjectContext,)

from portal_admin.processor import process_input
from portal_admin.tasks import start_indexer

from django.conf import settings

from unittest.mock import patch

class PreviewMain(TestCase):
    '''
    Tests to ensure proper execution of conversion of records from Deposited to Preview,
    with all necessary subroutines.
    '''
    fixtures = ['initial_data.json', 'test/test_ccc.json', 'test/test_users.json']

    def setUp(self):
        self.status_deposited = DigObjectStatus.objects.get(status="Deposited")
        self.status_previewed = DigObjectStatus.objects.get(status="Previewed")

        self.test_dir = 'portal_admin/tests/test_data/ccc_idot/preview/'
        self.profile = Profile.objects.get(name='IDOT content dm images')
        self.context = Context.objects.get(name='Chicago Collections Consortium')
        self.location = self.profile.institution.location_set.all()[0]
        self.user = User.objects.get(username="kate")

        # make sure files don't exist before deposit
        file_root = settings.FILE_ROOT
        if os.path.isdir(file_root):
            shutil.rmtree(file_root)

        self.meta_event, self.unexpected = process_input(self.user.id,
                                                         self.profile.id,
                                                         self.context.id,
                                                         self.test_dir,
                                                         self.location.id)

        self.objs_deposited = DigObject.objects.filter(digobjectcontext__digobject_status=1)
        self.objs_previewed = DigObject.objects.filter(digobjectcontext__digobject_status=2)

        self.obj1 = DigObject.objects.get(pk=1)
        self.obj2 = DigObject.objects.get(pk=2)
        self.obj3 = DigObject.objects.get(pk=3)

    def tearDown(self):
        # clean up after deposit
        #file_root = settings.FILE_ROOT
        #if os.path.isdir(file_root):
        #    shutil.rmtree(file_root)
        pass

    def test_deposited(self):
        '''object should have been properly deposited'''
        self.assertTrue(self.objs_deposited)
        self.assertEqual(len(self.objs_deposited), 10)
        self.assertEqual(len(self.objs_previewed), 0)

        # isolate statuses
        for digobject in self.objs_deposited:
            # has data and media files
            xml = '{}/Deposited/data/images/uic/1/{}.xml'.format(settings.FILE_ROOT, digobject.ark)
            img = '{}/Deposited/media/images/uic/1/{}.jpg'.format(settings.FILE_ROOT, digobject.ark)
            self.assertTrue(os.path.isfile(xml))
            self.assertTrue(os.path.isfile(img))

    def successful_reindex(index, directory):
        arks = [f.replace(".xml", "") for f in os.listdir("{}Previewed/data/{}".format(settings.FILE_ROOT, directory))]
        return arks, '', ''

    @patch('portal_admin.tasks.perform_reindex', side_effect=successful_reindex)
    def test_change_status_to_preview(self, reindex_function):
        '''object status should change to "Previewed" from "Deposited"'''

        self.assertEqual(len(self.objs_deposited), 10)
        self.assertEqual(len(self.objs_previewed), 0)
        for digobject in self.objs_deposited:
            # has status: deposited
            self.assertEqual(digobject.get_status(self.context), self.status_deposited)
        for digobject in self.objs_previewed:
            # has status: previewed
            self.assertEqual(digobject.get_status(self.context), self.status_previewed)

        # change status
        self.assertEqual(self.obj1.get_status(self.context), self.status_deposited)
        self.assertEqual(self.obj2.get_status(self.context), self.status_deposited)
        self.assertEqual(self.obj3.get_status(self.context), self.status_deposited)

        docs = DigObjectContext.objects.filter(digobject__in=[self.obj1.pk, self.obj2.pk],
                                               context=self.context).values_list('pk', flat=True)
        start_indexer(self.user.id,
                      self.context.id,
                      docs,
                      "http://localhost/",
                      "Previewed")

        self.assertEqual(self.obj1.get_status(self.context), self.status_previewed)
        self.assertEqual(self.obj2.get_status(self.context), self.status_previewed)

        # double check
        self.objs_deposited = DigObject.objects.filter(digobjectcontext__digobject_status=1)
        self.objs_previewed = DigObject.objects.filter(digobjectcontext__digobject_status=2)
        self.assertEqual(len(self.objs_deposited), 8)
        self.assertEqual(len(self.objs_previewed), 2)
        for digobject in self.objs_deposited:
            # has status: deposited
            self.assertEqual(digobject.get_status(self.context), self.status_deposited)
        for digobject in self.objs_previewed:
            # has status: previewed
            self.assertEqual(digobject.get_status(self.context), self.status_previewed)

    @patch('portal_admin.tasks.perform_reindex', side_effect=successful_reindex)
    def test_duplicate_directory_structure(self, reindex_function):
        '''proper directory structure should be created for previewed objects'''

        deposited_dirs = ["{}Deposited/data/images/uic/1".format(settings.FILE_ROOT),
                          "{}Deposited/media/images/uic/1/thumbs".format(settings.FILE_ROOT)]

        previewed_dirs = ["{}Previewed/data/images/uic/1".format(settings.FILE_ROOT),
                          "{}Previewed/media/images/uic/1/thumbs".format(settings.FILE_ROOT)]

        for d in deposited_dirs:
            self.assertTrue(os.path.isdir(d))
        for d in previewed_dirs:
            self.assertFalse(os.path.isdir(d))

        docs = DigObjectContext.objects.filter(digobject__in=[self.obj1.pk, self.obj2.pk],
                                               context=self.context).values_list('pk', flat=True)
        start_indexer(self.user.id,
                      self.context.id,
                      docs,
                      "http://localhost/",
                      "Previewed")

        for d in deposited_dirs:
            self.assertTrue(os.path.isdir(d))
        for d in previewed_dirs:
            self.assertTrue(os.path.isdir(d))


    @patch('portal_admin.tasks.perform_reindex', side_effect=successful_reindex)
    def test_event_created(self, reindex_function):
        '''preview events and meta_events should execute properly'''

        # cursory examination of event quantities
        events_0      = Event.objects.all()
        meta_events_0 = MetaEvent.objects.all()

        events_quantity = len(events_0)
        meta_events_quantity = len(meta_events_0)

        docs = DigObjectContext.objects.filter(digobject__in=[self.obj1.pk],
                                               context=self.context).values_list('pk', flat=True)
        start_indexer(self.user.id,
                      self.context.id,
                      docs,
                      "http://localhost/",
                      "Previewed")

        events_1      = Event.objects.all()
        meta_events_1 = MetaEvent.objects.all()
        self.assertEqual(len(events_1), events_quantity + 1)
        self.assertEqual(len(meta_events_1), meta_events_quantity + 1)

        docs = DigObjectContext.objects.filter(digobject__in=[self.obj2.pk],
                                               context=self.context).values_list('pk', flat=True)
        start_indexer(self.user.id,
                      self.context.id,
                      docs,
                      "http://localhost/",
                      "Previewed")

        events_2      = Event.objects.all()
        meta_events_2 = MetaEvent.objects.all()
        self.assertEqual(len(events_2), events_quantity + 2)
        self.assertEqual(len(meta_events_2), meta_events_quantity + 2)

        events_new = []
        for event in events_2:
            if event not in events_0:
                events_new.append(event)
        self.assertEqual(len(events_new), 2)

        meta_events_new = []
        for meta_event in meta_events_2:
            if meta_event not in meta_events_0:
                meta_events_new.append(meta_event)
        self.assertEqual(len(meta_events_new), 2)

        event1 = Event.objects.get(event_type__name='Status Change: Previewed',
                                   digobject=self.obj1)
        event2 = Event.objects.get(event_type__name='Status Change: Previewed',
                                   digobject = self.obj2)
        self.assertTrue(event1)
        self.assertTrue(event1)

    @patch('portal_admin.tasks.perform_reindex', side_effect=successful_reindex)
    def test_symlinks(self, reindex_function):
        '''proper file structure should be created for each previewed object'''
        objs = [self.obj1, self.obj2]

        docs = DigObjectContext.objects.filter(digobject__in=[self.obj1.pk, self.obj2.pk],
                                               context=self.context).values_list('pk', flat=True)
        start_indexer(self.user.id,
                      self.context.id,
                      docs,
                      "http://localhost/",
                      "Previewed")

        for obj in objs:
            i_data = "{}Deposited/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, obj.ark)
            i_media = "{}Deposited/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, obj.ark)
            i_thumb = "{}Deposited/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, obj.ark)
            p_data = "{}Previewed/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, obj.ark)
            p_media = "{}Previewed/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, obj.ark)
            p_thumb = "{}Previewed/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, obj.ark)

            self.assertTrue(os.path.isfile(i_data))
            self.assertTrue(os.path.isfile(i_media))
            self.assertTrue(os.path.isfile(i_thumb))
            self.assertTrue(os.path.isfile(p_data))
            self.assertTrue(os.path.isfile(p_media))
            self.assertTrue(os.path.isfile(p_thumb))

        obj3 = self.obj3
        obj3_i_xml = "{}Deposited/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, obj3.ark)
        obj3_i_img = "{}Deposited/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, obj3.ark)
        obj3_i_thumb = "{}Deposited/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, obj3.ark)
        obj3_p_xml = "{}Previewed/data/images/uic/1/{}.xml".format(settings.FILE_ROOT, obj3.ark)
        obj3_p_img = "{}Previewed/media/images/uic/1/{}.jpg".format(settings.FILE_ROOT, obj3.ark)
        obj3_p_thumb = "{}Previewed/media/images/uic/1/thumbs/{}.jpg".format(settings.FILE_ROOT, obj3.ark)

        self.assertTrue(os.path.isfile(obj3_i_xml))
        self.assertTrue(os.path.isfile(obj3_i_img))
        self.assertTrue(os.path.isfile(obj3_i_thumb))
        self.assertFalse(os.path.isfile(obj3_p_xml))
        self.assertFalse(os.path.isfile(obj3_p_img))
        self.assertFalse(os.path.isfile(obj3_p_thumb))

    @patch('portal_admin.tasks.perform_reindex', side_effect=successful_reindex)
    def test_multiple_profiles(self, reindex_function):
        # import with another profile
        ead_profile = Profile.objects.get(name='EAD')
        meta_event, unexpected = process_input(self.user.id,
                                               ead_profile.id,
                                               self.context.id,
                                               'portal_admin/tests/test_data/uic_ead/',
                                               self.location.id)

        self.assertEqual(DigObject.objects.filter(digobjectcontext__digobject_status__status="Deposited").count(), 34)
        self.assertEqual(DigObject.objects.filter(digobjectcontext__digobject_status__status="Previewed").count(), 0)

        img_obj = DigObject.objects.filter(profile=self.profile)[0]
        ead_obj = DigObject.objects.filter(profile=ead_profile)[0]

        docs = DigObjectContext.objects.filter(digobject__in=[img_obj.pk, ead_obj.pk],
                                               context=self.context).values_list('pk', flat=True)

        start_indexer(self.user.id,
                      self.context.id,
                      docs,
                      "http://localhost/",
                      "Previewed")

        self.assertEqual(DigObject.objects.filter(digobjectcontext__digobject_status__status="Deposited").count(), 32)
        self.assertEqual(DigObject.objects.filter(digobjectcontext__digobject_status__status="Previewed").count(), 2)
