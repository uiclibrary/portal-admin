from portal_admin.models import DCXMLFormat

from django.utils.module_loading import import_by_path
from django.conf import settings


def autotag(digobjects, trigger_terms, meta_event):
    facet_counts = {}
    facet_values = []
    for dcxml_format in DCXMLFormat.objects.all():
        autotagger_class = import_by_path(settings.AUTOTAGGERS.get(dcxml_format.element_name,
                                                                   "portal_admin.autotaggers.DefaultAutotagger"))
        autotagger = autotagger_class()
        counts, fvalues = autotagger.autotag(digobjects,
                                             dcxml_format,
                                             trigger_terms.filter(dcxml_format=dcxml_format),
                                             meta_event)
        for f in counts:
            if f in facet_counts:
                facet_counts[f] += counts[f]
            else:
                facet_counts[f] = counts[f]
        facet_values.extend(fvalues)
    return facet_counts, facet_values
