import tempfile, shutil, os
import logging
from contextlib import contextmanager

logr = logging.getLogger(__name__)

from django.conf import settings

from portal_admin.models import Profile
from portal_admin import utils

def strip_ns_file(fpath):
    """
    Strips the any XML namespace information from
    the particular file, using the method:
    portal_admin.utils.strip_ns()
    """

    with open(fpath, 'r', encoding="utf-8") as infile:
        data = infile.read()
    with open(fpath, 'w', encoding="utf-8") as outfile:
        outfile.write(utils.strip_ns(data))

@contextmanager
def clean_room(fpath, profile_id):
    """
    Accepts as input a particular file path, which is
    assumed to contain an archivist upload - be it a single
    XML file, or many; with or without media files.

    This contextmanager generates a temporary directory, and
    attempts to clean the given upload.  This may include:
    - parsing a large, multi-record XML file into many smaller files
    - removing XML namespace(s)
    - removing HTML tags
    ...

    In general, the goal of this contextmanager is to provide a
    sanitized set of input, for the processor to fire against; such
    that regardless of the upload, once cleaned, the input set has
    the same form and is ready to work on.
    """

    profile = Profile.objects.get(id=profile_id)
    xml_fpaths = [fpath+x for x in os.listdir(fpath) if x.endswith('xml')]
    allowed_exts = settings.ALLOWED_EXTS[profile.digobject_form._type]
    other_files = [x for x in os.listdir(fpath) if os.path.isfile(fpath + x) and x.split(".")[-1] in allowed_exts]
    tempdir = tempfile.mkdtemp() + '/'

    if profile.split_node and len(profile.split_node):
        if len(xml_fpaths) > 0:
            xml_file = xml_fpaths[0]
            strip_ns_file(xml_file)
            utils.split_xml(xml_file, profile_id, tempdir)
            xml_fpaths = [tempdir + x for x in os.listdir(tempdir) if x.endswith('xml')]
    else:
        [shutil.copy(x, tempdir) for x in xml_fpaths]
        xml_fpaths = [tempdir+x for x in os.listdir(tempdir) if x.endswith('xml')]
        [strip_ns_file(x) for x in xml_fpaths]

    [shutil.copy(fpath+x, tempdir) for x in other_files]

    yield tempdir

    if not settings.DEBUG:
        shutil.rmtree(tempdir)
