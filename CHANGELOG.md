Metadata Hopper (pre-release)
-------------------

### Fixes for 09/02/2015 update

* Fixed bug where delete sometimes didn't reindex all the changed objects
* Updates urls at EZID when objects are published
* Status at EZID set to "unavailable" when objects are deleted

### Fixes for 08/26/2015 update

* Added object thumbnails to browse object lists
* Fixed bug where the "Untag" action did not index the correct objects
* Added bitmaps to the accepted image types
* Added date published to the dc.xml file
* When published objects are changed they will be reindexed in preview and publish environments
* Allows admins to publish objects for testing purposes

### Fixes for 08/19/2015 update

* Fix bug where indexer often failed due to concurrent indexing requests
* Preview confirmation email revised
* Institution selection alphabetized for manager login
* "Select all on all pages" in Connect now works
* Fixed bug where test button in rules creation didn't work with default values

