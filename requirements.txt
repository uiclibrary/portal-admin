Django==1.6.7
Pillow==3.0.0
South==1.0.1
amqp==1.4.6
anyjson==0.3.3
billiard==3.3.0.18
boto==2.36.0
celery==3.1.16
celery-with-redis==3.0
coverage==3.7.1
defusedxml==0.4.1
django-celery==3.1.16
django-extensions==1.3.9
django-jfu==2.0.9
django-widget-tweaks==1.3
kombu==3.0.23
pytz==2014.7
redis==2.10.3
selenium==2.45.0
six==1.7.3
sorl-thumbnail==11.12
wget==2.2
